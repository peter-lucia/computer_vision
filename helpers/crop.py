#!/usr/bin/python

import cv2

infile = "IMG-5110.png"
img = cv2.imread(infile)

resp = raw_input("Enter x1,x2,y1,y2 ('q' to quit): ")
print(type(resp))
resp = [int(each) for each in resp.split(",")]
cropped = img[resp[0]:resp[1], resp[2]:resp[3]]

name = infile.split(".")[0]
cv2.imwrite("{0}_cropped.png".format(name), cropped)
