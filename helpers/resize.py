#!/usr/bin/python

import cv2

infile = "IMG-5110.png"
# infile = "IMG-5413.png"
img = cv2.imread(infile)

print("Current dimensions for {0}: {1} x {2}".format(infile,
                                                     img.shape[0],
                                                     img.shape[1]))
resp = raw_input("Enter new dimensions width,height ('q' to quit): ")
width = int(resp.split(",")[0])
height = int(resp.split(",")[1])

resized = cv2.resize(img, (height, width), interpolation = cv2.INTER_AREA) 

name = infile.split(".")[0]
cv2.imwrite("{0}_{1}_{2}.png".format(name,
                                     resized.shape[0],
                                     resized.shape[1]),
                                     resized)
