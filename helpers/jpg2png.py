#!/usr/bin/python3

from PIL import Image

infile = input("Enter the name of the jpg to convert: ")
img = Image.open(infile)
name = infile.split(".")[0]
img.save("{0}.png".format(name))
