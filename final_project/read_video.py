
import cv2


# def video_gen(vid_path):
    # c = cv2.VideoCapture(vid_path)
    # success, image = c.read()
    # frame_num = 0
    # while success:
        # print("Reading frame {0}".format(frame_num))
        # yield image
        # success, image = c.read()
        # frame_num += 1

def video_frame_generator(filename):
    """A generator function that returns a frame on each 'next()' call.

    Will return 'None' when there are no frames left.

    Args:
        filename (string): Filename.

    Returns:
        None.
    """
    # Todo: Open file with VideoCapture and set result to 'video'. Replace None
    # https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_gui/py_video_display/py_video_display.html
    video = cv2.VideoCapture(filename)

    # Do not edit this while loop
    while video.isOpened():
        ret, frame = video.read()

        if ret:
            yield frame
        else:
            break

    # Todo: Close video (release) and yield a 'None' value. (add 2 lines)
    video.release()
    yield None

def mp4_video_writer(filename, frame_size, fps=20):
    """Opens and returns a video for writing.

    Use the VideoWriter's `write` method to save images.
    Remember to 'release' when finished.

    Args:
        filename (string): Filename for saved video
        frame_size (tuple): Width, height tuple of output video
        fps (int): Frames per second
    Returns:
        VideoWriter: Instance of VideoWriter ready for writing
    """
    # fourcc = cv2.cv.CV_FOURCC(*'MP4V')
    fourcc = cv2.VideoWriter_fourcc('M', 'P', '4', 'V')
    # filename = filename.replace(".mp4", ".avi")
    # fourcc = cv2.cv.CV_FOURCC(*'XVID')
    return cv2.VideoWriter(filename=filename, fourcc=fourcc, fps=fps, frameSize=frame_size)


