import tensorflow as tf
from tensorflow import keras
import numpy as np
import cv2
import os
import sys
import h5py
import scipy.io as sio
from datetime import datetime
from sklearn import svm as sk_svm
import math
# from skimage.feature import hog # last resort
import xml.etree.ElementTree as ET
import pickle
import re
from read_video import video_frame_generator, mp4_video_writer

print("tf version: {0}".format(tf.__version__))
print("keras version: {0}".format(tf.keras.__version__))
print("cv2 version: {0}".format(cv2.__version__))

"""
During grading, TAs expect to be able to run a python 3 file named run.py that 
writes five images to a graded_images folder in the current directory. 
The images should be named 1.png, 2.png, 3.png, 4.png and 5.png.  
You can pick these images; however, across the five of them we will be checking that you demonstrate following:

Correct classification at different scales
Correct classification at different orientations
Correct classification at different locations within the image.
Correct classification with different lighting conditions.

"""

INPUT_DIR = "input"
CUSTOM_DIR = os.path.join(INPUT_DIR, 'custom')
SVHN_CROPPED_DIR = os.path.join(INPUT_DIR, "svhn/cropped")
SVHN_UNCROPPED_DIR = os.path.join(INPUT_DIR, "svhn/uncropped")
GOOGLE_DIR = os.path.join(INPUT_DIR, "google")

FILENAME_FMT1_TRAIN = os.path.join(SVHN_UNCROPPED_DIR, "train/digitStruct.mat")
PARENT_PATH_FMT1_TRAIN = os.path.join(SVHN_UNCROPPED_DIR, "train/")
FILENAME_FMT1_TEST = os.path.join(SVHN_UNCROPPED_DIR, "test/digitStruct.mat")
PARENT_PATH_FMT1_TEST = os.path.join(SVHN_UNCROPPED_DIR, "test/")
VID1 = "input/video/IMG_5922.MOV"
VID2 = "input/video/IMG_5923.MOV"


def parse_bbox(idx, f):
    # call the h5py object's item() method to get its instance
    # at this index
    item = f['digitStruct']['bbox'][idx].item()
    res = {}
    for category in ['label', 'left', 'top', 'width', 'height']:
        # get category
        c = f[item][category]
        # use c.value[0][0] if only one digit in image, 
        # otherwise get data in this category for all digits in the image
        values = [f[c.value[i].item()].value[0][0]
                  for i in range(len(c))] if len(c) > 1 else [c.value[0][0]]
        # store this category's data in the resultant dict
        res[category] = values
    return res

def parse_name(idx, f):
    # get the name dataset
    name = f['/digitStruct/name']
    # get each character in the filename at the index
    return ''.join([chr(each[0]) for each in f[name[idx][0]].value])

def read_mat_format_1(filename):
    """read_mat
    Outputs np ndarray of the data

    :param filename: str of the filename
    """
    temp_filename = filename.replace("/", "_").replace(".", "_")
    if (os.path.exists("filenames_format_1_{0}.npy".format(temp_filename)) and
        os.path.exists("train_images_json_format_1_{0}.npy".format(temp_filename))):
        filenames = np.load("filenames_format_1_{0}.npy".format(temp_filename))
        train_images_json = np.load("train_images_json_format_1_{0}.npy".format(temp_filename))
        return filenames, train_images_json

    names = []
    bboxes = []
    with h5py.File(filename, 'r') as f:
        # name which is a string containing the filename of the corresponding image. 
        # bbox which is a struct array that contains the position, size and label 
        # of each digit bounding box in the image
        bbox = f['digitStruct']['bbox']
        name = f['digitStruct']['name']
        maximum = len(bbox)
        
        for idx, info in enumerate(bbox):
            temp_bbox = parse_bbox(idx, f)
            bboxes.append(temp_bbox)
            temp_name = parse_name(idx, f)
            names.append(temp_name)
            if (idx % 1000 == 0):
                print("idx = {0}/{1}".format(idx, maximum))
                # print("name = {0}".format(temp_name))
                # print("bbox = {0}".format(temp_bbox))
            # Uncomment the following (for faster debugging)
            # if idx == 50:
                # break

    names = np.array(names)
    bboxes = np.array(bboxes)
    np.save("filenames_format_1_{0}.npy".format(temp_filename), names)
    np.save("train_images_json_format_1_{0}.npy".format(temp_filename), bboxes)
    return names, bboxes

def get_negative_samples(filename_fmt1, parent_path, shape=(32,32,3)):
    names, bboxes = read_mat_format_1(filename_fmt1)
    
    _x = shape[0]
    _y = shape[1]
    labels = []
    images = []
    for idx, filename in enumerate(names):
        info = bboxes[idx]
        # read in the filename
        f = cv2.imread(os.path.join(parent_path, filename))
        # show_img("f", f)

        # Finish this section if necessary
        # either use the full uncropped image or individual cropped digits
        for idx, digit in enumerate(info["label"]):
            # get digit location
            x = int(info["left"][idx])
            y = int(info["top"][idx])
            w = int(info["width"][idx])
            h = int(info["height"][idx])

            # non-square crops
            # cutout = f[y:y+h, x:x+w]

            # show_img("f {0}x{1}".format(f.shape[0], f.shape[1]), f)
            if f.shape[0] > _x and f.shape[1] > _y:
                if x - _x >= 0 and y - _y >= 0:
                    # there is a 32 x 32 square available 
                    # above and to the left of the digit
                    # print("square available above and to the left of the digit")
                    cutout = f[0:_y, 0:_x]
                elif x - _x >= 0 and not y - _y >= 0:
                    # there's a square available to the 
                    # left but not above
                    # print("square available to the left but not above")
                    cutout = f[0:_y, 0:_x]
            else:
                # print("no square available. skipping this image.")
                continue
                cutout = np.zeros((_x, _y))
            # show_img("cutout {0}x{1}".format(cutout.shape[0], cutout.shape[1]), cutout)
            images.append(cutout)
            # append this as a non-number image
            labels.append(10) 

    images = np.array(images)
    labels = np.array(labels)
    labels = labels.reshape((labels.shape[0], 1))
    return images, labels

def read_mat_format_2(filename):
    """read_mat_format_2
    Loads cropped svhn dataset files
    X which is a 4-D matrix containing the images, 
    y which is a vector of class labels. 
    :param filename: str of the filename
    """
    print("opening filename: {0}".format(filename))
    contents = sio.loadmat(filename)
    X = contents["X"]
    y = contents["y"]
    return X, y

def show_img(img_name, img):
    """show_img
    Show the image for debugging

    :param img_name: str name of the image
    :param img: numpy array of the image
    """
    img = img.copy()
    if True:
        cv2.namedWindow(img_name, cv2.WINDOW_NORMAL)
        cv2.imshow(img_name, img)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

def get_model_basic(train_images, train_labels, epochs=5, classes=10):
    # setup the layers of the neural network
    model = keras.Sequential([
        # 2d array (32x32 pixels) to 1d array
        keras.layers.Flatten(input_shape=(224, 224, 3)),
        # 128 neurons (or nodes), REctified Linear Unit (Relu) 
        # where negative values are 0, anything 0 or above is unchanged
        # Relu = [0 -> inf]
        keras.layers.Dense(128, activation=tf.nn.relu),
        # 10 neurons (or nodes), returns an array of 10 probability scores that sum to 1
        # each node contains a score that indicates the probability that the current image belongs
        # to one of the 10 classes
        keras.layers.Dense(classes, activation=tf.nn.softmax)])
    # model = tf.keras.models.Sequential([
        # tf.keras.layers.Flatten(),
        # tf.keras.layers.Dense(512, activation=tf.nn.relu),
        # tf.keras.layers.Dropout(0.2),
        # tf.keras.layers.Dense(10, activation=tf.nn.softmax)
        # ])

    # optimizer = this is how the model is updated based on the data it sees and its loss function
    # loss function = measures how accurate the model is during training,
    #                 we want to minimize this function to steer the model in the right direction
    # metrics = used to monitor the training and testing. Here using accuracy, the fraction of the
    #           images that are correctly classified
    model.compile(optimizer=tf.train.AdamOptimizer(),
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])

    # start training: the model is "fit" to the training data
    # the model learns to associate images and labels
    model.fit(train_images, train_labels, epochs=epochs)
    # model.fit(train_images, train_labels, epochs=epochs, steps_per_epoch=1000)

    return model

def preprocess(X, y, grayscale=False):
    """preprocess

    :param X: images 4D np ndarry with shape (M, N, 3, count)
    :param y: labels, (count, 1)
    :param grayscale: bool, whether to take grayscale or not
    """
    X = X.copy()
    y = y.copy()

    # put images count first instead of last
    X = np.transpose(X, axes=(3,0,1,2))
    if grayscale:
        X = X[:,:,:,1] # take green channel (0:B, 1:G, 2:R)
    X=  X / 255.0

    # Since '0' has label 10 for the svhn dataset
    # Fix so all labels that equal 10 are now 0
    y[y == 10] = 0

    return X, y

def model_generator_vgg16(mat_filename, parent_path, train, batch_size=10, classes=10):

    # filenames, train_images_json = read_mat_format_1(mat_filename)

    X, y = read_mat_format_2(mat_filename)
    X, y = preprocess(X, y)

    if train:
        neg_images, neg_labels = get_negative_samples(FILENAME_FMT1_TRAIN, PARENT_PATH_FMT1_TRAIN)
    else:
        neg_images, neg_labels = get_negative_samples(FILENAME_FMT1_TEST, PARENT_PATH_FMT1_TEST)
    neg_images =  neg_images / 255.0

    X = np.vstack((X, neg_images))
    y = np.vstack((y, neg_labels))
    X,y = shuffle_input(X,y)

    while True:
        for i in range(0, X.shape[0], batch_size):
        # for i in range(0, filenames.shape[0], batch_size):

            images = X[i:i+batch_size,:,:,:]
            labels = y[i:i+batch_size]

            # X, y = parse_uncropped_single_digits(filenames[i:i+batch_size],
                                                 # train_images_json[i:i+batch_size],
                                                 # parent_path=parent_path)
            # X, y = parse_uncropped_all_digits(filenames[i:i+batch_size],
                                                 # train_images_json[i:i+batch_size],
                                                 # parent_path=parent_path)

            # X = np.transpose(X, axes=(1,2,3,0))
            # input(X.shape)
            # input(y.shape)

            # images, labels = preprocess(X, y)
            # images = X[i:i+batch_size,:,:,:]
            # labels = y[i:i+batch_size]
            # images = keras.applications.vgg16.preprocess_input(images)

            # inspect input
            if False:
                for i in range(0, images.shape[0]):
                    show_img("{0}".format(labels[i][0]), images[i, :, :])

            # print("images.shape = {0}".format(images.shape))
            # print("labels.shape = {0}".format(labels.shape))

            yield images, labels

def shuffle_input(X, y):
    assert X.shape[0] == y.shape[0]
    p = np.random.permutation((X.shape[0]))
    return X[p], y[p]


def model_generator(mat_filename, parent_path, train, batch_size=10, classes=10):

    X, y = read_mat_format_2(mat_filename)
    X, y = preprocess(X, y)
    if train:
        neg_images, neg_labels = get_negative_samples(FILENAME_FMT1_TRAIN, PARENT_PATH_FMT1_TRAIN)
    else:
        neg_images, neg_labels = get_negative_samples(FILENAME_FMT1_TEST, PARENT_PATH_FMT1_TEST)
    neg_images =  neg_images / 255.0
    # input(X.shape)
    # input(neg_images.shape)
    # input(y.shape)
    # input(neg_labels.shape)

    X = np.vstack((X, neg_images))
    y = np.vstack((y, neg_labels))
    X,y = shuffle_input(X,y)

    while True:
        for i in range(0, X.shape[0], batch_size):

            images = X[i:i+batch_size,:,:,:]
            labels = y[i:i+batch_size]

            # inspect input
            if False:
                for i in range(0, images.shape[0]):
                    show_img("{0}".format(labels[i][0]), images[i, :, :])

            # print("images.shape = {0}".format(images.shape))
            # print("labels.shape = {0}".format(labels.shape))

            yield images, labels

def model_generator_binary(mat_filename, parent_path, train, batch_size=10, classes=10):

    X, y = read_mat_format_2(mat_filename)
    X, y = preprocess(X, y)
    if train:
        neg_images, neg_labels = get_negative_samples(FILENAME_FMT1_TRAIN, PARENT_PATH_FMT1_TRAIN)
    else:
        neg_images, neg_labels = get_negative_samples(FILENAME_FMT1_TEST, PARENT_PATH_FMT1_TEST)
    neg_images =  neg_images / 255.0
    # input(X.shape)
    # input(neg_images.shape)
    # input(y.shape)
    # input(neg_labels.shape)

    X = np.vstack((X, neg_images))
    y = np.vstack((y, neg_labels))
    X,y = shuffle_input(X,y)

    # change so 0-9
    y[y<10] = 1
    y[y==10] = 0

    while True:
        for i in range(0, X.shape[0], batch_size):

            images = X[i:i+batch_size,:,:,:]
            labels = y[i:i+batch_size]

            # inspect input
            if False:
                for i in range(0, images.shape[0]):
                    show_img("{0}".format(labels[i][0]), images[i, :, :])

            # print("images.shape = {0}".format(images.shape))
            # print("labels.shape = {0}".format(labels.shape))

            yield images, labels

def get_vgg16_optimizer(epochs):

    learning_rate = 0.01
    sgd = keras.optimizers.SGD(lr=learning_rate,
            # decay=1e-6,
            decay=learning_rate / epochs,
            momentum=0.9,
            nesterov=True)
    # return tf.train.AdamOptimizer()
    return sgd

def get_custom_optimizer(epochs):

    # learning_rate = 0.01
    # sgd = keras.optimizers.SGD(lr=learning_rate,
            # # decay=1e-6,
            # decay=learning_rate / epochs,
            # momentum=0.9,
            # nesterov=True)
    return tf.train.AdamOptimizer()
    # return sgd

def train_evaluate_model_vgg16(mat_filename_train,
        parent_path_train,
        mat_filename_test,
        parent_path_test,
        epochs=5,
        classes=11,
        batch_size=10,
        vanilla=False):

    # https://github.com/keras-team/keras/issues/4465
    print("Creating vgg16 model...")
    # ----------------- vanilla vgg16 -------------------------- #
    if vanilla:
        print("Training vgg16 model without pretrained weights...")
        model = keras.applications.vgg16.VGG16(include_top=False,
            weights=None,
            # input_tensor=tf.keras.layers.Input(shape=(32,32,3), batch_size=batch_size),
            input_tensor=None,
            input_shape=(32,32,3),
            pooling='max',
            classes=classes)
    else:
        print("Training vgg16 model with pretrained weights...")
        model_partial = keras.applications.vgg16.VGG16(include_top=False,
                weights="imagenet")
        model_input = keras.layers.Input(shape=(32,32,3), name="image_input")

        model_output = model_partial(model_input)
        # temp = keras.layers.ZeroPadding2D(padding=(1,1), input_shape=(32,32,3))(model_output)
        # temp = keras.layers.Convolution2D(filters=64, kernel_size=3, strides=3, activation=tf.nn.relu)(temp)
        # temp = keras.layers.ZeroPadding2D(padding=(1,1))(temp)
        # temp = keras.layers.Convolution2D(filters=64, kernel_size=3, strides=3, activation=tf.nn.relu)(temp)

        temp = keras.layers.Flatten(name='flatten')(model_output)
        # temp = keras.layers.Dense(4096, activation='relu', name='fc3')(temp)
        # temp = keras.layers.Dense(classes, activation='softmax', name='predictions')(temp)
        temp = keras.layers.Dense(classes, activation=tf.nn.softmax)(temp)

        model = keras.models.Model(inputs=model_input, outputs=temp)

    print(model.summary())

    model.compile(
             optimizer=get_vgg16_optimizer(epochs),
             loss='sparse_categorical_crossentropy',
             metrics=['accuracy'])

    """
    # used only for determining samples_per_epoch
    img_filenames_train, images_json_train = read_mat_format_1(mat_filename_train)
    steps_train = int(len(img_filenames_train) / batch_size)
    # used only for determining steps in evaluate
    img_filenames_test, images_json_test = read_mat_format_1(mat_filename_test)
    steps_test = int(len(img_filenames_test) / batch_size)
    """
    # used only for determining samples_per_epoch
    img_filenames_train, images_json_train = read_mat_format_2(mat_filename_train)
    X, y = get_negative_samples(FILENAME_FMT1_TRAIN, PARENT_PATH_FMT1_TRAIN)
    train_samples = img_filenames_train.shape[3] + X.shape[0]
    print("train samples = {0}".format(train_samples))
    steps_train = int(float(train_samples) / batch_size)

    # used only for determining steps in evaluate
    img_filenames_test, images_json_test = read_mat_format_2(mat_filename_test)
    X, y = get_negative_samples(FILENAME_FMT1_TEST, PARENT_PATH_FMT1_TEST)
    test_samples = img_filenames_test.shape[3] + X.shape[0]
    print("test samples = {0}".format(test_samples))
    steps_test = int(float(test_samples) / batch_size)

    # TODO: Check out keras.callbacks.ReduceLROnPlateau
    callbacks = []
    callbacks.append(keras.callbacks.EarlyStopping(monitor='acc',
                              min_delta=0,
                              patience=2,
                              verbose=0, mode='auto'))
    # start training: the model is "fit" to the training data
    # the model learns to associate images and labels
    # model.fit(train_images, train_labels, epochs=epochs)
    history = model.fit_generator(model_generator_vgg16(mat_filename=mat_filename_train,
                                        parent_path=parent_path_train,
                                        train=True,
                                        batch_size=batch_size,
                                        classes=classes),
                        steps_per_epoch=steps_train,
                        epochs=epochs,
                        callbacks=callbacks,
                        validation_data=model_generator_vgg16(mat_filename=mat_filename_test,
                                                                         parent_path=parent_path_test,
                                                                         train=False),
                        validation_steps=steps_test)
                        # only do this if using IDs
                        # https://stanford.edu/~shervine/blog/keras-how-to-generate-data-on-the-fly
                        # use_multiprocessing=True,
                        # workers=10)
    test_loss, test_acc = model.evaluate_generator(model_generator_vgg16(mat_filename=mat_filename_test,
                                                                         parent_path=parent_path_test,
                                                                         train=False),
                                                   steps=steps_test)
    print("Test accuracy: {0}".format(test_acc))
    print("Test loss: {0}".format(test_loss))
    save_performance(model, history)

    return model

def train_evaluate_model(mat_filename_train,
        parent_path_train,
        mat_filename_test,
        parent_path_test,
        epochs=5,
        classes=11,
        batch_size=10,
        model_type="custom",
        model_generator=model_generator):
    """train_evaluate_model

    :param mat_filename_train:
    :param parent_path_train:
    :param mat_filename_test:
    :param parent_path_test:
    :param epochs:
    :param classes:
    :param batch_size:
    :param model_type:
    """
    # from Piazza: You will be training for 11 classes as your output, 0-9 + None. 
    # For None, all negative samples count. 
    # Think about how you could use the dataset you have to create negative samples.

    # Ivan:
    # 1. Train two classifiers one that detects number or not a number and then feed the number detection to a classifier
    # 2. Train a single classifier that detects all 10 number and has an 11th class for not a number
    # I would recommend doing 2.

    print("Custom Model..")
    # setup the layers of the neural network
    model = keras.Sequential([
        keras.layers.ZeroPadding2D(padding=(1,1), input_shape=(32,32,3)),
        keras.layers.Convolution2D(filters=256, kernel_size=3, strides=3, activation=tf.nn.relu),
        keras.layers.ZeroPadding2D(padding=(1,1)),
        keras.layers.Convolution2D(filters=256, kernel_size=3, strides=3, activation=tf.nn.relu),
        # keras.layers.MaxPooling2D(pool_size=(2,2), strides=(2,2), data_format='channels_last'),

        keras.layers.ZeroPadding2D(padding=(1,1)),
        keras.layers.Convolution2D(filters=256, kernel_size=3, strides=3, activation=tf.nn.relu),
        keras.layers.ZeroPadding2D(padding=(1,1)),
        keras.layers.Convolution2D(filters=256, kernel_size=3, strides=3, activation=tf.nn.relu),

        keras.layers.ZeroPadding2D(padding=(1,1)),
        keras.layers.Convolution2D(filters=256, kernel_size=3, strides=3, activation=tf.nn.relu),
        keras.layers.ZeroPadding2D(padding=(1,1)),
        keras.layers.Convolution2D(filters=256, kernel_size=3, strides=3, activation=tf.nn.relu),

        keras.layers.ZeroPadding2D(padding=(1,1)),
        keras.layers.Convolution2D(filters=256, kernel_size=3, strides=3, activation=tf.nn.relu),
        keras.layers.ZeroPadding2D(padding=(1,1)),
        keras.layers.Convolution2D(filters=256, kernel_size=3, strides=3, activation=tf.nn.relu),

        # keras.layers.ZeroPadding2D(padding=(1,1)),
        # keras.layers.Convolution2D(filters=128, kernel_size=3, strides=3, activation=tf.nn.relu),
        # keras.layers.ZeroPadding2D(padding=(1,1)),
        # keras.layers.Convolution2D(filters=128, kernel_size=3, strides=3, activation=tf.nn.relu),

        keras.layers.Flatten(),
        # keras.layers.Flatten(input_shape=(32, 32, 3)),
        # 128 neurons (or nodes), REctified Linear Unit (Relu) 
        # where negative values are 0, anything 0 or above is unchanged
        # Relu = [0 -> inf]
        # keras.layers.Dense(128, activation=tf.nn.relu),
        # 10 neurons (or nodes), returns an array of 10 probability scores that sum to 1
        # each node contains a score that indicates the probability that the current image belongs
        # to one of the 10 classes
        keras.layers.Dense(classes, activation=tf.nn.softmax)
    ])

    print(model.summary())

    # optimizer = this is how the model is updated based on the data it sees and its loss function
    # loss function = measures how accurate the model is during training,
    #                 we want to minimize this function to steer the model in the right direction
    # metrics = used to monitor the training and testing. Here using accuracy, the fraction of the
    #           images that are correctly classified
    model.compile(optimizer=get_custom_optimizer(epochs),
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])

    # used only for determining samples_per_epoch
    img_filenames_train, images_json_train = read_mat_format_2(mat_filename_train)
    X, y = get_negative_samples(FILENAME_FMT1_TRAIN, PARENT_PATH_FMT1_TRAIN)
    train_samples = img_filenames_train.shape[3] + X.shape[0]
    print("train samples = {0}".format(train_samples))
    steps_train = int(float(train_samples) / batch_size)

    # used only for determining steps in evaluate
    img_filenames_test, images_json_test = read_mat_format_2(mat_filename_test)
    X, y = get_negative_samples(FILENAME_FMT1_TEST, PARENT_PATH_FMT1_TEST)
    test_samples = img_filenames_test.shape[3] + X.shape[0]
    print("test samples = {0}".format(test_samples))
    steps_test = int(float(test_samples) / batch_size)

    callbacks = []
    callbacks.append(keras.callbacks.EarlyStopping(monitor='acc',
                              min_delta=0,
                              patience=2,
                              verbose=0, mode='auto'))
    # start training: the model is "fit" to the training data
    # the model learns to associate images and labels
    # model.fit(train_images, train_labels, epochs=epochs)
    history = model.fit_generator(model_generator(mat_filename=mat_filename_train,
                                        parent_path=parent_path_train,
                                        train=True,
                                        batch_size=batch_size,
                                        classes=classes),
                        steps_per_epoch=steps_train,
                        epochs=epochs,
                        callbacks=callbacks,
                        validation_data=model_generator(mat_filename=mat_filename_test,
                                                                   parent_path=parent_path_test,
                                                                   train=False),
                        validation_steps=steps_test)
                        # only do this if using IDs
                        # https://stanford.edu/~shervine/blog/keras-how-to-generate-data-on-the-fly
                        # use_multiprocessing=True,
                        # workers=10)
    test_loss, test_acc = model.evaluate_generator(model_generator(mat_filename=mat_filename_test,
                                                                   parent_path=parent_path_test,
                                                                   train=False),
                                                   steps=steps_test)
    print("Test accuracy: {0}".format(test_acc))
    print("Test loss: {0}".format(test_loss))
    save_performance(model, history)

    return model

def save_performance(model, history):
    time_str = datetime.now().strftime("%m.%d.%Y_%H.%M.%S")
    # save picture of the model
    # keras.utils.vis_utils.plot_model(model, "model{0}.png".format(time_str), show_layer_names=True, show_shapes=True)

    # save the history
    with open("history_dict_{0}.txt".format(time_str), 'wb') as f:
        pickle.dump(history.history, f, protocol=pickle.HIGHEST_PROTOCOL)

    with open("history_dict_{0}.txt".format(time_str), 'rb') as f:
        history_loaded = pickle.load(f)

        print(history_loaded == history.history)


def resize_cutouts(cutouts, h=32,w=32):
    """resize_cutouts

    :param cutouts: ndarray of images
    :param h: height
    :param w: width
    """
    # https://stackoverflow.com/questions/44515532/tensorflow-resize-image-with-crop-or-pad

    # print(cutouts[0].shape)
    # show_img("cutouts[0]", cutouts[0])
    # convert the cutouts to tensors
    # tf.constant(
    #   value,
    #   dtype=None,
    #   shape=None,
    #   name='Const',
    #   verify_shape=False
    # )
    # result = tf.constant(np.empty((h, w, 3)), dtype="uint8")
    # result = np.empty((h, w, 3), dtype="uint8")
    result = []
    error_indices = []
    for idx, cutout in enumerate(cutouts):
        # print("resize_cutouts: idx = {0}".format(idx))
        tf_image = tf.constant(cutout)
        try:
            tf_image = tf.image.resize_image_with_crop_or_pad(image=tf_image,
                                                   target_height=h,
                                                   target_width=w)
            # tf_image = cv2.resize(cutout, dsize=(32,32))
        except Exception as e:
            # show_img("problem img", cutout)
            print("Error = {0}".format(e))
            error_indices.append(idx)
            continue
        # print("tf_image.shape = {0}".format(tf_image.shape))
        tf_image = tensor_to_np(tf_image)
        # show_img("image", tf_image)
        # show_tensor("tensor", tf_image)
        # tf.stack([result, tf_image])
        # np.vstack((result, tf_image))
        result.append(tf_image)
    return np.array(result), error_indices

def resize_cutouts_cv(cutouts, h=32,w=32):
    result = []
    error_indices = []
    for idx, cutout in enumerate(cutouts):
        tf_image = cv2.resize(cutout, dsize=(h, w))
        result.append(tf_image)
    return np.array(result), error_indices


def tensor_to_np(tf_image):
    with tf.Session() as sess:
        return tf_image.eval(session=sess)
    
def show_tensor(text, tf_image):
    with tf.Session() as sess:
        show_img(text, tf_image.eval(session=sess))



def parse_uncropped_all_digits(filenames, train_images_json, parent_path):
    # name = 1.png
    # bbox = {'label': [1.0, 9.0], 'left': [246.0, 323.0], 'top': [77.0, 81.0], 'width': [81.0, 96.0], 'height': [219.0, 219.0]}

    labels = []
    images = []
    for idx, filename in enumerate(filenames):
        info = train_images_json[idx]
        # read in the filename
        f = cv2.imread(os.path.join(parent_path, filename))
        images.append(f)

        # Finish this section if necessary
        # either use the full uncropped image or individual cropped digits
        # for digit in info["label"]:
        # convert floats to integers
        label = [int(each) for each in info["label"]]
        # join the integers as one number
        label = int(''.join([str(each) for each in label])) 
        labels.append(label)
        # show_img(str(label), f)

    images = np.array(images)
    images, error_indices = resize_cutouts(images, h=224,w=224)
    labels = np.array(labels)
    labels = np.delete(labels, error_indices)
    print("deleted indices: {0}".format(error_indices))
    # show_tensor(str(labels[0]), images[0][0])
    labels = labels.reshape((labels.shape[0], 1))
    return images, labels

def parse_uncropped_single_digits(filenames, train_images_json, parent_path):
    # name = 1.png
    # bbox = {'label': [1.0, 9.0], 'left': [246.0, 323.0], 'top': [77.0, 81.0], 'width': [81.0, 96.0], 'height': [219.0, 219.0]}

    labels = []
    images = []
    for idx, filename in enumerate(filenames):
        info = train_images_json[idx]
        # read in the filename
        f = cv2.imread(os.path.join(parent_path, filename))
        # show_img("f", f)

        # Finish this section if necessary
        # either use the full uncropped image or individual cropped digits
        for idx, digit in enumerate(info["label"]):
            # convert float to int
            labels.append(int(digit))
            # crop out the digit
            x = int(info["left"][idx])
            y = int(info["top"][idx])
            w = int(info["width"][idx])
            h = int(info["height"][idx])

            # non-square crops
            cutout = f[y:y+h, x:x+w]
            

            # square crops 32x32
            # x_center = int(x + w/2)
            # y_center = int(y + h/2)
            # base_cutout = np.zeros((32,32,3))
            # x_min = x_center - 16 if x_center - 16 >= 0 else 0
            # x_max = x_center + 16 if x_center + 16 <= f.shape[1] else f.shape[1] - 1
            # y_min = y_center - 16 if y_center - 16 >= 0 else 0
            # y_max = y_center + 16 if y_center + 16 <= f.shape[0] else f.shape[0] - 1
            # input("f.shape = {0}".format(f.shape))
            # input("x_min = {0}".format(x_min))
            # input("x_max = {0}".format(x_max))
            # input("y_min = {0}".format(y_min))
            # input("y_max = {0}".format(y_max))
            # cutout = base_cutout
            # cutout = f[x_min:x_max, y_min:y_max]


            # Investigate playing with image size input
            # since this will be required anyway, will need to play around
            # with flipping the input randomly, resizing to 32x32x32
            # https://keras.io/preprocessing/image/
            # https://stackoverflow.com/questions/41907598/how-to-train-images-when-they-have-different-size

            # print(f.shape)
            # show_img("f", f)
            # append cropped digit to images
            images.append(cutout)

    # for each in images:
        # print(each.shape)
    images = np.array(images)
    labels = np.array(labels)
    # images, error_indices = resize_cutouts(images, h=32,w=32)
    # labels = np.array(labels)
    # labels = np.delete(labels, error_indices)
    # print("deleted indices: {0}".format(error_indices))
    labels = labels.reshape((labels.shape[0], 1))
    return images, labels


def run(train=True, vgg16=False, vanilla_vgg16=True, image=None, boxes=None, save_as="output/output.png"):
    """run

    :param train:
    :param vgg16:
    :param vanilla_vgg16:
    :param image:
    :param boxes:
    :param save_as:
    """

    class_names = [0,1,2,3,4,5,6,7,8,9,10] # 0-9 + no digit (10 is reserved for no digit)
    epochs = 25

    if train:
        if not vgg16:
            mat_filename_train = os.path.join(SVHN_CROPPED_DIR, "train_32x32.mat")
            parent_path_train = SVHN_CROPPED_DIR
            mat_filename_test = os.path.join(SVHN_CROPPED_DIR, "test_32x32.mat")
            parent_path_test = SVHN_CROPPED_DIR


            # custom
            model = train_evaluate_model(mat_filename_train,
                    parent_path_train,
                    mat_filename_test, 
                    parent_path_test,
                    epochs=epochs,
                    classes=11, 
                    batch_size=256,
                )
            model.save("model_custom_{0}.h5".format(datetime.now().strftime("%m.%d.%Y_%H.%M.%S")))
        else:
            # vgg16
            # mat_filename_train = os.path.join(SVHN_UNCROPPED_DIR, "train/digitStruct.mat")
            # parent_path_train = os.path.join(SVHN_UNCROPPED_DIR, "train/")
            # mat_filename_test = os.path.join(SVHN_UNCROPPED_DIR, "test/digitStruct.mat")
            # parent_path_test = os.path.join(SVHN_UNCROPPED_DIR, "test/")

            mat_filename_train = os.path.join(SVHN_CROPPED_DIR, "train_32x32.mat")
            parent_path_train = SVHN_CROPPED_DIR
            mat_filename_test = os.path.join(SVHN_CROPPED_DIR, "test_32x32.mat")
            parent_path_test = SVHN_CROPPED_DIR

            model = train_evaluate_model_vgg16(mat_filename_train,
                    parent_path_train,
                    mat_filename_test, 
                    parent_path_test,
                    epochs=epochs,
                    classes=11, 
                    batch_size=256,
                    vanilla=vanilla_vgg16,
                )
            model.save("model_vgg16_{0}.h5".format(datetime.now().strftime("%m.%d.%Y_%H.%M.%S")))
    else:
        if not vgg16:
            model_name = "model_custom_12.03.2018_03.57.57.h5" # 89% test accuracy
            print("loading model: {0}".format(model_name))
            model = keras.models.load_model(model_name)
            model.compile(optimizer=get_vgg16_optimizer(epochs=epochs),
                          loss='sparse_categorical_crossentropy',
                          metrics=['accuracy'])
        else:
            if vanilla_vgg16:
                # model_name = "model_vgg16_11.26.2018_01.32.45.h5"
                # model_name = "model_vgg16_11.26.2018_10.11.05.h5" # 96.4397 test accuracy, 99.91 training accuracy
                model_name = "model_vgg16_11.29.2018_03.30.05.h5" # 46.91% test accuracy, 43% training accuracy
            else:
                model_name = "model_vgg16_12.03.2018_05.07.39.h5" # 96.335 test accuracy
            print("loading model: {0}".format(model_name))
            model = keras.models.load_model(model_name)


    if False:
        # how to export as .pb file
        # get output node name (e.g. dense/Softmax)
        print(model.output.op.name)
        saver = tf.train.Saver()
        saver.save(keras.backend.get_session(), '/tmp/keras_model.ckpt')
        """
        pip show tensorflow

        # cd to tensorflow root dir
        cd /home/peter/miniconda3/envs/cv_final_cpu/lib/python3.6/site-packages
        python tensorflow/python/tools/freeze_graph.py \
        --input_meta_graph=/tmp/keras_model.ckpt.meta \
        --input_checkpoint=/tmp/keras_model.ckpt \
        --output_graph=/tmp/keras_frozen.pb \
        --output_node_names="dense/Softmax" \
        --input_binary=true

        """

    # inspect the predictions
    if False:
        X, y = read_mat_format_2(os.path.join(SVHN_CROPPED_DIR, "test_32x32.mat"))

        # preprocess
        X, y = preprocess(X, y)

        test_images = X.copy()

        # make predictions
        print("Calling model.predict against the test images...")
        predictions = model.predict(test_images)
        # input("predictions = {0}".format(predictions))
        num = input("Enter the number of predictions to view: ")
        num = int(num)
        for i in range(0, len(predictions)):
            # get indices of the max values along axis 
            max_idx = np.argmax(predictions[i], axis=None) 
            show_img("Prediction: {0}".format(class_names[max_idx]), test_images[i])
            if i == num:
                res = input("Quit? (y/n)")
                if res.lower() == "y":
                    break

    
    if False:
        # get_text_cutout_MSER(os.path.join(GOOGLE_DIR, "part1/000011_2.jpg"))
        # get_pyramid(cv2.imread(os.path.join(GOOGLE_DIR, "part1/000069_3.jpg"), cv2.IMREAD_COLOR))
        # sys.exit()

        # must be grayscale
        # img_original = cv2.imread(os.path.join(GOOGLE_DIR, "part1/000011_2.jpg"), cv2.IMREAD_COLOR)
        img_original = cv2.imread(os.path.join(GOOGLE_DIR, "part1/000069_3.jpg"), cv2.IMREAD_COLOR)

        # couple of options here
        # MSER
        # https://stackoverflow.com/questions/40078625/opencv-mser-detect-text-areas-python
        # OR
        # sliding window approach OR
        # sliding window on image pyramids and HoG (probably better)
        cutouts, positions = get_cutouts(img_original)

        for idx, cutout in enumerate(cutouts):
            # show_img("current cutout under test: {0}".format(cutout), cutout)
            digit_prediction = model.predict(np.array([cutout]))
            # print("prediction = {0}".format(digit_prediction))
            max_idx = np.argmax(digit_prediction, axis=None) 
            # print("max_idx = {0}".format(max_idx))
            class_prediction = class_names[max_idx]
            confidence = round(digit_prediction[0][max_idx]*100, 2)
            # show_img("Prediction: {0}".format(class_prediction), cutout)
            if class_prediction != 10 and confidence > 99.0:
                print("found {0} with confidence of {1}%".format(class_prediction,
                    confidence))
                x = positions[idx][0]
                y = positions[idx][1]
                img_original = draw_box(img_original,
                                        x,
                                        y,
                                        class_prediction,
                                        label="{0}".format(class_prediction))

                show_img("image", img_original)

    if False:
        if image is not None and boxes is not None:
            # TODO: get all cutouts and their positions of where the 
            # binary classifier thinks the digits are

            # run sliding window + NMS on each of the cutouts
            cutouts, positions = get_digit_cutouts(image,
                                                   boxes,
                                                   out_shape=(32, 32, 3))

            result = show_predictions(model, image, cutouts, positions, class_names)
            cv2.imwrite(save_as, result)


def show_predictions(model, image, cutouts, positions, class_names):
    # TODO: implement sliding window here on image pyramid
    for idx, cutout in enumerate(cutouts):
        # show_img("cutout", cutout)

        digit_prediction = model.predict(np.array([cutout]))
        # print("prediction = {0}".format(digit_prediction))
        max_idx = np.argmax(digit_prediction, axis=None) 
        # print("max_idx = {0}".format(max_idx))
        class_prediction = class_names[max_idx]
        confidence = round(digit_prediction[0][max_idx]*100, 2)
        # show_img("Prediction: {0}".format(class_prediction), cutout)
        if class_prediction != 10 and confidence > 90.0:
            print("found {0} with confidence of {1}%".format(class_prediction,
                confidence))
            x = positions[idx][0]
            y = positions[idx][1]
            image = draw_box(image,
                                x,
                                y,
                                class_prediction,
                                label="{0}".format(class_prediction))
    show_img("image", image)
    return image

def get_sliding_windows_over_gaussian_pyramid(image, min_window_size=(32,32,3), blur=True, scale=0.75):
    """get_sliding_windows_over_gaussian_pyramid
    Returns cutouts and positions of each cutout in the original image 
    after iterating through all sliding windows in the gaussian pyramid.
    """
    image = image.copy()
    h = min_window_size[0]
    w = min_window_size[1]
    cutouts_result = []
    positions = []
    images = get_pyramid(image, min_window_size=(h,w), blur=blur, scale=scale)
    # get 32x32 cutouts for each of the images in pyramid
    for idx, img in enumerate(images):
        cutouts, positions_2 = get_cutouts(img)
        cutouts = np.array(cutouts)
        for idx2, cutout in enumerate(cutouts):
            # get positions of each cutout in the original image
            pos_m = int(float(image.shape[0] - img.shape[0]) / 2 + positions_2[idx2][0])
            pos_n = int(float(image.shape[1] - img.shape[1]) / 2 + positions_2[idx2][1])
            cutouts_result.append(cutout)
            box_scale = (1.0/(scale**idx))
            positions.append([pos_n, pos_m, box_scale])

    cutouts_result = np.array(cutouts_result)
    positions = np.array(positions)
    return cutouts_result, positions


        
        
def get_digit_cutouts(image, boxes, out_shape=(32,32,3)):
    image = image.copy()
    image_original = image.copy()
    positions = []
    cutouts = []
    h = out_shape[0]
    w = out_shape[1]
    h_stride = h
    w_stride = w
    for x_start, y_start, x_end, y_end in boxes.astype(int):
        x = x_start
        y = y_start
        img_temp = image[y_start-h : y_end+h, x_start-w : x_end+w]

        if True:
            get_sliding_windows_over_gaussian_pyramid(img_temp, out_shape=out_shape)
        if False:
            # will want to run sliding window on image pyramid here instead
            images = get_pyramid(img_temp, min_window_size=(h, w), blur=True, scale=0.75)
            # for i in range(0, len(images)):
                # positions.append([x_start, y_start])
            for idx, img in enumerate(images):
                # # input(img.shape)
                # show_img("img - {0}".format(img.shape), img)
                # cutouts.append(img)
            # cutouts += images
            # TODO: sliding window over the cutouts
                x1 = 0
                y1 = 0
                x2 = img.shape[0]
                y2 = img.shape[1]
                
        # if False:
                for x in range(x1,x2+w, w_stride):
                    for y in range(y1,y2+h, h_stride):
                        print("x = {0}".format(x))
                        print("y = {0}".format(y))
                        # if y+h - y < 32 or x+w - x < 32:
                            # continue
                        cutout = img[y:y+h, x:x+w]
                        # input(cutout.shape)
                        if cutout.shape == (32,32,3):
                            cutouts.append(cutout)
                            positions.append([x_start+x1+x, y_start+y1+y])
                            image_original = cv2.rectangle(image_original,
                                                           pt1=(x_start+x, y_start+y),
                                                           pt2=(x_start+x+w, y_start+y+h),
                                                           color=(0, 255, 0),
                                                           thickness=1)
    cutouts = np.array(cutouts)
    input(cutouts.shape)
    positions = np.array(positions)
    show_img("original", image_original)
    return cutouts, positions
        

def draw_box(image, x, y, class_prediction, h=32, w=32, label="", thickness=1):
    image = image.copy()

    # draw white box for where the label will go.
    image[y+w-14:y+w, x:x+14, 0] = 0
    image[y+w-14:y+w, x:x+14, 1] = 255.0
    image[y+w-14:y+w, x:x+14, 2] = 0
    cv2.line(image,
             (x, y), # first point
             (x+h, y), # secondpoint
             (0,255,0), # color
             thickness) # line thickness
    cv2.line(image,
             (x, y), # first point
             (x, y+w), # secondpoint
             (0,255,0), # color
             thickness) # line thickness
    cv2.line(image,
             (x+h, y+w), # secondpoint
             (x, y+w), # first point
             (0,255,0), # color
             thickness) # line thickness
    cv2.line(image,
             (x+h, y+w), # first point
             (x+h, y), # secondpoint
             (0,255,0), # color
             thickness) # line thickness
    colors = [(125,27, 67), (151, 44, 87),
             (23, 105, 56), (37, 126, 73),
             (109, 160, 47), (86, 133, 29),
             (146,59,31), (176, 81, 51),
             (51, 143, 88), (199, 102,  71),
             (0, 255, 0)]
    color = (0,0,0)
    # write in label inside the box
    addText(image, label, (x,y+w), color=color)
    return image

def addText(img, text, location, color=(0, 0, 0)):
    fontFace = cv2.FONT_HERSHEY_SIMPLEX
    location = location
    fontScale = 0.6
    lineType = 2
    thickness = 1
    cv2.putText(img=img,
                text=text,
                org=location,
                fontFace=fontFace,
                fontScale=fontScale,
                color=color,
                lineType=lineType,
                thickness=thickness)


def get_cutouts(image):
    """generate_cutous
    Assumes the image is grayscale already

    :param image:
    """
    image = image.copy()

    # generate a bunch of 32 x 32 image cutouts from image
    cutouts = []
    positions = []

    M = image.shape[0]
    N = image.shape[1]

    i_step = 18
    j_step = 24
    for i in range(0, M, i_step):
        if i + i_step >= M:
            break
        for j in range(0, N, j_step):
            if j + j_step >= N:
                break
            min_i = i
            max_i = i+32
            min_j = j
            max_j = j+32

            # if next 32x32 window is outside of
            # the image, set starting i or j to
            # 32 pixels inside the edge
            if max_i >= M:
                min_i = M - 32
                max_i = M
            if max_j >= N:
                min_j = N - 32
                max_j = N
                 
            cutout = image[min_i:max_i, min_j:max_j]
            # show_img("cutout", cutout)

            # normalize the image between 0 and 1
            # cutout = cutout / 255.0

            cutouts.append(cutout)
            positions.append([i, j])

    return cutouts, positions

def get_text_cutout_MSER(filename):
    # initialize MSER object
    mser = cv2.MSER_create()

    # image = cv2.imread(filename, cv2.IMREAD_GRAYSCALE)
    image = cv2.imread(filename)

    # initialize image for visualization
    vis = image.copy()

    msers, bboxes = mser.detectRegions(image)

    # finds the convex hull of a 2D point set using the Sklansky's algorithm
    # hull=cv.convexHull(points[, hull[, clockwise[, returnPoints]]])
    hulls = [cv2.convexHull(points=p.reshape(-1, 1, 2)) for p in msers]

    # outline detected features
    # img=cv.polylines(img, pts,isClosed, color[, thickness[, lineType[, shift]]])
    cv2.polylines(img=vis,
                  pts=hulls,
                  isClosed=1,
                  color=(0, 255, 0))

    # show the feature visualizations
    show_img("MSER", vis)

    mask = np.zeros((image.shape[0],
                     image.shape[1],
                     1))

    for contour in hulls:
        # from documentations:
        # image	= cv.drawContours(image,
        # contours,
        # contourIdx,
        # color[, thickness[, lineType[, hierarchy[, maxLevel[, offset]]]]])
        # The function draws contour outlines in the image if thickness>=0
        # or fills the area bounded by the contours if thickness < 0

        cv2.drawContours(image=mask,
                         contours=[contour],
                         contourIdx=-1,
                         color=(255, 255, 255),
                         thickness=-1)  # thickness = -1 to fill area bounded by contours

    # get text only
    text = cv2.bitwise_and(image, image, mask=mask)
    show_img("MSER - Text", text)
    return text


def get_pyramid(image, min_window_size=(32,32), blur=True, scale=0.75):
    image = image.copy()
    
    M = image.shape[0]
    N = image.shape[1]
    result = []
    while M > min_window_size[0] and N > min_window_size[1]:
        # show_img("image {0}x{1}".format(M,N), image)
        result.append(image)
        if blur:
            image = cv2.pyrDown(src=image) # cv2.pyrDown (blurs image and downsamples)
        else:
            image = cv2.resize(src=image, dsize=(0,0), fx=scale, fy=scale) # cv2.resize (downsamples)
        M = image.shape[0]
        N = image.shape[1]

    return result

def run_hog(svm, filename):
    """get_hog
    Histogram of oriented gradients

    :param filename:
    """
    image = cv2.imread(filename)

    # https://www.learnopencv.com/handwritten-digits-classification-an-opencv-c-python-tutorial/
    # TODO: http://www.cs.utoronto.ca/~fidler/slides/CSC420/lecture17.pdf
    # TODO: http://scikit-image.org/docs/dev/auto_examples/features_detection/plot_hog.html

    # window_size = (32,32)
    # cell_size = (20,20)
    # hog = cv2.HOGDescriptor(window_size, block_size, block_stride, cell_size, num_bins)

    winSize = (32,32)
    blockSize = (20,20)
    blockStride = (4,4)
    cellSize = (10,10)
    nbins = 9
    derivAperture = 1
    winSigma = -1.
    histogramNormType = 0
    L2HysThreshold = 0.2
    gammaCorrection = 1
    nlevels = 64
    useSignedGradients = True
    # hog = cv2.HOGDescriptor(winSize,blockSize,blockStride,cellSize,nbins,derivAperture,winSigma,histogramNormType,L2HysThreshold,gammaCorrection,nlevels,useSignedGradients)
    hog = cv2.HOGDescriptor()
    # hog = cv2.HOGDescriptor(winSize=window_size
            # blockSize=(16,16),
            # blockStride=(8,8),
            # cellSize=(8,8),
            # nbins=9,
            # derivAperture=1,
            # winSigma=4,
            # )

    # hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())
    svm = svm.reshape((svm.shape[0], 1))
    # input(cv2.HOGDescriptor_getDefaultPeopleDetector().shape)
    # input(svm.shape)
    hog.setSVMDetector(svm)
    # hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())
    rects, weights = hog.detectMultiScale(image,
            winStride=(2,2), # step size of sliding window in x and y direction
            # padding=(8,8), # (optional) number of pixels in x and y direction that are padded prior to HOG feature extraction 
                       # (8,8), (16, 16), (24,24), (32,32) are common values. can improve accuracy
            scale=1.01, # factor by which image is resized at each layer of the image pyramid
            useMeanshiftGrouping=False) # whether to use mean shift instead of NMS or not

    # TODO: hog.compute(image)
    return rects, weights


def get_svm_sklearn(train_images, train_labels, test_images, test_labels):
    # classifier = sklearn.svm.SVC(gamma='scale')
    classifier = sk_svm.SVC(gamma='scale')
    classifier.fit(train_images, train_labels)
    predicted_labels = classifier.predict(test_images)

    if False:
        for i in range(0, test_images.shape[0]):
            show_img("{0}".format(predicted_labels[i]), test_images[i, :].reshape((32,32,3)))
    return classifier

def get_hog_skimage(image):
    desc, hog_image = hog(image=image,
            orientations=9,
            pixels_per_cell=(6,6),
            cells_per_block=(2,2),
            block_norm='L2',
            visualize=True,
            feature_vector=False,
            multichannel=True)
    return desc, hog_image

def get_svm(train_images, train_labels, test_images, test_labels):
    """get_svm

    :param train_images: np.ndarray
    :param train_labels: np.ndarray
    :param test_images: np.ndarray
    :param test_labels: np.ndarray
    """
    # https://docs.opencv.org/trunk/dd/d3b/tutorial_py_svm_opencv.html
    # https://www.learnopencv.com/handwritten-digits-classification-an-opencv-c-python-tutorial/
    # https://stackoverflow.com/questions/37038861/creating-svm-with-opencv-3-0-1-python-2-7
    # https://docs.opencv.org/3.4/d1/d73/tutorial_introduction_to_svm.html

    # instantiate support vector machine (svm)
    svm = cv2.ml.SVM_create()

    # set type of svm
    svm.setType(cv2.ml.SVM_C_SVC)

    # set svm kernel to radial basis function (rbf)
    # which allows us to classify non-linear data in a linear fashion
    svm.setKernel(cv2.ml.SVM_RBF)
    # svm.setKernel(cv2.ml.SVM_LINEAR)

    # TODO: find optimal C and gamma parameters
    # svm.trainAuto(td)
    C = 12.5 # will need to change likely
    gamma = 0.50625 # will need to change likely
    svm.setC(C)
    svm.setGamma(gamma)

    svm.train(train_images, cv2.ml.ROW_SAMPLE, train_labels)

    svm.save("svm_model.xml")

    predicted_labels = svm.predict(test_images)[1]
    if False:
        for i in range(0, test_images.shape[0]):
            show_img("{0}".format(predicted_labels[i][0]), test_images[i, :].reshape((32,32,3)))

    return svm


def get_binary_classifier_images(mat_filename, train=True):
    filename_X = "binary_classifier_X_{0}.npy".format(mat_filename.replace(".", "_").replace("/", "_"))
    filename_y = "binary_classifier_y_{0}.npy".format(mat_filename.replace(".", "_").replace("/", "_"))

    if os.path.exists(filename_X) and os.path.exists(filename_y):
        X = np.load(filename_X)
        y = np.load(filename_y)
        return X, y

    X, y = read_mat_format_2(mat_filename)

    X, y = preprocess(X, y)

    if train:
        neg_images, neg_labels = get_negative_samples(FILENAME_FMT1_TRAIN, PARENT_PATH_FMT1_TRAIN)
    else:
        neg_images, neg_labels = get_negative_samples(FILENAME_FMT1_TEST, PARENT_PATH_FMT1_TEST)

    neg_images =  neg_images / 255.0


    X = np.vstack((X, neg_images))
    y = np.vstack((y, neg_labels))
    X,y = shuffle_input(X,y)

    y[y != 10] = 1 # digit present, positive label
    y[y == 10] = -1 # no digit present, negative label

    print("images: {0}".format(X.shape[0]))
    print("converting X to float32...")
    X = X.astype("float32")
    # y = y.astype("float32")

    # inspect input
    if False:
        for i in range(0, X.shape[0]):
            show_img("{0}".format(y[i][0]), X[i, :, :])

    # replace images with hog images, which will be 32x32
    X_hog = []
    for idx, img in enumerate(X):
        # http://scikit-image.org/docs/dev/api/skimage.feature.html#skimage.feature.hog
        print("training: idx = {0}/{1}".format(idx, X.shape[0]))
        desc, hog_image = hog(image=img,
                orientations=9,
                pixels_per_cell=(8,8),
                cells_per_block=(3,3),
                block_norm='L2',
                visualize=True,
                feature_vector=False,
                multichannel=True)
        X_hog.append(hog_image)
        # show_img("hog_image", hog_image)
    
    X_hog = np.array(X_hog)
    np.save(filename_X, X_hog)
    np.save(filename_y, y)
    return X, y

def train_hog_svm():
    fname = os.path.join(GOOGLE_DIR, "part1/000069_3.jpg")
    image = cv2.imread(fname)

    mat_filename_train = os.path.join(SVHN_CROPPED_DIR, "train_32x32.mat")
    mat_filename_test = os.path.join(SVHN_CROPPED_DIR, "test_32x32.mat")

    train_images, train_labels = get_binary_classifier_images(mat_filename_train, train=True)
    test_images, test_labels = get_binary_classifier_images(mat_filename_test, train=False)

    if False:
        limit = 1000
        print("Remember to remove the training / testing limit ................................")
        train_images = train_images[0:limit]
        train_labels = train_labels[0:limit]
        test_images = test_images[0:limit]
        test_labels = test_labels[0:limit]

    d1, d2, d3 = train_images.shape
    train_images = train_images.reshape((d1, d2*d3))
    d1, d2, d3 = test_images.shape
    test_images = test_images.reshape((d1, d2*d3))
    # input(train_images.shape)

    if False:
        train_images = np.transpose(train_images, axes=(1,2,3,0))
        test_images = np.transpose(test_images, axes=(1,2,3,0))

        train_images = train_images[:,:,1,:] # use grayscale version
        test_images = test_images[:,:,1,:] # use grayscale version

        train_labels = train_labels.reshape((train_labels.shape[0],))
        test_labels = test_labels.reshape((test_labels.shape[0],))


    print(train_images.shape)
    print(train_labels.shape)
    # for idx, each in enumerate(train_images):
        # print(each.shape)


    svm = get_svm_sklearn(train_images, train_labels, test_images, test_labels)

    pickle.dump(svm, open("svm.joblib", 'wb'))

def save_svm_detector_as_pickle():
    tree = ET.parse("svm_model.xml")
    root = tree.getroot()

    SVs = root.getchildren()[0].getchildren()[-2].getchildren()[0]
    rho = float(root.getchildren()[0].getchildren()[-1].getchildren()[0].getchildren()[1].text)
    svmvec = [float(x) for x in re.sub('\s+', ' ', SVs.text ).strip().split(' ')]
    svmvec.append(-rho)
    pickle.dump(svmvec, open("svm.pickle", 'wb'))

def east_text_detector(image, H=32, W=32):
    # https://www.pyimagesearch.com/2018/08/20/opencv-text-detection-east-text-detector/
    # load pretrained text detector model
    # layers to add
    # TODO: Use this but create frozen_east_text_detection.pb myself
    # instead of using downloaded verison
    # https://github.com/kurapan/EAST
    # see the east structure here: https://github.com/kurapan/EAST/blob/master/model.py
    layers = ["feature_fusion/Conv_7/Sigmoid",
            "feature_fusion/concat_3"]
    if False:
        layers = ["dense/MatMul", "dense/Softmax"]
        layers = ["dense/Softmax"]

    # .pb corresponds to tensorflow
    conv_net = cv2.dnn.readNet("frozen_east_text_detection.pb")
    # print("Loaded layer names: ")
    # print(conv_net.getLayerNames())
    if False:
        conv_net = cv2.dnn.readNet("keras_frozen.pb")
    blob = cv2.dnn.blobFromImage(image=image,
            scalefactor=1.0,
            size=(W,H), 
            mean=(123.68,116.78,103.94), # mean subtraction combats illumination changes in training dataset
                                         # provide u_R, u_G, and u_B,
                                         # so R = R - u_R, G = G - u_G, and B = B - u_B
            swapRB=True,
            crop=False)
    conv_net.setInput(blob)
    if False:
        print("Loaded layer names: ")
        layer_names = conv_net.getLayerNames()
        print(layer_names)
        print(conv_net.getUnconnectedOutLayers())
        result = conv_net.forward(layers)
        input(result)
    (weights, rects) = conv_net.forward(layers)
    return weights, rects

def process_east(weights, rects, min_confidence=0.50):

    rows, cols = weights.shape[2:4]
    boxes = []
    probabilities = []
    for i in range(0, rows):
        score_results = weights[0,0,i]
        x0 = rects[0,0,i]
        x1 = rects[0,1,i]
        x2 = rects[0,2,i]
        x3 = rects[0,3,i]
        angles =rects[0,4,i]

        for j in range(0, cols):
            # ignore low probabilty predictions
            probability = score_results[j]
            if probability < min_confidence:
                continue

            x_offset, y_offset = (j*4.0, i*4.0)

            angle = angles[j]
            angle_cos = np.cos(angle)
            angle_sin = np.sin(angle)

            h = x0[j] + x2[j]
            w = x1[j] + x3[j]

            x_end = int(x_offset + (angle_cos*x1[j]) + (angle_sin*x2[j]))
            y_end = int(y_offset - (angle_sin*x1[j]) + (angle_cos*x2[j]))
            x_start = int(x_end - w)
            y_start = int(y_end - h)

            probabilities.append(probability)
            boxes.append((x_start, y_start, x_end, y_end))

    return probabilities, boxes

def run_nms(boxes, weights, ratio_thresh=0.3):
    """non_maxima_suppression
    NMS will eliminate boxes that are inside larger boxes

    :param boxes: np.ndarray (x1,y1,x2,y2)
    :param weights: np.ndarray holding probabilities
    :param ratio_thresh: float of overlap ratio threshold
    """
    boxes = boxes.astype("float32")
    picks = []

    # get x1,x2,y1,y2 for each box
    x1 = boxes[:, 0]
    y1 = boxes[:, 1]
    x2 = boxes[:, 2]
    y2 = boxes[:, 3]

    # get array for areas of all boxes
    area = (x2 - x1 + 1) * (y2 - y1 + 1)

    # get indices that would sort the weights
    idxs = np.argsort(weights)

    while len(idxs) > 0:
        idx_last = len(idxs) - 1
        i = idxs[idx_last]
        picks.append(i)
        

        # get four coordinates of of largest box
        # in the remaining boxes
        x1_max = np.maximum(x1[i],
                            x1[idxs[:idx_last]])
        y1_max = np.maximum(y1[i],
                            y1[idxs[:idx_last]])
        x2_min = np.minimum(x2[i],
                            x2[idxs[:idx_last]])
        y2_min = np.minimum(y2[i],
                            y2[idxs[:idx_last]])

        # get largest possible width and height
        width = np.maximum(0,
                           x2_min - x1_max + 1)
        height = np.maximum(0,
                            y2_min - y1_max + 1)

        # get all original boxes ratio
        # to the latest max bounding box
        ratios = (width * height) / area[idxs[:idx_last]]

        # delete indices of boxes that overlap with current largest bounding box
        idxs = np.delete(idxs,
                         np.concatenate(([idx_last],
                                         np.where(ratios > ratio_thresh)[0])))

    # return the picks
    return boxes[picks], picks

def get_bounding_boxes(fname):
    image_original = cv2.imread(fname)
    H, W = image_original.shape[:2]
    if False:
        num_32x32_H = math.ceil(H / 32)
        num_32x32_W = math.ceil(W / 32)
        H_NEW = 32*num_32x32_H
        W_NEW = 32*num_32x32_W
    else:
        H_NEW = 320
        W_NEW = 320
    h_ratio = H / float(H_NEW)
    w_ratio = W / float(W_NEW)

    image = cv2.resize(src=image_original, dsize=(W_NEW,H_NEW))

    # run east text detector
    weights, rects = east_text_detector(image,H=image.shape[0], W=image.shape[1])

    # get probabilities for each box
    probabilities, boxes = process_east(weights, rects, min_confidence=0.50)
    boxes = np.array(boxes)

    if len(probabilities) == 0:
        return None

    # apply non-maxima suppression to get rid of overlapping bounding boxes
    boxes = run_nms(boxes,
                    weights=probabilities,
                    ratio_thresh=0.3)
    result_boxes = []
    for x_start, y_start, x_end, y_end in boxes:
        x1 = int(x_start*w_ratio)
        y1 = int(y_start*h_ratio)

        x2 = int(x_end*w_ratio)
        y2 = int(y_end*h_ratio)
        result_boxes.append([x1, y1, x2, y2])
        image_original = cv2.rectangle(img=image_original,
                pt1=(x1, y1), # first vertex
                pt2=(x2, y2), # second vertex
                color=(0, 255, 0),
                thickness=2)
    show_img("result", image_original)
    result_boxes = np.array(result_boxes)
    return result_boxes

def add_noise_to_image(image, noise_type="gauss"):
    """add_noise_to_image
    Adds noise to an image.

    Note: will convert image to 0 and 1 if it has 
    pixels > 127
    :param image: ndarray input image
    :param noise_type: str, noise type
    :returns: ndarray output image between 0 and 1
    """
    image = image.copy()
    image = image.astype("float64")
    M,N,C = image.shape
    image /= 255.0
    if noise_type == "gauss":
        M,N,CH = image.shape
        mean = 0
        var = 0.1
        sigma = var**0.5
        noise = np.random.normal(mean, sigma, (M,N,CH))
        image += noise
    elif noise_type == "salt_and_pepper":
        # .10 of the image will be either salt and pepper, 
        # since 2/20 idxs are likely to be 1's or 0's
        factor = 20
        idxs = np.random.randint(0, factor, size=(image.shape[0], image.shape[1]))
        # add salt
        image[np.where(idxs == 1)] = 1.0
        # add pepper
        image[np.where(idxs == 0)] = 0.0

    # show_img("image after", image)
    image *= 255
    image = np.clip(image, 0, 255)
    image = image.astype("uint8")
    return image

def train_CNN_binary_classifier():

    class_names = [0,1]
    epochs = 25

    mat_filename_train = os.path.join(SVHN_CROPPED_DIR, "train_32x32.mat")
    parent_path_train = SVHN_CROPPED_DIR
    mat_filename_test = os.path.join(SVHN_CROPPED_DIR, "test_32x32.mat")
    parent_path_test = SVHN_CROPPED_DIR


    # custom
    model = train_evaluate_model(mat_filename_train,
            parent_path_train,
            mat_filename_test, 
            parent_path_test,
            epochs=epochs,
            classes=2, 
            batch_size=256,
            model_generator=model_generator_binary
        )
    model.save("model_binary_classifier_custom_cnn_{0}.h5".format(datetime.now().strftime("%m.%d.%Y_%H.%M.%S")))

def run_image_through_pipeline(image, model_bin_classifier=None, model_multi_digit_classifier=None):
    if model_bin_classifier is None or model_multi_digit_classifier is None:
        return
    
    class_names = [0,1] # no digit or digit
    sliding_windows, positions = get_sliding_windows_over_gaussian_pyramid(image, min_window_size=(32,32,3), blur=True, scale=0.75)
    # TODO: Run non-maxima suppression on boxes


    print("Running image through the custom CNN for binary classification.")
    predicted_labels = model_bin_classifier.predict(sliding_windows)

    class_names_multi = [0,1,2,3,4,5,6,7,8,9,10]
    # get multi-digit classification from second CNN
    print("Running image through the VGG16 CNN with pretrained weights for multi-digit classification.")
    predicted_digits_multi = model_multi_digit_classifier.predict(sliding_windows)

    # boxes has [x1, y1, scale]
    boxes = positions.copy()
    # make column for x2
    boxes[2] = boxes[0] + 32
    # add column for y2
    # boxes will have the format [x1, y1, x2, y2]
    temp = boxes[:,1] + 32
    temp = temp.reshape((temp.shape[0], 1))
    boxes = np.hstack((boxes, temp))
    # run nms
    boxes, picks = run_nms(boxes,
            weights=predicted_labels.max(axis=1),
            ratio_thresh=0.3)

    for idx, label in enumerate(predicted_labels):
        if idx not in picks:
            continue
        max_idx = np.argmax(label, axis=None) 
        max_idx_multi = np.argmax(predicted_digits_multi[idx], axis=None)
        class_prediction = class_names[max_idx]
        class_prediction_multi = class_names_multi[max_idx_multi]
        # if either CNN doesn't think it's a digit, it's not
        if class_prediction == 0 or class_prediction_multi == 10:
            continue

        # input(predicted_digits_multi[idx])
        # input(predicted_labels[idx])
        confidence_multi = predicted_digits_multi[idx][max_idx_multi]
        confidence_single = predicted_labels[idx][max_idx]
        # if either CNN's confidence in guess is too low, reject guess
        if confidence_single < 1 or confidence_multi < 1:
            continue
        x = int(positions[idx][0])
        y = int(positions[idx][1])
        scale_factor = positions[idx][2]
        h = int(32*scale_factor)
        w = int(32*scale_factor)
        # print("h = {0}".format(h))
        # print("w = {0}".format(w))
        image = draw_box(image,
                x=x,
                y=y,
                class_prediction=class_prediction_multi,
                h=h,
                w=w,
                label="{0}".format(class_prediction_multi))

    # show_img("image", image)
    return image


def run_video(video_path, out_path, model_bin_classifier=None, model_multi_digit_classifier=None):
    if model_bin_classifier is None or model_multi_digit_classifier is None:
        return

    image_gen = video_frame_generator(video_path)
    fps = 60

    image = next(image_gen)
    h, w, d = image.shape
    video_out = mp4_video_writer(out_path, (w, h), fps=fps)
    frame_num = 0
    while image is not None:
        processed_image = run_image_through_pipeline(image,
                    model_bin_classifier=model_bin_classifier,
                    model_multi_digit_classifier=model_multi_digit_classifier)
        image = next(image_gen)

        video_out.write(processed_image)

        frame_num += 1

    video_out.release()

if __name__ == '__main__':
    if False:
        train_hog_svm()

    if False:
        train_CNN_binary_classifier()
        sys.exit()


    binary_classifier_model_name = "model_binary_classifier_custom_cnn_12.03.2018_02.47.28.h5"
    model_bin_classifier = keras.models.load_model(binary_classifier_model_name)

    multi_digit_model_name = "model_vgg16_12.03.2018_05.07.39.h5" # 96.335 test accuracy
    model_multi_digit_classifier = keras.models.load_model(multi_digit_model_name)

    # process images 1.png -> 5.jpg
    if True:
        # get list of input files.
        files_list_orig = os.listdir("input/final")
        files_list = ["input/final/" + each for each in files_list_orig]
        for idx, filename in enumerate(files_list):
            print("Reading {0}".format(filename))
            image = cv2.imread(filename)
            result = run_image_through_pipeline(image,
                    model_bin_classifier=model_bin_classifier,
                    model_multi_digit_classifier=model_multi_digit_classifier)
            out_filename = "graded_images/{0}".format(files_list_orig[idx])
            print("Writing {0}".format(out_filename))
            cv2.imwrite(out_filename, result)
    
    # generate the movie #1 (movie #2 is my presentation)
    if False:
        print("Generating Video...")
        run_video("input/video/MyVideo.MOV",
                out_path="output/out.mp4",
                model_bin_classifier=model_bin_classifier,
                model_multi_digit_classifier=model_multi_digit_classifier)

    if False:
        # get list of input files.
        files_list_orig = os.listdir("input/custom")
        files_list = ["input/custom/" + each for each in files_list_orig]
        # generate noisy images
        for idx, filename in enumerate(files_list):
            print("Processing {0}".format(filename))
            image = cv2.imread(filename)
            image_gauss = add_noise_to_image(image, noise_type="gauss")
            image_peppered = add_noise_to_image(image, noise_type="salt_and_pepper")
            # show_img("image_gauss", image_gauss)
            cv2.imwrite("input/custom/" + files_list_orig[idx].replace(".jpg", "") + "_gauss.png", image_gauss)
            cv2.imwrite("input/custom/" + files_list_orig[idx].replace(".jpg", "") + "_pepper.png", image_peppered)
        sys.exit()

    if False:
        class_names = [0,1] # no digit or digit
        for filename in files_list:
            print("Processing {0}".format(filename))
            if "gauss" in filename or "pepper" in filename:
                continue
            image = cv2.imread(filename)
            sliding_windows, positions = get_sliding_windows_over_gaussian_pyramid(image, min_window_size=(32,32,3), blur=True, scale=0.75)

            # binary_classifier_model_name = "model_binary_classifier_custom_cnn_12.03.2018_01.27.24.h5"
            binary_classifier_model_name = "model_binary_classifier_custom_cnn_12.03.2018_02.47.28.h5"
            model = keras.models.load_model(binary_classifier_model_name)

            print("predicting labels...")
            predicted_labels = model.predict(sliding_windows)

            for idx, label in enumerate(predicted_labels):
                max_idx = np.argmax(label, axis=None) 
                class_prediction = class_names[max_idx]
                if class_prediction == 0:
                    continue
                confidence = round(predicted_labels[0][max_idx]*100, 2)
                x = int(positions[idx][0])
                y = int(positions[idx][1])
                scale_factor = positions[idx][2]
                h = int(32*scale_factor)
                w = int(32*scale_factor)
                print("h = {0}".format(h))
                print("w = {0}".format(w))
                image = draw_box(image,
                        x=x,
                        y=y,
                        class_prediction=1,
                        h=h,
                        w=w,
                        label="{0}".format(confidence))

            show_img("image", image)



    if False:
        for filename in files_list:
            print("Processing {0}".format(filename))
            if "gauss" in filename or "pepper" in filename:
                continue
            image = cv2.imread(filename)

            # numpy array [image, pos_x, pos_y], where positions are those
            # in the original image size
            sliding_windows, positions = get_sliding_windows_over_gaussian_pyramid(image, min_window_size=(32,32,3), blur=True, scale=0.75)
            svm = pickle.load(open("svm.joblib", 'rb'))
            # input(sliding_windows.shape)
            # get hog for all sliding windows
            sld_win_hog = []
            for idx, sld_win in enumerate(sliding_windows):
                if (idx % 100 == 0):
                    print("idx = {0}/{1}".format(idx, sliding_windows.shape[0]))
                desc, hog_image = hog(image=sld_win,
                        orientations=9,
                        pixels_per_cell=(8,8),
                        cells_per_block=(3,3),
                        block_norm='L2',
                        visualize=True,
                        feature_vector=False,
                        multichannel=True)
                # print("hog_image.shape = {0}".format(hog_image.shape))
                sld_win_hog.append(hog_image)
            sld_win_hog = np.array(sld_win_hog)

            d1, d2, d3 = sld_win_hog.shape
            sliding_windows_flat = sld_win_hog.reshape((d1, d2*d3))
            predicted_labels = svm.predict(sliding_windows_flat)
            predicted_labels = np.array(predicted_labels)
            for idx, label in enumerate(predicted_labels):
                if label == -1:
                    continue
                x = int(positions[idx][0])
                y = int(positions[idx][1])
                scale_factor = positions[idx][2]
                h = int(32*scale_factor)
                w = int(32*scale_factor)
                print("h = {0}".format(h))
                print("w = {0}".format(w))
                image = draw_box(image, x=x, y=y, class_prediction=1, h=h, w=w, label="1")

            show_img("image", image)
                # if False:
                    # for i in range(0, sliding_windows_flat.shape[0]):
                        # show_img("{0}".format(predicted_labels[i]), sliding_windows_flat[i, :].reshape((32,32)))

            # apply nms






    if False:
        for filename in files_list:
            print("Processing {0}".format(filename))
            image = cv2.imread(filename)

            # get bounding boxes from binary classifier
            boxes = get_bounding_boxes(filename)
            if boxes is None:
                continue
            # prepare filename to save as
            temp = (filename.replace("/","_")
                    .replace(".jpg","")
                    .replace(".png","")
                    .replace("input","")
                    .replace("custom","") + "_result.png")
            save_as = "output/" + temp

            if True:
                run(train=False,
                        vgg16=False,
                        vanilla_vgg16=False,
                        image=image,
                        save_as=save_as,
                        boxes=boxes)

    if False:
        run(train=True,
                vgg16=True,
                vanilla_vgg16=True,
                image=None,
                boxes=None)
