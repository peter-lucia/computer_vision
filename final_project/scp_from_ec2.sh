#!/bin/bash

#scp -i ~/.ssh/Peter.pem ec2-user@ec2-3-16-29-173.us-east-2.compute.amazonaws.com:/home/ec2-user/computer_vision/final_project/model_vgg16_11.25.2018_23.00.10.h5 .
#scp -i ~/.ssh/Peter.pem ec2-user@ec2-3-16-29-173.us-east-2.compute.amazonaws.com:/home/ec2-user/computer_vision/final_project/model_vgg16_11.26.2018_01.32.45.h5 .
#scp -i ~/.ssh/Peter.pem ec2-user@ec2-3-16-29-173.us-east-2.compute.amazonaws.com:/home/ec2-user/computer_vision/final_project/model_vgg16_11.26.2018_02.23.53.h5 .

# VGG16 With Pretrained weights
#scp -i ~/.ssh/Peter.pem ec2-user@ec2-3-16-29-173.us-east-2.compute.amazonaws.com:/home/ec2-user/computer_vision/final_project/model_vgg16_11.26.2018_10.11.05.h5 .

# Vanilla VGG16
#scp -i ~/.ssh/Peter.pem ec2-user@ec2-3-16-29-173.us-east-2.compute.amazonaws.com:/home/ec2-user/computer_vision/final_project/model_vgg16_11.29.2018_03.30.05.h5 .

#scp -i ~/.ssh/Peter.pem ec2-user@18.225.7.137:/home/ec2-user/computer_vision/final_project/model_binary_classifier_custom_cnn_12.03.2018_01.27.24.h5 .
#scp -i ~/.ssh/Peter.pem ec2-user@18.225.7.137:/home/ec2-user/computer_vision/final_project/history_dict_12.03.2018_02.12.49.txt .

# custom cnn binary classifier
#scp -i ~/.ssh/Peter.pem ec2-user@18.225.7.137:/home/ec2-user/computer_vision/final_project/history_dict_12.03.2018_02.47.28.txt .
#scp -i ~/.ssh/Peter.pem ec2-user@18.225.7.137:/home/ec2-user/computer_vision/final_project/model_binary_classifier_custom_cnn_12.03.2018_02.47.28.h5 .

# custom cnn multi digit classifier
#scp -i ~/.ssh/Peter.pem ec2-user@18.225.7.137:/home/ec2-user/computer_vision/final_project/history_dict_12.03.2018_03.57.57.txt .
#scp -i ~/.ssh/Peter.pem ec2-user@18.225.7.137:/home/ec2-user/computer_vision/final_project/model_custom_12.03.2018_03.57.57.h5 .

# vgg16 with pretrained weights
#scp -i ~/.ssh/Peter.pem ec2-user@18.225.7.137:/home/ec2-user/computer_vision/final_project/history_dict_12.03.2018_05.07.39.txt .
#scp -i ~/.ssh/Peter.pem ec2-user@18.225.7.137:/home/ec2-user/computer_vision/final_project/model_vgg16_12.03.2018_05.07.39.h5 .

# vanilla VGG16
scp -i ~/.ssh/Peter.pem ec2-user@18.225.7.137:/home/ec2-user/computer_vision/final_project/history_dict_12.03.2018_05.45.10.txt .
scp -i ~/.ssh/Peter.pem ec2-user@18.225.7.137:/home/ec2-user/computer_vision/final_project/model_vgg16_12.03.2018_05.45.10.h5 .






