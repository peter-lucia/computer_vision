import matplotlib.pyplot as plt
import pickle

files = {
"Plain VGG16": "history_dict_12.03.2018_05.45.10.txt",
"VGG16 with Pre-Trained Weights": "history_dict_12.03.2018_05.07.39.txt",
"Custom CNN Multi-digit Classifier": "history_dict_12.03.2018_03.57.57.txt",
"Custom CNN Binary Classifier": "history_dict_12.03.2018_02.47.28.txt",
}

for title, fname in files.items():
    with open(fname, 'rb') as f:
        data = pickle.load(f)
        plt.plot(data['acc'])
        plt.plot(data['val_acc'])
        plt.legend(["Train Acc", "Test Acc"], loc="upper left")
        plt.title("{0} Accuracy".format(title))
        plt.ylabel("Accuracy")
        if "Plain" in title:
            plt.ylim((0, 1))
        plt.xlabel("Epoch")
        plt.savefig("{0}_Accuracy.png".format(title), dpi=100)
        plt.clf()
        # plt.show()

for title, fname in files.items():
    with open(fname, 'rb') as f:
        data = pickle.load(f)
        plt.plot(data['loss'])
        plt.plot(data['val_loss'])
        plt.legend(["Train Loss", "Test Loss"], loc="upper left")
        plt.title("{0} Loss".format(title))
        plt.ylabel("Loss")
        plt.xlabel("Epoch")
        plt.savefig("{0}_Loss.png".format(title), dpi=100)
        plt.clf()
        # plt.show()
