#!/bin/bash

# -L means to follow redirects, commonly used by sourceforge, for example.
#mkdir -p input/svhn/uncropped/train
#mkdir -p input/svhn/uncropped/test
#mkdir -p input/svhn/uncropped/extra
mkdir -p input/svhn/cropped
#curl -L http://ufldl.stanford.edu/housenumbers/train.tar.gz > input/svhn/uncropped/train/train.tar.gz
#curl -L http://ufldl.stanford.edu/housenumbers/test.tar.gz > input/svhn/uncropped/test/test.tar.gz
#curl -L http://ufldl.stanford.edu/housenumbers/extra.tar.gz > input/svhn/uncropped/extra/extra.tar.gz

curl -L http://ufldl.stanford.edu/housenumbers/train_32x32.mat > input/svhn/cropped/train_32x32.mat
curl -L http://ufldl.stanford.edu/housenumbers/test_32x32.mat > input/svhn/cropped/test_32x32.mat
curl -L http://ufldl.stanford.edu/housenumbers/extra_32x32.mat > input/svhn/cropped/extra_32x32.mat

#tar -xvzf input/svhn/uncropped/train/train.tar.gz -C input/svhn/uncropped/
#tar -xvzf input/svhn/uncropped/test/test.tar.gz -C input/svhn/uncropped/
#tar -xvzf input/svhn/uncropped/extra/extra.tar.gz -C input/svhn/uncropped/
