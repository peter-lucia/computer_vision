# Multi-digit Number Recognition in Real-World Imagery using Convolutional Neural Networks
<center>Peter Lucia, plucia3@gatech.edu</center>
## Video Presentation Link
- https://youtu.be/r6ppFaZm7nM
- https://www.youtube.com/watch?v=r6ppFaZm7nM&feature=youtu.be

## Video Demonstration Link
- https://youtu.be/HO5dnaFViJ8
- https://www.youtube.com/watch?v=HO5dnaFViJ8

## CNN Download Links for convenience. Only the first two links are needed by run.py and should be placed in the project's root directory
- Custom CNN - Binary Classifier: https://drive.google.com/file/d/1dMxfh7qzTxXQuOb1d9HOOqlmcew57z9J/view?usp=sharing
- VGG16 with pretrained weights: https://drive.google.com/open?id=1iXfGIJi7HZr9f6OhNchivm3loDd493Lc

- (optional) Plain VGG16: https://drive.google.com/open?id=1ceTtojhsIawz5l4s-6jU3xehdXUDFumm
- (optional) Custom CNN - Multi-digit Classifier: https://drive.google.com/open?id=1MkT8fDXMa6IzFkKSzwj-xtwPBHbvWqwL



 
