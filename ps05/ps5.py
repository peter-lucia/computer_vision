"""
CS6476 Problem Set 5 imports. Only Numpy and cv2 are allowed.
"""
import numpy as np
np.random.seed(1234)
import cv2


# Assignment code
class KalmanFilter(object):
    """A Kalman filter tracker"""

    def __init__(self, init_x, init_y, Q=0.1 * np.eye(4), R=0.1 * np.eye(2)):
        """Initializes the Kalman Filter

        Args:
            init_x (int or float): Initial x position.
            init_y (int or float): Initial y position.
            Q (numpy.array): Process noise array.
            R (numpy.array): Measurement noise array.
        """
        # note, not using acceleration in our exercise
        # X (state vector) = [x, y, v_x, v_y]
        self.state = np.array([init_x, init_y, 0., 0.])  # state

        self.covariance = np.eye(4) # (4x4) covariance matrix
        self.Dt = np.eye(4)         # (4x4) state transition matrix Dt
        # set delta t's (change in time) in the Dt matrix to 1
        self.Dt[0, 2] = 1
        self.Dt[1, 3] = 1
        self.Mt = np.zeros((2, 4))  # (2x4) measurement matrix
        self.process_noise = Q      # (4x4) 
        self.measurement_noise = R  # (2x2) 

    def predict(self):
        """predict
        Replace the class variables for the state and covariance arrays
        with the prediction process

        """

        self.state = self.Dt.dot(self.state)
        self.covariance = (self.Dt*self.covariance*(self.Dt.T) +
                           self.process_noise)
        # raw_input("state before prediction: {0}".format(self.state))
        self.state[0] = self.state[0] + self.state[2]
        self.state[1] = self.state[1] + self.state[3]
        # raw_input("state after prediction: {0}".format(self.state))

    def correct(self, meas_x, meas_y):
        """correct
        Correct the state and covariances using the Kalman gain
        and the measurements obtained from our sensor.

        :param meas_x:
        :param meas_y:
        """

        y = np.array([meas_x, meas_y])
        temp1 = y - self.Mt.dot(self.state)
        temp2 = np.linalg.inv(
            (self.Mt.dot(self.covariance).dot(
                self.Mt.T)) +
                    self.measurement_noise)

        kalman_gain = self.covariance.dot(self.Mt.T).dot(temp2)
        self.state = self.state + kalman_gain.dot(temp1)
        self.covariance = (np.eye(4) - kalman_gain.dot(
            self.Mt))*self.covariance
        # raw_input("meas_x = {0}".format(meas_x))
        # raw_input("meas_y = {0}".format(meas_y))
        self.state[2] = self.state[0] - meas_x
        self.state[3] = self.state[1] - meas_y
        self.state[0] = meas_x
        self.state[1] = meas_y
        # raw_input("state after correction: {0}".format(self.state))

    def process(self, measurement_x, measurement_y):

        self.predict()
        self.correct(measurement_x, measurement_y)

        return self.state[0], self.state[1]


class ParticleFilter(object):
    """A particle filter tracker.

    Encapsulating state, initialization and update methods. Refer to
    the method run_particle_filter( ) in experiment.py to understand
    how this class and methods work.
    """

    def __init__(self, frame, template, **kwargs):
        """Initializes the particle filter object.

        The main components of your particle filter should at least be:
        - self.particles (numpy.array): Here you will store your particles.
                                        This should be a N x 2 array where
                                        N = self.num_particles. This component
                                        is used by the autograder so make sure
                                        you define it appropriately.
                                        Make sure you use (x, y)
        - self.weights (numpy.array): Array of N weights, one for each
                                      particle.
                                      Hint: initialize them with a uniform
                                      normalized distribution (equal weight for
                                      each one). Required by the autograder.
        - self.template (numpy.array): Cropped section of the first video
                                       frame that will be used as the template
                                       to track.
        - self.frame (numpy.array): Current image frame.

        Args:
            frame (numpy.array): color BGR uint8 image of initial video frame,
                                 values in [0, 255].
            template (numpy.array): color BGR uint8 image of patch to track,
                                    values in [0, 255].
            kwargs: keyword arguments needed by particle filter model:
                    - num_particles (int): number of particles.
                    - sigma_exp (float): sigma value used in the similarity
                                         measure.
                    - sigma_dyn (float): sigma value that can be used when
                                         adding gaussian noise to u and v.
                    - template_rect (dict): Template coordinates with x, y,
                                            width, and height values.
        """
        self.num_particles = kwargs.get('num_particles')  # required by the autograder
        self.sigma_exp = kwargs.get('sigma_exp')  # required by the autograder
        self.sigma_dyn = kwargs.get('sigma_dyn')  # required by the autograder
        self.template_rect = kwargs.get('template_coords')  # required by the autograder
        # If you want to add more parameters, make sure you set a default value so that
        # your test doesn't fail the autograder because of an unknown or None value.
        #
        # The way to do it is:
        # self.some_parameter_name = kwargs.get('parameter_name', default_value)
        self.gray = False

        self.template = template
        if self.gray:
            self.template = self.template[:,:,1]
        self.frame = frame
        if self.gray:
            self.frame = frame[:,:,1] # use green channel (grey)

        # Initialize your particles array. Read the docstring.

        # self.particles contains a list of x, y positions for where the object we're tracking is 
        # thought to be located in each frame
        # initialize shape (N x 2) of particles matrix
        self.particles = np.zeros((self.num_particles, 2))

        # (unused) set all particles to random positions within the frame
        # select random integers in range 0, frame.shape[0] for the x values
        # self.particles[:,0] = np.random.randint(low=0, high=frame.shape[0], size=(self.num_particles,))
        # select random integers in range 0, frame.shape[1] for the y values
        # self.particles[:,1] = np.random.randint(low=0, high=frame.shape[1], size=(self.num_particles, ))

        # initialize the particles to the center of the template
        self.particles[:,0] = int(self.template_rect['x'] + self.template_rect['w'] / 2)
        self.particles[:,1] = int(self.template_rect['y'] + self.template_rect['h'] / 2)

        # find out what we need to store in the particles N x 2 matrix, position, weight, action, new measurement.
        # equal weight for each particle initially
        self.weights = np.array([1.0/self.num_particles]*self.num_particles)# Initialize your weights array. Read the docstring.

        # Initialize any other components you may need when designing your filter.

        # self.state is initialized as the mean of all particles, since there are no weights yet.
        # later, the state (x,y) position of where the object is predicted to be at, will be defined
        # by the weighted mean of all the particles
        self.state = np.array([self.particles[:,0].mean(axis=None), self.particles[:,1].mean(axis=None), 0, 0])
        self.similarities = np.zeros((self.num_particles, 1))
        np.random.seed(1234)


    def get_particles(self):
        """Returns the current particles state.

        This method is used by the autograder. Do not modify this function.

        Returns:
            numpy.array: particles data structure.
        """
        return self.particles

    def get_weights(self):
        """Returns the current particle filter's weights.

        This method is used by the autograder. Do not modify this function.

        Returns:
            numpy.array: weights data structure.
        """
        return self.weights

    def get_error_metric(self, template, frame_cutout):
        """Returns the error metric used based on the similarity measure.
        PL: Use each particle's location as the center of the frame cutout
        and compare it to the template

        Returns:
            float: similarity value.
        """

        # get mse
        # axis = 0 # return array, take average for each column
        # axis = 1 # return array, take average for each row
        axis = None # return float, take average for all elements
        mse = ((template - frame_cutout)**2).mean(axis=axis)

        # get similarity
        similarity = np.exp(-mse / (2*(self.sigma_exp**2)))
        
        return similarity

    def resample_particles(self, weights):
        """Returns a new set of particles

        This method does not alter self.particles.

        Use self.num_particles and self.weights to return an array of
        resampled particles based on their weights.

        See np.random.choice or np.random.multinomial.
        
        Returns:
            numpy.array: particles data structure.
        """

        # select self.num_particles number of random integers from 0 to self.num_particles
        # using weights as the probability value
        result = np.random.choice(self.num_particles, size=self.num_particles, p=weights)

        return self.particles[result]



    def process(self, frame):
        """Processes a video frame (image) and updates the filter's state.

        Implement the particle filter in this method returning None
        (do not include a return call). This function should update the
        particles and weights data structures.

        Make sure your particle filter is able to cover the entire area of the
        image. This means you should address particles that are close to the
        image borders.

        Args:
            frame (numpy.array): color BGR uint8 image of current video frame,
                                 values in [0, 255].

        Returns:
            None.
        """

        frame = frame.copy()
        if self.gray:
            frame = frame[:,:,1] # use gray only

        
        # get latest center and movement. (movement is unused)
        new_x, new_y = self.get_weighted_mean(self.particles, self.weights)
        self.state = np.array([new_x,
                            new_y,
                            self.state[0] - new_x,
                            self.state[1] - new_y])

        # prepare dynamics to be applied to each particle
        dx = np.random.randint(-self.sigma_dyn, high=self.sigma_dyn, size=self.num_particles)
        dy = np.random.randint(-self.sigma_dyn, high=self.sigma_dyn, size=self.num_particles)

        # move the particles
        self.particles[:, 0] += dx
        self.particles[:, 1] += dy

        # ensure particles are not outside the frame
        self.clip_particles(self.frame)


        # iterate through all the particles, and compute similarity
        # for each particle as if cutout is centered around each 
        # of the particles
        for index, row in enumerate(self.particles[:, 0]):
            # get a box centered around the particle
            (x_start,
             x_end,
             y_start,
             y_end) = self.get_cutout(self.particles[index, 0],
                                      self.particles[index, 1],
                                      self.template,
                                      frame)

            frame_cutout = frame[y_start:y_end, x_start:x_end]

            similarity = self.get_error_metric(self.template, frame_cutout)
            
            self.similarities[index] = similarity

        # then reweight the particles based on their similarity
        self.weights = np.array(self.similarities.flatten())

        # take the particle with the best similarity to the template, 
        # set the other particles' weights to zero.
        maxWeight = self.weights.max(axis=None)
        self.weights[self.weights < (maxWeight - 0.01*maxWeight)] = 0 #OPTION 1
        # self.weights[self.weights < maxWeight] = 0 # OPTION 2

        # normalize the weights
        self.weights /= self.weights.sum()

        # resample
        self.particles = self.resample_particles(self.weights)

    def render(self, frame_in):
        """Visualizes current particle filter state.

        This method may not be called for all frames, so don't do any model
        updates here!

        These steps will calculate the weighted mean. The resulting values
        should represent the tracking window center point.

        In order to visualize the tracker's behavior you will need to overlay
        each successive frame with the following elements:

        - Every particle's (x, y) location in the distribution should be
          plotted by drawing a colored dot point on the image. Remember that
          this should be the center of the window, not the corner.
        - Draw the rectangle of the tracking window associated with the
          Bayesian estimate for the current location which is simply the
          weighted mean of the (x, y) of the particles.
        - Finally we need to get some sense of the standard deviation or
          spread of the distribution. First, find the distance of every
          particle to the weighted mean. Next, take the weighted sum of these
          distances and plot a circle centered at the weighted mean with this
          radius.

        This function should work for all particle filters in this problem set.

        Args:
            frame_in (numpy.array): copy of frame to render the state of the
                                    particle filter.
        """

        x_weighted_mean, y_weighted_mean = np.average(self.particles, axis=0, weights=self.weights)

        # Complete the rest of the code as instructed.
        # weighted mean = sum(x_i*w_i) / sum(w_i)
        # since sum(w_i) = 1, we already have the weighted mean

        # PL: this function is only used to generate the current positions of the particles
        # for various frames selected by the local grader. it is not used in the actual
        # implementation of the particle filter.

        # draw box equal to the template size around latest self.state
        (x_start,
         x_end,
         y_start,
         y_end) = self.get_cutout(x_weighted_mean,
                                  y_weighted_mean,
                                  self.template,
                                  frame_in)

        top_left = [x_start, y_start]
        top_right = [x_end, y_start]
        bottom_right = [x_end, y_end]
        bottom_left = [x_start, y_end]
        frame_in = self.draw_box(frame_in,
                             top_left,
                             top_right,
                             bottom_left,
                             bottom_right,
                             thickness=2,
                             copy=False)
        distances = []
        for row in self.particles:
            frame_in[int(row[1]), int(row[0])] = (0, 255, 0)
            # cv2.circle(img=frame_in,
                       # center=(int(row[0]), int(row[1])),
                       # radius=1,
                       # color=(0, 255, 0),
                       # thickness=2)

            # first, find the distance of every
            # particle to the weighted mean. 
            # distance formula: d = sqrt((x2 - x1)**2 + (y2 - y1)**2)
            d = np.sqrt((row[0] - x_weighted_mean)**2 + 
                        (row[1] - y_weighted_mean)**2)
            distances.append(d)

        # second, find the weighted sum of these distances
        d_weighted_mean = np.average(distances, axis=None, weights=self.weights)

        # plot a circle that tracks the weighted mean of the particles
        cv2.circle(img=frame_in,
                   center=(int(x_weighted_mean), int(y_weighted_mean)),
                   radius=int(d_weighted_mean),
                   color=(0, 0, 255),
                   thickness=2)



    def show_img(self, img_name, img):
        if True:
            cv2.imshow(img_name, img)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

    def show_particles(self, frame):
        frame = frame.copy()
        for row in self.particles:
            frame[int(row[1]), int(row[0])] = (0, 255, 0)
            # frame[int(row[1]), int(row[0])] = 255

        self.show_img("particles", frame)

    def draw_box(self, img_temp, top_left, top_right, bottom_left, bottom_right, thickness=1, copy=True):
        if copy:
            img_temp = img_temp.copy()

        cv2.line(img_temp,
                 (top_left[0], top_left[1]), # first point
                 (bottom_left[0], bottom_left[1]), # secondpoint
                 (0,255,0), # color
                 thickness) # line thickness
        cv2.line(img_temp,
                 (top_left[0], top_left[1]), # first point
                 (top_right[0], top_right[1]), # secondpoint
                 (0,255,0), # color
                 thickness) # line thickness
        cv2.line(img_temp,
                 (bottom_right[0], bottom_right[1]), # secondpoint
                 (top_right[0], top_right[1]), # first point
                 (0,255,0), # color
                 thickness) # line thickness
        cv2.line(img_temp,
                 (bottom_right[0], bottom_right[1]), # first point
                 (bottom_left[0], bottom_left[1]), # secondpoint
                 (0,255,0), # color
                 thickness) # line thickness
        return img_temp

    def get_cutout(self, x, y, template, frame):

        # center cutout around x and y, 
        # it will be the same size as the template
        x_start = int(round(x - template.shape[1] / 2.0))
        x_end = int(round(x + template.shape[1] / 2.0))
        y_start = int(round(y - template.shape[0] / 2.0))
        y_end = int(round(y + template.shape[0] / 2.0))

        # check cutout isn't outside the frame
        if y_start < 0:
            y_end += abs(y_start)
            y_start = 0
        if x_start < 0:
            x_end += abs(x_start)
            x_start = 0
        if x_end > frame.shape[1]:
            x_start -= x_end - (frame.shape[1] - 1)
            x_end = frame.shape[1] - 1
        if y_end > frame.shape[0]:
            y_start -= y_end - (frame.shape[0] - 1)
            y_end = frame.shape[0] - 1

        return x_start, x_end, y_start, y_end

    def clip_particles(self, frame):
        self.particles[np.where(self.particles[:,0] > frame.shape[0] - 1)] = frame.shape[0] - 1
        self.particles[np.where(self.particles[:,1] > frame.shape[1] - 1)] = frame.shape[1] - 1

    def get_weighted_mean(self, particles, weights):

        x_weighted_mean, y_weighted_mean = np.average(particles, axis=0, weights=weights)

        return x_weighted_mean, y_weighted_mean

class AppearanceModelPF(ParticleFilter):
    """A variation of particle filter tracker."""

    def __init__(self, frame, template, **kwargs):
        """Initializes the appearance model particle filter.

        The documentation for this class is the same as the ParticleFilter
        above. There is one element that is added called alpha which is
        explained in the problem set documentation. By calling super(...) all
        the elements used in ParticleFilter will be inherited so you do not
        have to declare them again.
        """

        super(AppearanceModelPF, self).__init__(frame, template, **kwargs)  # call base class constructor

        self.alpha = kwargs.get('alpha')  # required by the autograder
        # If you want to add more parameters, make sure you set a default value so that
        # your test doesn't fail the autograder because of an unknown or None value.
        #
        # The way to do it is:
        # self.some_parameter_name = kwargs.get('parameter_name', default_value)
        self.template = self.template[:,:,1]
        self.frame = frame[:,:,1] # use green channel (grey)
        self.gray = True

    def process(self, frame):
        """Processes a video frame (image) and updates the filter's state.

        This process is also inherited from ParticleFilter. Depending on your
        implementation, you may comment out this function and use helper
        methods that implement the "Appearance Model" procedure.

        Args:
            frame (numpy.array): color BGR uint8 image of current video frame, values in [0, 255].

        Returns:
            None.
        """

        frame = frame.copy()
        if self.gray:
            frame = frame[:,:,1] # use gray only

        
        # get latest center and movement. (movement is unused)
        new_x, new_y = self.get_weighted_mean(self.particles, self.weights)
        self.state = np.array([new_x,
                            new_y,
                            self.state[0] - new_x,
                            self.state[1] - new_y])

        # prepare dynamics to be applied to each particle
        dx = np.random.randint(-self.sigma_dyn, high=self.sigma_dyn, size=self.num_particles)
        dy = np.random.randint(-self.sigma_dyn, high=self.sigma_dyn, size=self.num_particles)

        # move the particles
        self.particles[:, 0] += dx
        self.particles[:, 1] += dy

        # ensure particles are not outside the frame
        self.clip_particles(self.frame)


        # iterate through all the particles, and compute similarity
        # for each particle as if cutout is centered around each 
        # of the particles
        for index, row in enumerate(self.particles[:, 0]):
            # get a box centered around the particle
            (x_start,
             x_end,
             y_start,
             y_end) = self.get_cutout(self.particles[index, 0],
                                      self.particles[index, 1],
                                      self.template,
                                      frame)

            frame_cutout = frame[y_start:y_end, x_start:x_end]

            similarity = self.get_error_metric(self.template, frame_cutout)
            
            self.similarities[index] = similarity

        # then reweight the particles based on their similarity
        self.weights = np.array(self.similarities.flatten())

        # take the particle with the best similarity to the template, 
        # set the other particles' weights to zero.
        maxWeight = self.weights.max(axis=None)
        self.weights[self.weights < (maxWeight - 0.01*maxWeight)] = 0 #OPTION 1
        # self.weights[self.weights < maxWeight] = 0 # OPTION 2


        # normalize the weights
        self.weights /= self.weights.sum()

        # resample
        self.particles = self.resample_particles(self.weights)

        # self.show_particles(frame)
        # update template
        # large alpha = objects that move quickly
        # small alpha = objects that move slowly
        maxWeight = self.weights.max(axis=None)
        indices = np.argwhere(self.weights == maxWeight)
        # raw_input("indices = {0}".format(indices))
        best_particles = self.particles[indices]
        # raw_input("best particles = {0}".format(best_particles))
        mean_particle_coords = best_particles.mean(axis=0)
        # raw_input("mean_particle_coords = {0}".format(mean_particle_coords))
        mean_x = int(mean_particle_coords[0][0])
        mean_y = int(mean_particle_coords[0][1])
        # raw_input("mean_x = {0}".format(mean_x))
        # raw_input("mean_y = {0}".format(mean_y))
        # raw_input(index)
        (x_start,
         x_end,
         y_start,
         y_end) = self.get_cutout(mean_x,
                                  mean_y,
                                  self.template, # used for shape only
                                  frame)

        best_t = frame[y_start:y_end, x_start:x_end]
        # self.show_img("best_t", best_t)
        self.template = self.alpha*best_t + (1 - self.alpha)*self.template
        # self.show_img("self.template", self.template)
        # self.show_img("frame", frame)



class MDParticleFilter(AppearanceModelPF):
    """A variation of particle filter tracker that incorporates more dynamics."""

    def __init__(self, frame, template, **kwargs):
        """Initializes MD particle filter object.

        The documentation for this class is the same as the ParticleFilter
        above. By calling super(...) all the elements used in ParticleFilter
        will be inherited so you don't have to declare them again.
        """

        super(MDParticleFilter, self).__init__(frame, template, **kwargs)  # call base class constructor
        # If you want to add more parameters, make sure you set a default value so that
        # your test doesn't fail the autograder because of an unknown or None value.
        #
        # The way to do it is:
        # self.some_parameter_name = kwargs.get('parameter_name', default_value)
        self.template = template.copy()
        self.original_template = template.copy()
        self.frame = frame.copy()
        self.gray = False

        self.alpha = 0.30
        self.wait_frames = 26 # how often to update appearance model
        self.expand_xy = 1.05
        self.reduce_xy = 0.95
        self.counter = 0
        self.counter2 = 0
        self.templates = []
        np.random.seed(1234)

    def process(self, frame):
        """Processes a video frame (image) and updates the filter's state.

        This process is also inherited from ParticleFilter. Depending on your
        implementation, you may comment out this function and use helper
        methods that implement the "More Dynamics" procedure.

        Args:
            frame (numpy.array): color BGR uint8 image of current video frame,
                                 values in [0, 255].

        Returns:
            None.
        """
        frame = frame.copy()
        frame_color = frame.copy()
        if self.gray:
            frame = frame[:,:,1] # use gray only

        self.old_template = self.template.copy()
        
        # get latest center and movement. (movement is unused)
        new_x, new_y = self.get_weighted_mean(self.particles, self.weights)
        vx, vy = self.state[0] - new_x, self.state[1] - new_y

        temp_sim = self.get_similarity_for_template(self.template,
                                            frame,
                                            self.state[0],
                                            self.state[1])

        # check if velocity is high or similarity is too low
        # if abs(vx) > 3 or abs(vy) > 5 or (0.25 < temp_sim < 0.70):
        if abs(vx) > 4 or abs(vy) > 4:
            print("similarity = {0}".format(temp_sim))
            if self.counter < self.wait_frames: 
                self.counter += 1
                # don't update the template, occlusion possibly occuring
                return
            else:
                # reset the counter and resume
                H, W = self.template.shape[0], self.template.shape[1]
                self.template = self.original_template.copy()
                self.template = cv2.resize(self.template,
                                dsize=(W,H))
                self.template = cv2.resize(self.template,
                                  dsize=(0,0),
                                  fx=self.reduce_xy,
                                  fy=self.reduce_xy)
                self.template = cv2.resize(self.template,
                                  dsize=(0,0),
                                  fx=self.reduce_xy,
                                  fy=self.reduce_xy)
                self.counter = 0

        # update state
        self.state = np.array([new_x,
                            new_y,
                            vx,
                            vy])
        self.std_x, self.std_y = np.std(self.particles, axis=0)
        if self.counter % 2 == 1:
            self.resize_template(frame)
        
        # prepare dynamics to be applied to each particle
        dx = np.random.randint(-self.sigma_dyn, high=self.sigma_dyn, size=self.num_particles)
        dy = np.random.randint(-self.sigma_dyn*0.99, high=self.sigma_dyn, size=self.num_particles)


        # move the particles
        self.particles[:, 0] += dx
        self.particles[:, 1] += dy

        # iterate through all the particles, and compute similarity
        # for each particle as if cutout is centered around each 
        # of the particles
        temp_template = self.template.copy()
        for index, row in enumerate(self.particles[:, 0]):
            # get a box centered around the particle
            (x_start,
             x_end,
             y_start,
             y_end) = self.get_cutout_improved(self.particles[index, 0],
                                      self.particles[index, 1],
                                      temp_template,
                                      frame)

            frame_cutout = frame[y_start:y_end, x_start:x_end]
            # ensure cutout and tmeplate are of the same size
            frame_cutout, temp_template = self.trim(frame_cutout, temp_template)

            similarity = self.get_error_metric(temp_template, frame_cutout)
            # print("similarity = {0}".format(similarity))
            
            self.similarities[index] = similarity


        # print("self.similarities.max() = {0}".format(round(self.similarities.max(), 2)))
        # then reweight the particles based on their similarity
        self.weights = np.array(self.similarities.flatten())

        # take the particle with the best similarity to the template, 
        # set the other particles' weights to zero.
        maxWeight = self.weights.max(axis=None)
        # self.weights[self.weights < (maxWeight - 0.01*maxWeight)] = 0 #OPTION 1
        self.weights[self.weights < maxWeight] = 0 # OPTION 2


        # normalize the weights
        self.weights /= self.weights.sum()

        # resample
        self.particles = self.resample_particles(self.weights)

        # large alpha = objects that move quickly
        # small alpha = objects that move slowly
        if self.counter2 % 2 == 1:
            maxWeight = self.weights.max(axis=None)
            indices = np.argwhere(self.weights == maxWeight)
            best_particles = self.particles[indices]
            mean_particle_coords = best_particles.mean(axis=0)
            mean_x = int(mean_particle_coords[0][0])
            mean_y = int(mean_particle_coords[0][1])
            (x_start,
             x_end,
             y_start,
             y_end) = self.get_cutout(mean_x,
                                      mean_y,
                                      self.template, # used for shape only
                                      frame)

            best_t = frame[y_start:y_end, x_start:x_end]
            best_t, self.template = self.trim(best_t, self.template)
            self.template = self.alpha*best_t + (1 - self.alpha)*self.template

        self.counter += 1
        self.counter2 += 1


    def resize_template(self, frame):
        frame = frame.copy()
        # 1. check if making the template bigger makes the similarity better 
        template_expand_x_y = cv2.resize(src=self.template,
                                  dsize=(0,0),
                                  fx=self.expand_xy,
                                  fy=self.expand_xy)
        expanded_similarity_x_y = self.get_similarity_for_template(template_expand_x_y,
                                        frame,
                                        self.state[0],
                                        self.state[1])


        # 2. check if making the template smaller makes the similarity better
        template_reduce_x_y = cv2.resize(src=self.template,
                                  dsize=(0,0),
                                  fx=self.reduce_xy,
                                  fy=self.reduce_xy)
        reduced_similarity_x_y = self.get_similarity_for_template(template_reduce_x_y,
                                        frame,
                                        self.state[0],
                                        self.state[1])

        if reduced_similarity_x_y > expanded_similarity_x_y:
            self.template = template_reduce_x_y.copy()
        else:
            self.template = template_expand_x_y.copy()

    def set_best_template(self, frame):
        frame = frame.copy()
        sims = []
        for index, each in enumerate(self.templates):
            similarity = self.get_similarity_for_template(each,
                                                        frame,
                                                        self.state[0],
                                                        self.state[1])
            sims.append([index, similarity])

        best = max(sims, key=lambda x: x[1])
        self.template = self.templates[best[0]].copy()

    def get_similarity_for_template(self, template, frame, x, y):

        template = template.copy()
        # get a box centered around the particle
        (x_start,
         x_end,
         y_start,
         y_end) = self.get_cutout_improved(x,
                                  y,
                                  template,
                                  frame)

        frame_cutout = frame[y_start:y_end, x_start:x_end]
        frame_cutout, template = self.trim(frame_cutout, template)

        similarity = self.get_error_metric(template, frame_cutout)
        return similarity

    def trim(self, frame_cutout, template):
        frame_cutout = frame_cutout.copy()
        template = template.copy()
        if template.shape != frame_cutout.shape:
            MT, NT = template.shape[0], template.shape[1]
            MC, NC = frame_cutout.shape[0], frame_cutout.shape[1]
            if MT > MC:
                template = template[0:MC, :]
            elif MC > MT:
                frame_cutout = frame_cutout[0:MT,:]
            if NT > NC:
                template = template[:,0:NC]
            elif NC > NT:
                frame_cutout = frame_cutout[:,0:NT]

        if template.shape != frame_cutout.shape:
            print("Warning, corrected shapes don't match!")
        return frame_cutout, template

    def get_cutout_improved(self, x, y, template, frame):

        # center cutout around x and y, 
        # it will be the same size as the template
        M, N = template.shape[0], template.shape[1]
        M /= 2.0
        N /= 2.0
        x_start = int(round(x - N))
        x_end = int(round(x + N))
        y_start = int(round(y - M))
        y_end = int(round(y + M))

        # check cutout isn't outside the frame
        if y_start < 0:
            y_end += abs(y_start) - 1
            y_start = 0
        if x_start < 0:
            x_end += abs(x_start) - 1
            x_start = 0
        if x_end > frame.shape[1]:
            x_start -= x_end - (frame.shape[1] - 1)
            x_end = frame.shape[1] - 1
        if y_end > frame.shape[0]:
            y_start -= y_end - (frame.shape[0] - 1)
            y_end = frame.shape[0] - 1

        return x_start, x_end, y_start, y_end

class Template:
    def __init__(self,
            template=None,
            alpha=0.85,
            max_appearance_updates=65,
            expand_xy=1.01,
            reduce_xy=0.99,
            counter=0,
            counter2=0,
            particles=None,
            weights=None,
            state=None,
            similarities=None,
            start_frame=0,
            template_rect=None,
            num_particles=None):
        self.start_frame = start_frame
        self.template_rect = template_rect
        self.in_frame = template_rect['in_frame']
        self.out_frame = template_rect['out_frame']
        self.show = True
        self.sigma_dyn = template_rect['sigma_dyn']
        self.flag = template_rect['flag']

        self.template = template.copy()
        self.alpha = alpha
        self.max_appearance_updates = max_appearance_updates # how often to update appearance model
        self.expand_xy = expand_xy
        self.reduce_xy = reduce_xy
        self.counter = counter
        self.counter2 = counter2
        self.particles = particles
        self.weights = weights
        self.state = state
        self.similarities = similarities
        self.std_y = 0.0
        self.std_x = 0.0
        self.num_particles = num_particles

        # initialize the particles to the center of the template
        self.particles[:,0] = int(self.template_rect['x'] + self.template_rect['w'] / 2)
        self.particles[:,1] = int(self.template_rect['y'] + self.template_rect['h'] / 2)

        # find out what we need to store in the particles N x 2 matrix, position, weight, action, new measurement.
        # equal weight for each particle initially
        self.weights = np.array([1.0/self.num_particles]*self.num_particles)# Initialize your weights array. Read the docstring.

        # Initialize any other components you may need when designing your filter.

        # self.state is initialized as the mean of all particles, since there are no weights yet.
        # later, the state (x,y) position of where the object is predicted to be at, will be defined
        # by the weighted mean of all the particles
        self.state = np.array([self.particles[:,0].mean(axis=None), self.particles[:,1].mean(axis=None), 0, 0])
        self.similarities = np.zeros((self.num_particles, 1))
        print(self.state)

    def resample_particles(self, weights):
        """Returns a new set of particles

        This method does not alter self.particles.

        Use self.num_particles and self.weights to return an array of
        resampled particles based on their weights.

        See np.random.choice or np.random.multinomial.
        
        Returns:
            numpy.array: particles data structure.
        """

        # select self.num_particles number of random integers from 0 to self.num_particles
        # using weights as the probability value
        result = np.random.choice(self.num_particles, size=self.num_particles, p=weights)

        return self.particles[result]

    def clip_particles(self, frame):
        self.particles[np.where(self.particles[:,0] > frame.shape[0] - 1)] = frame.shape[0] - 1
        self.particles[np.where(self.particles[:,1] > frame.shape[1] - 1)] = frame.shape[1] - 1

    def resize_template(self, frame):
        frame = frame.copy()
        # 1. check if making the template bigger makes the similarity better 
        template_expand_x_y = cv2.resize(src=self.template,
                                  dsize=(0,0),
                                  fx=self.expand_xy,
                                  fy=self.expand_xy)
        expanded_similarity_x_y = self.get_similarity_for_template(template_expand_x_y,
                                        frame,
                                        self.state[0],
                                        self.state[1])


        # 2. check if making the template smaller makes the similarity better
        template_reduce_x_y = cv2.resize(src=self.template,
                                  dsize=(0,0),
                                  fx=self.reduce_xy,
                                  fy=self.reduce_xy)
        reduced_similarity_x_y = self.get_similarity_for_template(template_reduce_x_y,
                                        frame,
                                        self.state[0],
                                        self.state[1])

        if reduced_similarity_x_y > expanded_similarity_x_y:
            self.template = template_reduce_x_y.copy()
        else:
            self.template = template_expand_x_y.copy()



class Part5ParticleFilter(MDParticleFilter):
    """A variation of particle filter tracker that can track multiple targets."""

    def __init__(self, frame, template, templates, template_rects, **kwargs):
        """Initializes MT particle filter object.
        """

        super(MDParticleFilter, self).__init__(frame, template, **kwargs)  # call base class constructor
        # If you want to add more parameters, make sure you set a default value so that
        # your test doesn't fail the autograder because of an unknown or None value.
        #
        # The way to do it is:
        # self.some_parameter_name = kwargs.get('parameter_name', default_value)
        np.random.seed(1234)
        self.frame = frame.copy()
        self.gray = False
        self.templates = []
        self.template_rects = template_rects
        self.num_particles = kwargs.get('num_particles', None)
        self.expand_xy = 1.04
        self.reduce_xy = 0.96
        for index, each in enumerate(templates):
            # self.show_img("template", each)
            t = Template(template=each.copy(),
                        particles=self.particles.copy(),
                        weights=self.weights.copy(),
                        state=self.state.copy(),
                        similarities=self.similarities.copy(),
                        template_rect=self.template_rects[index],
                        num_particles=self.num_particles)
            self.templates.append(t)

    def process(self, frame, template):
        """Processes a video frame (image) and updates the filter's state.

        This process is also inherited from ParticleFilter. Depending on your
        implementation, you may comment out this function and use helper
        methods that implement the "More Dynamics" procedure.

        Args:
            frame (numpy.array): color BGR uint8 image of current video frame,
                                 values in [0, 255].

        Returns:
            None.
        """
        # separate particles and weights are tracked for each template
        # that we are tracking
        for t in self.templates:
            if ((t.counter + 1) < t.in_frame) or ((t.counter+1) > t.out_frame):
                t.show = False
                t.counter += 1
                continue
            else:
                t.show = True
            frame = frame.copy()
            if self.gray:
                frame = frame[:,:,1] # use gray only

            # get latest center and movement. (movement is unused)
            new_x, new_y = self.get_weighted_mean(t.particles, t.weights)
            vx, vy = t.state[0] - new_x, t.state[1] - new_y

            # update state
            t.state = np.array([new_x,
                                new_y,
                                vx,
                                vy])
            t.std_x, t.std_y = np.std(t.particles, axis=0)
            
            # prepare dynamics to be applied to each particle
            dx = np.random.randint(-t.sigma_dyn*2, high=t.sigma_dyn*2, size=t.num_particles)
            dy = np.random.randint(-t.sigma_dyn, high=t.sigma_dyn, size=t.num_particles)
            # dx += int(vx)
            # dy += int(vy)

            # re-initialize particles to object's state
            # t.particles[:] = np.array([t.state[0], t.state[1]])
                                      # move the particles
            t.particles[:, 0] += dx
            t.particles[:, 1] += dy

            # iterate through all the particles, and compute similarity
            # for each particle as if cutout is centered around each 
            # of the particles
            temp_template = t.template.copy()
            for index, row in enumerate(t.particles[:, 0]):
                # get a box centered around the particle
                (x_start,
                 x_end,
                 y_start,
                 y_end) = self.get_cutout_improved(t.particles[index, 0],
                                          t.particles[index, 1],
                                          temp_template,
                                          frame)

                frame_cutout = frame[y_start:y_end, x_start:x_end]
                # ensure cutout and tmeplate are of the same size
                frame_cutout, temp_template = self.trim(frame_cutout, temp_template)

                similarity = self.get_error_metric(temp_template, frame_cutout)
                # print("similarity = {0}".format(similarity))
                
                t.similarities[index] = similarity
            
            # print("self.similarities.max() = {0}".format(round(self.similarities.max(), 2)))
            # then reweight the particles based on their similarity
            t.weights = np.array(t.similarities.flatten())

            # take the particle with the best similarity to the template, 
            # set the other particles' weights to zero.
            maxWeight = t.weights.max(axis=None)

            if not t.flag and maxWeight < 0.65:
                t.counter += 1
                continue 

            t.weights[t.weights < (maxWeight - 0.01*maxWeight)] = 0 #OPTION 1
            # t.weights[t.weights < maxWeight] = 0 # OPTION 2


            # normalize the weights
            t.weights /= t.weights.sum()

            # resample
            t.particles = t.resample_particles(t.weights)

            t.counter += 1

    def render_all(self, frame_in):
        """Visualizes current particle filter state.

        This method may not be called for all frames, so don't do any model
        updates here!

        These steps will calculate the weighted mean. The resulting values
        should represent the tracking window center point.

        In order to visualize the tracker's behavior you will need to overlay
        each successive frame with the following elements:

        - Every particle's (x, y) location in the distribution should be
          plotted by drawing a colored dot point on the image. Remember that
          this should be the center of the window, not the corner.
        - Draw the rectangle of the tracking window associated with the
          Bayesian estimate for the current location which is simply the
          weighted mean of the (x, y) of the particles.
        - Finally we need to get some sense of the standard deviation or
          spread of the distribution. First, find the distance of every
          particle to the weighted mean. Next, take the weighted sum of these
          distances and plot a circle centered at the weighted mean with this
          radius.

        This function should work for all particle filters in this problem set.

        Args:
            frame_in (numpy.array): copy of frame to render the state of the
                                    particle filter.
        """

        for t in self.templates:
            # print("t.show = {0}, counter = {1}".format(t.show, t.counter))
            if t.show:
                x_weighted_mean, y_weighted_mean = np.average(t.particles, axis=0, weights=t.weights)

                # Complete the rest of the code as instructed.
                # weighted mean = sum(x_i*w_i) / sum(w_i)
                # since sum(w_i) = 1, we already have the weighted mean

                # PL: this function is only used to generate the current positions of the particles
                # for various frames selected by the local grader. it is not used in the actual
                # implementation of the particle filter.

                # draw box equal to the template size around latest self.state
                (x_start,
                 x_end,
                 y_start,
                 y_end) = self.get_cutout_improved(x_weighted_mean,
                                          y_weighted_mean,
                                          t.template,
                                          frame_in)

                top_left = [x_start, y_start]
                top_right = [x_end, y_start]
                bottom_right = [x_end, y_end]
                bottom_left = [x_start, y_end]
                frame_in = self.draw_box(frame_in,
                                     top_left,
                                     top_right,
                                     bottom_left,
                                     bottom_right,
                                     thickness=2,
                                     copy=False)
                distances = []
                for row in t.particles:
                    frame_in[int(row[1]), int(row[0])] = (0, 255, 0)
                    # cv2.circle(img=frame_in,
                               # center=(int(row[0]), int(row[1])),
                               # radius=1,
                               # color=(0, 255, 0),
                               # thickness=2)

                    # first, find the distance of every
                    # particle to the weighted mean. 
                    # distance formula: d = sqrt((x2 - x1)**2 + (y2 - y1)**2)
                    d = np.sqrt((row[0] - x_weighted_mean)**2 + 
                                (row[1] - y_weighted_mean)**2)
                    distances.append(d)

                # second, find the weighted sum of these distances
                d_weighted_mean = np.average(distances, axis=None, weights=t.weights)

                # plot a circle that tracks the weighted mean of the particles
                cv2.circle(img=frame_in,
                           center=(int(x_weighted_mean), int(y_weighted_mean)),
                           radius=int(d_weighted_mean),
                           color=(0, 0, 255),
                           thickness=2)

class Part6ParticleFilter(MDParticleFilter):
    """A variation of particle filter tracker that can track multiple targets."""

    def __init__(self, frame, template, **kwargs):
        """Initializes MT particle filter object.
        """

        super(Part6ParticleFilter, self).__init__(frame, template, **kwargs)  # call base class constructor

        self.alpha = 0.40
        self.expand_xy = 1.03
        self.reduce_xy = 0.97
        self.counter = 0
        self.counter2 = 0

    def process(self, frame):
        """Processes a video frame (image) and updates the filter's state.

        This process is also inherited from ParticleFilter. Depending on your
        implementation, you may comment out this function and use helper
        methods that implement the "More Dynamics" procedure.

        Args:
            frame (numpy.array): color BGR uint8 image of current video frame,
                                 values in [0, 255].

        Returns:
            None.
        """
        frame = frame.copy()
        if self.gray:
            frame = frame[:,:,1] # use gray only

        self.old_template = self.template.copy()
        
        # get latest center and movement. (movement is unused)
        new_x, new_y = self.get_weighted_mean(self.particles, self.weights)
        vx, vy = self.state[0] - new_x, self.state[1] - new_y

        # update state
        self.state = np.array([new_x,
                            new_y,
                            vx,
                            vy])
        self.std_x, self.std_y = np.std(self.particles, axis=0)
        self.resize_template(frame)
        
        # prepare dynamics to be applied to each particle
        dx = np.random.randint(-self.sigma_dyn, high=self.sigma_dyn, size=self.num_particles)
        dy = np.random.randint(-self.sigma_dyn, high=self.sigma_dyn, size=self.num_particles)


        # move the particles
        self.particles[:, 0] += dx
        self.particles[:, 1] += dy

        self.clip_particles(frame)

        # iterate through all the particles, and compute similarity
        # for each particle as if cutout is centered around each 
        # of the particles
        temp_template = self.template.copy()
        for index, row in enumerate(self.particles[:, 0]):
            # get a box centered around the particle
            (x_start,
             x_end,
             y_start,
             y_end) = self.get_cutout_improved(self.particles[index, 0],
                                      self.particles[index, 1],
                                      temp_template,
                                      frame)

            frame_cutout = frame[y_start:y_end, x_start:x_end]
            # ensure cutout and tmeplate are of the same size
            frame_cutout, temp_template = self.trim(frame_cutout, temp_template)

            similarity = self.get_error_metric(temp_template, frame_cutout)
            # print("similarity = {0}".format(similarity))
            
            self.similarities[index] = similarity


        # then reweight the particles based on their similarity
        self.weights = np.array(self.similarities.flatten())

        # take the particle with the best similarity to the template, 
        # set the other particles' weights to zero.
        maxWeight = self.weights.max(axis=None)
        self.weights[self.weights < (maxWeight - 0.02*maxWeight)] = 0 #OPTION 1
        # self.weights[self.weights < maxWeight] = 0 # OPTION 2

        # normalize the weights
        self.weights /= self.weights.sum()

        # resample
        self.particles = self.resample_particles(self.weights)

        # large alpha = objects that move quickly
        # small alpha = objects that move slowly
        if self.counter2 % 2 == 0:
            maxWeight = self.weights.max(axis=None)
            indices = np.argwhere(self.weights == maxWeight)
            best_particles = self.particles[indices]
            mean_particle_coords = best_particles.mean(axis=0)
            mean_x = int(mean_particle_coords[0][0])
            mean_y = int(mean_particle_coords[0][1])
            (x_start,
             x_end,
             y_start,
             y_end) = self.get_cutout(mean_x,
                                      mean_y,
                                      self.template, # used for shape only
                                      frame)

            best_t = frame[y_start:y_end, x_start:x_end]
            best_t, self.template = self.trim(best_t, self.template)
            self.template = self.alpha*best_t + (1 - self.alpha)*self.template

        self.counter += 1
        self.counter2 += 1

    def render(self, frame_in):
        """Visualizes current particle filter state.

        This method may not be called for all frames, so don't do any model
        updates here!

        These steps will calculate the weighted mean. The resulting values
        should represent the tracking window center point.

        In order to visualize the tracker's behavior you will need to overlay
        each successive frame with the following elements:

        - Every particle's (x, y) location in the distribution should be
          plotted by drawing a colored dot point on the image. Remember that
          this should be the center of the window, not the corner.
        - Draw the rectangle of the tracking window associated with the
          Bayesian estimate for the current location which is simply the
          weighted mean of the (x, y) of the particles.
        - Finally we need to get some sense of the standard deviation or
          spread of the distribution. First, find the distance of every
          particle to the weighted mean. Next, take the weighted sum of these
          distances and plot a circle centered at the weighted mean with this
          radius.

        This function should work for all particle filters in this problem set.

        Args:
            frame_in (numpy.array): copy of frame to render the state of the
                                    particle filter.
        """

        x_weighted_mean, y_weighted_mean = np.average(self.particles, axis=0, weights=self.weights)

        # Complete the rest of the code as instructed.
        # weighted mean = sum(x_i*w_i) / sum(w_i)
        # since sum(w_i) = 1, we already have the weighted mean

        # PL: this function is only used to generate the current positions of the particles
        # for various frames selected by the local grader. it is not used in the actual
        # implementation of the particle filter.

        # draw box equal to the template size around latest self.state
        (x_start,
         x_end,
         y_start,
         y_end) = self.get_cutout(x_weighted_mean,
                                  y_weighted_mean,
                                  self.template,
                                  frame_in)

        top_left = [x_start, y_start]
        top_right = [x_end, y_start]
        bottom_right = [x_end, y_end]
        bottom_left = [x_start, y_end]
        frame_in = self.draw_box(frame_in,
                             top_left,
                             top_right,
                             bottom_left,
                             bottom_right,
                             thickness=2,
                             copy=False)
        distances = []
        self.clip_particles_new(frame_in)
        for row in self.particles:
            frame_in[int(row[1]), int(row[0])] = (0, 255, 0)
            # cv2.circle(img=frame_in,
                       # center=(int(row[0]), int(row[1])),
                       # radius=1,
                       # color=(0, 255, 0),
                       # thickness=2)

            # first, find the distance of every
            # particle to the weighted mean. 
            # distance formula: d = sqrt((x2 - x1)**2 + (y2 - y1)**2)
            d = np.sqrt((row[0] - x_weighted_mean)**2 + 
                        (row[1] - y_weighted_mean)**2)
            distances.append(d)

        # second, find the weighted sum of these distances
        d_weighted_mean = np.average(distances, axis=None, weights=self.weights)

        # plot a circle that tracks the weighted mean of the particles
        cv2.circle(img=frame_in,
                   center=(int(x_weighted_mean), int(y_weighted_mean)),
                   radius=int(d_weighted_mean),
                   color=(0, 0, 255),
                   thickness=2)

    def clip_particles_new(self, frame):
        self.particles[np.where(self.particles[:,0] > frame.shape[1] - 1)] = frame.shape[1] - 1
        self.particles[np.where(self.particles[:,1] > frame.shape[0] - 1)] = frame.shape[0] - 1
