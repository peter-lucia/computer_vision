"""Problem Set 6: PCA, Boosting, Haar Features, Viola-Jones."""
import cv2
import os
import numpy as np
import matplotlib.pyplot as plt
import math
import time

import ps6

# I/O directories
INPUT_DIR = "input_images"
OUTPUT_DIR = "output"

YALE_FACES_DIR = os.path.join(INPUT_DIR, 'Yalefaces')
FACES94_DIR = os.path.join(INPUT_DIR, 'faces94')
POS_DIR = os.path.join(INPUT_DIR, "pos")
NEG_DIR = os.path.join(INPUT_DIR, "neg")
NEG2_DIR = os.path.join(INPUT_DIR, "neg2")


def load_images_from_dir(data_dir, size=(24, 24), ext=".png"):
    imagesFiles = [f for f in os.listdir(data_dir) if f.endswith(ext)]
    imgs = [np.array(cv2.imread(os.path.join(data_dir, f), 0)) for f in imagesFiles]
    imgs = [cv2.resize(x, size) for x in imgs]

    return imgs

# Utility function
def plot_eigen_faces(eig_vecs, fig_name="", visualize=False):
    r = np.ceil(np.sqrt(len(eig_vecs)))
    c = int(np.ceil(len(eig_vecs)/r))
    r = int(r)
    fig = plt.figure()

    for i,v in enumerate(eig_vecs):
        sp = fig.add_subplot(r,c,i+1)

        plt.imshow(v.reshape(32,32).real, cmap='gray')
        sp.set_title('eigenface_%i'%i)
        sp.axis('off')

    fig.subplots_adjust(hspace=.5)

    if visualize:
        plt.show()

    if not fig_name == "":
        plt.savefig("output/{}".format(fig_name))


# Functions you need to complete
def visualize_mean_face(x_mean, size, new_dims):
    """Rearrange the data in the mean face to a 2D array

    - Organize the contents in the mean face vector to a 2D array.
    - Normalize this image.
    - Resize it to match the new dimensions parameter

    Args:
        x_mean (numpy.array): Mean face values.
        size (tuple): x_mean 2D dimensions
        new_dims (tuple): Output array dimensions

    Returns:
        numpy.array: Mean face uint8 2D array.
    """
    x_mean = x_mean.copy()

    # x_mean is a (32, 32, 3) flattened image
    x_mean = np.reshape(x_mean,(size[0], size[1]))
    # ps6.show_img("x_mean", x_mean)
    
    x_mean = cv2.resize(x_mean,
            dsize=new_dims)
    return x_mean


def part_1a_1b():

    orig_size = (192, 231)
    small_size = (32, 32)
    X, y = ps6.load_images(YALE_FACES_DIR, small_size)

    # Get the mean face
    x_mean = ps6.get_mean_face(X)

    x_mean_image = visualize_mean_face(x_mean, small_size, orig_size)

    cv2.imwrite(os.path.join(OUTPUT_DIR, "ps6-1-a-1.png"), x_mean_image)

    # PCA dimension reduction
    k = 10
    eig_vecs, eig_vals = ps6.pca(X, k)

    plot_eigen_faces(eig_vecs.T, "ps6-1-b-1.png")


def part_1c():
    np.random.seed(123456)
    for k in [2, 5, 10]:
        for p in [0.25, 0.5, 0.75]:
            non_random_accuracies = []
            random_accuracies = []
            for i in range(0, 5):
                # print("-- Test #{0} -- ".format(i+1))
                # p = 0.5  # Select a split percentage value
                # k = 5  # Select a value for k

                size = [32, 32]
                X, y = ps6.load_images(YALE_FACES_DIR, size)
                Xtrain, ytrain, Xtest, ytest = ps6.split_dataset(X, y, p)

                # training
                mu = ps6.get_mean_face(Xtrain)
                eig_vecs, eig_vals = ps6.pca(Xtrain, k)
                Xtrain_proj = np.dot(Xtrain - mu, eig_vecs)

                # testing
                mu = ps6.get_mean_face(Xtest)
                Xtest_proj = np.dot(Xtest - mu, eig_vecs)

                good = 0
                bad = 0

                for i, obs in enumerate(Xtest_proj):

                    dist = [np.linalg.norm(obs - x) for x in Xtrain_proj]

                    idx = np.argmin(dist)
                    y_pred = ytrain[idx]

                    if y_pred == ytest[i]:
                        good += 1

                    else:
                        bad += 1

                # print("Non-random predictions: ")
                # print 'Good predictions = ', good, 'Bad predictions = ', bad
                non_random_accuracy = 100 * float(good) / (good + bad)
                non_random_accuracies.append(non_random_accuracy)
                # print '{0:.2f}% accuracy'.format(non_random_accuracy)

                good = 0
                bad = 0

                for i, each in enumerate(Xtest_proj):
                    y_pred = np.random.randint(0, 16)
                    if y_pred == ytest[i]:
                        good += 1
                    else:
                        bad += 1

                # print("Random predictions: ")
                # print 'Good predictions = ', good, 'Bad predictions = ', bad
                random_accuracy = 100 * float(good) / (good + bad)
                random_accuracies.append(random_accuracy)
                # print '{0:.2f}% accuracy'.format(random_accuracy)

            avg_non_random = round(float(sum(non_random_accuracies)) / len(non_random_accuracies), 2)
            print("Avg non-random accuracy for k = {0}, p = {1} is {2}%".format(k, p, avg_non_random))
            avg_random = round(float(sum(random_accuracies)) / len(random_accuracies), 2)
            print("Avg random accuracy for     k = {0}, p = {1} is {2}%".format(k, p, avg_random))


def get_accuracy(Xtrain, Ytrain, Xtest, Ytest):
    Xtrain = Xtrain.copy()
    Ytrain = Ytrain.copy()
    Xtest = Xtest.copy()
    good = 0
    bad = 0

    for i, obs in enumerate(Xtest):

        # compare each observed test value to the training value
        # and get the frobenius norm (sqrt(sum of absolute squares 
        # of the distance b/t observation Xtest and Xtrain))
        dist = [np.linalg.norm(obs - x) for x in Xtrain]

        # get the index where Xtest is closest to Xtrain
        idx = np.argmin(dist)

        # use that index to extract the best ytrain value
        y_pred = Ytrain[idx]

        # increment good 
        if y_pred == Ytest[i]:
            good += 1

        else:
            bad += 1

    print 'Good predictions = ', good, 'Bad predictions = ', bad
    return 100 * float(good) / (good + bad) 


def part_2a():
    y0 = 1
    y1 = 2

    X, y = ps6.load_images(FACES94_DIR)

    # Select only the y0 and y1 classes
    idx = y == y0
    idx |= y == y1

    X = X[idx,:]
    y = y[idx]

    # Label them 1 and -1
    y0_ids = y == y0
    y1_ids = y == y1
    y[y0_ids] = 1
    y[y1_ids] = -1

    np.random.seed(123456)
    p = 0.8 # percentage of data that will be training data
    Xtrain, ytrain, Xtest, ytest = ps6.split_dataset(X, y, p)

    # Picking random numbers
    rand_y = np.random.choice([-1, 1], (len(ytrain)))
    # TODO: find which of these labels match ytrain and report its accuracy

    # y consists of non-random set of 1, or -1 based on the output of ps6.load_images
    # 1 or -1 will attempt to classify a face as either male or female

    incorrect = np.argwhere(ytrain != rand_y).shape[0]
    correct = rand_y.shape[0] - incorrect
    rand_accuracy = 100 * float(correct) / (correct + incorrect)
    print '(Random) Training accuracy: {0:.2f}%'.format(rand_accuracy)

    # Using Weak Classifier
    uniform_weights = np.ones((Xtrain.shape[0],)) / Xtrain.shape[0]
    wk_clf = ps6.WeakClassifier(Xtrain, ytrain, uniform_weights)
    wk_clf.train()
    wk_results = [wk_clf.predict(x) for x in Xtrain]
    # TODO: find which of these labels match ytrain and report its accuracy

    incorrect = np.argwhere(ytrain != np.array(wk_results)).shape[0]
    correct = len(wk_results) - incorrect
    wk_accuracy = 100 * float(correct) / (correct + incorrect)
    print '(Weak) Training accuracy {0:.2f}%'.format(wk_accuracy)

    # num_iter = 5
    num_iter = 100

    boost = ps6.Boosting(Xtrain, ytrain, num_iter)
    boost.train()
    good, bad = boost.evaluate()
    boost_accuracy = 100 * float(good) / (good + bad)
    print '(Boosting) Training accuracy {0:.2f}%'.format(boost_accuracy)

    # Picking random numbers
    rand_y = np.random.choice([-1, 1], (len(ytest)))
    # TODO: find which of these labels match ytest and report its accuracy
    incorrect = np.argwhere(ytest != rand_y).shape[0]
    correct = rand_y.shape[0] - incorrect
    rand_accuracy = 100 * float(correct) / (correct + incorrect)
    print '(Random) Testing accuracy: {0:.2f}%'.format(rand_accuracy)

    # Using Weak Classifier
    wk_results = [wk_clf.predict(x) for x in Xtest]
    # TODO: find which of these labels match ytest and report its accuracy

    incorrect = np.argwhere(ytest != np.array(wk_results)).shape[0]
    correct = len(wk_results) - incorrect
    wk_accuracy = 100 * float(correct) / (correct + incorrect)
    print '(Weak) Testing accuracy {0:.2f}%'.format(wk_accuracy)

    y_pred = boost.predict(Xtest)
    # TODO: find which of these labels match ytest and report its accuracy
    incorrect = np.argwhere(ytest != y_pred).shape[0]
    correct = y_pred.shape[0] - incorrect
    boost_accuracy = 100 * float(correct) / (correct + incorrect)
    print '(Boosting) Testing accuracy {0:.2f}%'.format(boost_accuracy)
    # raw_input("Press enter to continue...")


def part_3a():
    """Complete the remaining parts of this section as instructed in the
    instructions document."""

    # HaarFeature(feat_type, position, size)
    # two horizontal
    feature1 = ps6.HaarFeature((2, 1), (25, 30), (50, 100))
    feature1.preview((200, 200), filename="ps6-3-a-1")

    # two vertical
    feature2 = ps6.HaarFeature((1,2), (10, 25), (50, 150))
    feature2.preview((200, 200), filename="ps6-3-a-2")

    # three horizontal
    feature3 = ps6.HaarFeature((3,1), (50,50), (100, 50))
    feature3.preview((200, 200), filename="ps6-3-a-3")

    # three vertical
    feature4 = ps6.HaarFeature((1,3), (50, 125), (100, 50))
    feature4.preview((200, 200), filename="ps6-3-a-4")

    # four square
    feature5 = ps6.HaarFeature((2,2), (50,25), (100, 150))
    feature5.preview((200, 200), filename="ps6-3-a-5")

def part_3c():

    test_count = 8 
    for i in range(0, test_count):
        M = (i+1)*1000
        N = (i+1)*1000
        haar_m = 50
        haar_n = 50 

        image = np.ones((M,N))

        integral_images = ps6.convert_images_to_integral_images([image])
        # HaarFeature(feature type, position, size)
        feature = ps6.HaarFeature((3, 1), (20, 20), [M-haar_m, N-haar_n])
        feature.preview(shape=(haar_m,haar_n))

        np_sum_start = time.time()
        feature.evaluate_slow(image)
        np_sum_end = time.time()

        ii_start = time.time()
        feature.evaluate(integral_images[0])
        ii_end = time.time()

        ii_duration = round(ii_end - ii_start, 2)
        np_sum_duration = round(np_sum_end - np_sum_start, 2)
        if ii_duration <= np_sum_duration:
            print(("Part 3c - Test {0}/{1}. Image Size: {2} "
                    "Using integral images "
                    "was {3}s faster than using np.sum.").format(i+1,
                                                                test_count,
                                                                (M, N),
                                                                np_sum_duration - ii_duration))
        else:
            print(("Part 3c - Test {0}/{1}. Image Size: {2} "
                "np.sum was {3}s faster "
                  "time than using integral images.").format(i+1,
                                                            test_count,
                                                            (M, N),
                                                            ii_duration - np_sum_duration))



def part_4_a_b():

    pos = load_images_from_dir(POS_DIR)
    # pos = [each.astype("float64") for each in pos]
    neg = load_images_from_dir(NEG_DIR)
    # neg = [each.astype("float64") for each in neg]

    train_pos = pos[:35]
    train_neg = neg[:]
    images = train_pos + train_neg
    labels = np.array(len(train_pos) * [1] + len(train_neg) * [-1])

    integral_images = ps6.convert_images_to_integral_images(images)
    VJ = ps6.ViolaJones(train_pos, train_neg, integral_images)
    VJ.createHaarFeatures()

    # VJ.train(4)
    VJ.train(5) # report accuracy with num_classifiers set to 5

    VJ.haarFeatures[VJ.classifiers[0].feature].preview(filename="ps6-4-b-1")
    VJ.haarFeatures[VJ.classifiers[1].feature].preview(filename="ps6-4-b-2")

    predictions = VJ.predict(images)

    predictions = np.array(predictions)
    incorrect = np.argwhere(labels != predictions).shape[0]
    correct = predictions.shape[0] - incorrect
    vj_accuracy = 100 * float(correct) / (correct + incorrect)
    print "Prediction accuracy on training: {0:.2f}%".format(vj_accuracy)

    neg = load_images_from_dir(NEG2_DIR)

    test_pos = pos[35:]
    test_neg = neg[:35]
    test_images = test_pos + test_neg
    real_labels = np.array(len(test_pos) * [1] + len(test_neg) * [-1])
    predictions = VJ.predict(test_images)

    predictions = np.array(predictions)
    incorrect = np.argwhere(real_labels != predictions).shape[0]
    correct = predictions.shape[0] - incorrect
    vj_accuracy = 100 * float(correct) / (correct + incorrect)
    print "Prediction accuracy on testing: {0:.2f}%".format(vj_accuracy)


def part_4_c():
    # pos = load_images_from_dir(POS_DIR)[:20]
    POS2_DIR = os.path.join(INPUT_DIR, "pos2")
    pos = load_images_from_dir(POS2_DIR)
    # pos = load_images_from_dir(POS_DIR)
    # pos = [each.astype("float64") for each in pos]
    if True:
        # raw_input(pos)
        temp = cv2.imread(os.path.join(INPUT_DIR, "man.jpeg"), 0)
        temp = temp[63:63+57, 107:107+55]
        # ps6.show_img("face", temp)
        temp = cv2.resize(temp, dsize=(24,24))
        # ps6.show_img("face", temp)
        # pos.append(temp)
        pos += [temp]*20
    neg = load_images_from_dir(NEG_DIR)
    # neg = [each.astype("float64") for each in neg]

    images = pos + neg

    integral_images = ps6.convert_images_to_integral_images(images)
    VJ = ps6.ViolaJones(pos, neg, integral_images)
    VJ.createHaarFeatures()

    VJ.train(4)

    image = cv2.imread(os.path.join(INPUT_DIR, "man.jpeg"), -1)
    image = cv2.resize(image, (120, 60))
    VJ.faceDetection(image, filename="ps6-4-c-1")


if __name__ == "__main__":
    part_1a_1b()
    part_1c()
    part_2a()
    part_3a()
    part_3c()
    part_4_a_b()
    part_4_c()
