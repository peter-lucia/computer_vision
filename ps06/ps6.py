"""Problem Set 6: PCA, Boosting, Haar Features, Viola-Jones."""
import numpy as np
import cv2
import os

from helper_classes import WeakClassifier, VJ_Classifier


# assignment code
def load_images(folder, size=(32, 32)):
    """Load images to workspace.

    Args:
        folder (String): path to folder with images.
        size   ([int]): new image sizes

    Returns:
        tuple: two-element tuple containing:
            X (numpy.array): data matrix of flatten images
                             (row:observations, col:features) (float).
            y (numpy.array): 1D array of labels (int).
    """

    images_files = [f for f in os.listdir(folder) if f.endswith(".png")]

    images = []
    labels = []
    for filename in images_files:
        # cv2.IMREAD_GRAYSCALE = 0
        # cv2.IMREAD_COLOR = 1
        # cv2.IMREAD_UNCHANGED = -1
        # frame = cv2.imread(os.path.join(folder, filename), cv2.IMREAD_GRAYSCALE)
        frame = cv2.imread(os.path.join(folder, filename), 0)
        frame = frame.astype('float64')
        frame = cv2.resize(frame,
                           dsize=tuple(size),
                           interpolation=cv2.INTER_CUBIC)
        frame = frame.flatten()
        images.append(frame)
        labels.append(int(filename.split(".")[0].split("subject")[1]))

    X = np.array(images)
    labels = np.array(labels)
    return X, labels


def split_dataset(X, y, p):
    """Split dataset into training and test sets.

    Let M be the number of images in X, select N random images that will
    compose the training data (see np.random.permutation). The images that
    were not selected (M - N) will be part of the test data. Record the labels
    accordingly.

    Args:
        X (numpy.array): 2D dataset.
        y (numpy.array): 1D array of labels (int).
        p (float): Decimal value that determines the percentage of the data
                   that will be the training data.

    Returns:
        tuple: Four-element tuple containing:
            Xtrain (numpy.array): Training data 2D array.
            ytrain (numpy.array): Training data labels.
            Xtest (numpy.array): Test data test 2D array.
            ytest (numpy.array): Test data labels.
    """
    X = X.copy()

    M = X.shape[0]
    # N = number of images in the training data
    # M - N = number of images in the test data
    N = int(M*p)

    # get permutation
    permutation = np.random.permutation(len(y))

    for old_idx, new_idx in enumerate(permutation):
        X[old_idx] = X[new_idx]
        y[old_idx] = y[new_idx]
    
    Xtrain = X[0:N]
    ytrain = y[0:N]

    Xtest = X[N:M]
    ytest = y[N:M]

    return Xtrain, ytrain, Xtest, ytest


def get_mean_face(x):
    """Return the mean face.

    Calculate the mean for each column.

    Args:
        x (numpy.array): array of flattened images.

    Returns:
        numpy.array: Mean face.
    """
    # return x.mean(axis=0)
    return np.average(x, axis=0)



def pca(X, k):
    """PCA Reduction method.

    Return the top k eigenvectors and eigenvalues using the covariance array
    obtained from X.


    Args:
        X (numpy.array): 2D data array of flatten images (row:observations,
                         col:features) (float).
        k (int): new dimension space

    Returns:
        tuple: two-element tuple containing
            eigenvectors (numpy.array): 2D array with the top k eigenvectors.
            eigenvalues (numpy.array): array with the top k eigenvalues.
    """
    X = X.copy()
    u = get_mean_face(X)

    A = X - u
    sigma = np.dot(A.T, A)
    evals, evectors = np.linalg.eigh(sigma)


    evectors = evectors[:, :-(k+1):-1]
    # alternative method of flipping eigenvectors
    # and getting the top k
    # evectors = np.flip(evectors, axis=1)
    # evectors = evectors[:,0:k]

    evals = evals[::-1]
    evals = evals[0:k]
    return evectors, evals

class Boosting:
    """Boosting classifier.

    Args:
        X (numpy.array): Data array of flattened images
                         (row:observations, col:features) (float).
        y (numpy.array): Labels array of shape (observations, ).
        num_iterations (int): number of iterations
                              (ie number of weak classifiers).

    Attributes:
        Xtrain (numpy.array): Array of flattened images (float32).
        ytrain (numpy.array): Labels array (float32).
        num_iterations (int): Number of iterations for the boosting loop.
        weakClassifiers (list): List of weak classifiers appended in each
                               iteration.
        alphas (list): List of alpha values, one for each classifier.
        num_obs (int): Number of observations.
        weights (numpy.array): Array of normalized weights, one for each
                               observation.
        eps (float): Error threshold value to indicate whether to update
                     the current weights or stop training.
    """

    def __init__(self, X, y, num_iterations):
        self.Xtrain = np.float32(X)
        self.ytrain = np.float32(y) # : Training data labels.
        self.num_iterations = num_iterations
        self.weakClassifiers = []
        self.alphas = []
        self.num_obs = X.shape[0]
        self.weights = np.array([1.0 / self.num_obs] * self.num_obs)  # uniform weights
        self.eps = 0.0001

    def train(self):
        """Implement the for loop shown in the problem set instructions."""

        self.weakClassifiers = []
        self.alphas = [1]*self.num_obs

        for j in range(0, self.num_iterations):
            # print("j = {0}".format(j))
            # print("self.weights.sum() = {0}".format(self.weights.sum()))
            # normalize the weights
            self.weights /= self.weights.sum()

            # instantiate and train weak classifier h
            h = WeakClassifier(self.Xtrain,
                               self.ytrain,
                               self.weights)
            h.train()

            self.weakClassifiers.append(h)

            # get predicted labels
            y_pred = self.predict(self.Xtrain)

            # get indices where the predicted labels of the images
            # don't match the training set labels
            error_idx = np.argwhere(y_pred != self.ytrain)

            # get the sum of weights where the predicted labels
            # were wrong
            epsilon = self.weights[error_idx].sum(axis=None)

            alpha = 0.5*(np.log((1 - epsilon)/epsilon))
            self.alphas.append(alpha)

            # if enough of the predictions were wrong
            # update the weights and continue
            if epsilon > self.eps:
                # update the weights
                self.weights = self.weights*np.exp(-(self.ytrain*alpha)*(y_pred))
                # print("Updated the weights.")
                # print("self.weights = {0}".format(self.weights))
            # enough of the predictions were correct, we can stop.
            else:
                # print("Enough of the predictions were correct. Stopping.")
                break

    def evaluate(self):
        """Return the number of correct and incorrect predictions.

        Use the training data (self.Xtrain) to obtain predictions. Compare
        them with the training labels (self.ytrain) and return how many
        were correct and incorrect.

        Returns:
            tuple: two-element tuple containing:
                correct (int): Number of correct predictions.
                incorrect (int): Number of incorrect predictions.
        """

        y_pred = self.predict(self.Xtrain)
        incorrect = np.argwhere(y_pred != self.ytrain).shape[0]
        correct = self.ytrain.shape[0] - incorrect
        
        return correct, incorrect

    def predict(self, X):
        """Return predictions for a given array of observations.

        Use the alpha values stored in self.aphas and the weak classifiers
        stored in self.weakClassifiers.

        Args:
            X (numpy.array): Array of flattened images (observations).

        Returns:
            numpy.array: Predictions, one for each row in X.
        """

        X = X.copy()

        H = []
        for i in range(0, X.shape[0]):
            predictions = []
            for j in range(0, len(self.weakClassifiers)):
                # get a prediction of this image from all the weak classifiers
                predictions.append(self.alphas[j]*self.weakClassifiers[j].predict(X[i]))

            predictions = np.array(predictions)

            # take the sum of the predictions
            H.append(np.sign(predictions.sum()))
        return np.array(H)



class HaarFeature:
    """Haar-like features.

    Args:
        feat_type (tuple): Feature type {(2, 1), (1, 2), (3, 1), (2, 2)}.
        position (tuple): (row, col) position of the feature's top left corner.
        size (tuple): Feature's (height, width)

    Attributes:
        feat_type (tuple): Feature type.
        position (tuple): Feature's top left corner.
        size (tuple): Feature's width and height.
    """

    def __init__(self, feat_type, position, size):
        self.feat_type = feat_type
        self.position = position
        self.size = size

    def _create_two_horizontal_feature(self, shape):
        """Create a feature of type (2, 1).

        Use int division to obtain half the height.

        Args:
            shape (tuple):  Array numpy-style shape (rows, cols).

        Returns:
            numpy.array: Image containing a Haar feature. (uint8).
        """
        # Each method should
        # create an 2D zeros array. Each feature is made of a white area (255)
        # and a gray area (126).

        result = np.zeros(shape) # shape is the size of the resultant image
        w_start_row = self.position[0]
        w_start_col = self.position[1]
        w_end_row = self.position[0] + self.size[0]
        w_end_col = self.position[1] + self.size[1]

        g_start_row = self.position[0] + int(self.size[0] / 2)
        g_end_row = self.position[0] + self.size[0]
        g_start_col = self.position[1]
        g_end_col = self.position[1] + self.size[1]

        # print("Create two vertical feature...")
        # print("self.position = {0}".format(self.position))
        # print("self.size = {0}".format(self.size))
        # print("shape = {0}".format(shape))
        # print("white range: [{0}:{1},{2}:{3}] = 255".format(w_start_row,
            # w_end_row,
            # w_start_col,
            # w_end_col))
        # raw_input("gray range: [{0}:{1}, {2}:{3}] = 126".format(g_start_row,
            # g_end_row,
            # g_start_col,
            # g_end_col))
        result[w_start_row:w_end_row, w_start_col:w_end_col] = 255
        result[g_start_row:g_end_row, g_start_col:g_end_col] = 126

        


        result = result.astype('uint8')
        # show_img("result", result)
        return result

    def _create_two_vertical_feature(self, shape):
        """Create a feature of type (1, 2).

        Use int division to obtain half the width.

        Args:
            shape (tuple):  Array numpy-style shape (rows, cols).

        Returns:
            numpy.array: Image containing a Haar feature. (uint8).
        """
        result = np.zeros((shape[0], shape[1]))
        # white bar height range
        # x = self.position[0]
        # y = self.position[1]

        # self.size = (height, width) of the area the white + gray bars will occupy
        # self.postion = top left corner (row, col) of the feature in the image

        w_start_row = self.position[0]
        w_end_row = self.position[0] + self.size[0] # height of white bar

        # white bar width range
        w_start_col = self.position[1]
        w_end_col = self.position[1] + self.size[1] # width of white bar

        # gray bar height range
        g_start_row = self.position[0]
        g_end_row = self.position[0] + self.size[0]

        # gray bar width range
        g_start_col = self.position[1] + int(self.size[1] / 2)
        g_end_col = self.position[1] + self.size[1]

        # print("Create two vertical feature...")
        # print("self.position = {0}".format(self.position))
        # print("self.size = {0}".format(self.size))
        # print("shape = {0}".format(shape))
        # print("white range: [{0}:{1},{2}:{3}] = 255".format(w_start_row,
            # w_end_row,
            # w_start_col,
            # w_end_col))
        # raw_input("gray range: [{0}:{1}, {2}:{3}] = 126".format(g_start_row,
            # g_end_row,
            # g_start_col,
            # g_end_col))
        result[w_start_row:w_end_row, w_start_col:w_end_col] = 255
        result[g_start_row:g_end_row, g_start_col:g_end_col] = 126

        result = result.astype('uint8')
        # show_img("result", result)
        return result

    def _create_three_horizontal_feature(self, shape):
        """Create a feature of type (3, 1).

        Use int division to obtain a third of the height.

        Args:
            shape (tuple):  Array numpy-style shape (rows, cols).

        Returns:
            numpy.array: Image containing a Haar feature. (uint8).
        """

        result = np.zeros(shape)
        # paint the whole square white
        w_start_row = self.position[0]
        w_end_row = self.position[0] + self.size[0]
        w_start_col = self.position[1]
        w_end_col = self.position[1] + self.size[1]

        # paint the middle vert rectangle gray
        g_start_row = self.position[0] + int(self.size[0] / 3)
        # g_end_row = self.position[0] + int(2*self.size[0] / 3)
        g_end_row = g_start_row + int(self.size[0] / 3)
        g_start_col = self.position[1]
        g_end_col = self.position[1] + self.size[1]

        result[w_start_row:w_end_row, w_start_col:w_end_col] = 255
        result[g_start_row:g_end_row, g_start_col:g_end_col] = 126
        result = result.astype('uint8')
        # show_img("result", result)
        return result

    def _create_three_vertical_feature(self, shape):
        """Create a feature of type (1, 3).

        Use int division to obtain a third of the width.

        Args:
            shape (tuple):  Array numpy-style shape (rows, cols).

        Returns:
            numpy.array: Image containing a Haar feature. (uint8).
        """

        result = np.zeros(shape)
        # paint the whole square white
        w_start_row = self.position[0]
        w_end_row = self.position[0] + self.size[0]
        w_start_col = self.position[1]
        w_end_col = self.position[1] + self.size[1]

        # paint the middle vert rectangle gray
        g_start_row = self.position[0]
        g_end_row = self.position[0] + self.size[0]
        g_start_col = self.position[1] + int(self.size[1] / 3)
        g_end_col = self.position[1] + int(2*self.size[1] / 3)

        result[w_start_row:w_end_row, w_start_col:w_end_col] = 255
        result[g_start_row:g_end_row, g_start_col:g_end_col] = 126

        result = result.astype('uint8')
        # show_img("result", result)
        return result

    def _create_four_square_feature(self, shape):
        """Create a feature of type (2, 2).

        Use int division to obtain half the width and half the height.

        Args:
            shape (tuple):  Array numpy-style shape (rows, cols).

        Returns:
            numpy.array: Image containing a Haar feature. (uint8).
        """

        result = np.zeros(shape)
        # paint the whole square white
        w_start_row = self.position[0]
        w_end_row = self.position[0] + self.size[0]
        w_start_col = self.position[1]
        w_end_col = self.position[1] + self.size[1]

        result[w_start_row:w_end_row, w_start_col:w_end_col] = 255

        # paint the top left rectangle gray
        g_start_row = self.position[0]
        g_end_row = self.position[0] + int(self.size[0] / 2)
        g_start_col = self.position[1]
        g_end_col = self.position[1] + int(self.size[1] / 2)
        result[g_start_row:g_end_row, g_start_col:g_end_col] = 126

        # paint the bottom right rectangle gray

        g_start_row = self.position[0] + int(self.size[0] / 2)
        g_end_row = self.position[0] + self.size[0]
        g_start_col = self.position[1] + int(self.size[1] / 2)
        g_end_col = self.position[1] + self.size[1]
        result[g_start_row:g_end_row, g_start_col:g_end_col] = 126

        result = result.astype('uint8')
        # show_img("result", result)
        return result

    def preview(self, shape=(24, 24), filename=None):
        """Return an image with a Haar-like feature of a given type.

        Function that calls feature drawing methods. Each method should
        create an 2D zeros array. Each feature is made of a white area (255)
        and a gray area (126).

        The drawing methods use the class attributes position and size.
        Keep in mind these are in (row, col) and (height, width) format.

        Args:
            shape (tuple): Array numpy-style shape (rows, cols).
                           Defaults to (24, 24).

        Returns:
            numpy.array: Array containing a Haar feature (float or uint8).
        """

        if self.feat_type == (2, 1):  # two_horizontal
            X = self._create_two_horizontal_feature(shape)

        if self.feat_type == (1, 2):  # two_vertical
            X = self._create_two_vertical_feature(shape)

        if self.feat_type == (3, 1):  # three_horizontal
            X = self._create_three_horizontal_feature(shape)

        if self.feat_type == (1, 3):  # three_vertical
            X = self._create_three_vertical_feature(shape)

        if self.feat_type == (2, 2):  # four_square
            X = self._create_four_square_feature(shape)

        if filename is None:
            cv2.imwrite("output/{}_feature.png".format(self.feat_type), X)

        else:
            cv2.imwrite("output/{}.png".format(filename), X)

        return X

    def evaluate(self, ii):
        """Evaluates a feature's score on a given integral image.

        Calculate the score of a feature defined by the self.feat_type.
        Using the integral image and the sum / subtraction of rectangles to
        obtain a feature's value. Add the feature's white area value and
        subtract the gray area.

        For example, on a feature of type (2, 1):
        score = sum of pixels in the white area - sum of pixels in the gray area

        Keep in mind you will need to use the rectangle sum / subtraction
        method and not numpy.sum(). This will make this process faster and
        will be useful in the ViolaJones algorithm.

        Args:
            ii (numpy.array): Integral Image.

        Returns:
            float: Score value.
        """

        pos = self.position
        pos = (pos[0] - 1, pos[1] - 1)

        size = self.size
        # ii = ii.astype("uint8")
        ii = ii.copy()
        # raw_input("np.max(ii) = {0}".format(np.max(ii)))
        ii = ii.astype(int)
        # print("ii.dtype = {0}".format(ii.dtype))


        if self.feat_type == (2, 1): # two horizontal

            # ii[up and down, left to right]
            ii_1 = ii[pos[0], pos[1]] # top left point
            ii_2 = ii[pos[0], pos[1] + size[1]] # top right
            ii_3 = ii[pos[0] + size[0] / 2, pos[1]] # left vertical mid point
            ii_4 = ii[pos[0] + size[0] / 2, pos[1] + size[1]] # right vertical mid point

            sum_A = ii_4 - ii_2 - ii_3 + ii_1

            ii_1 = ii[pos[0] + size[0] / 2, pos[1]] # left vertical mid point
            ii_2 = ii[pos[0] + size[0] / 2, pos[1] + size[1]] # right vertical mid point
            ii_3 = ii[pos[0] + size[0], pos[1]] # bottom left
            ii_4 = ii[pos[0] + size[0], pos[1] + size[1]] # bottom right
            sum_B = ii_4 - ii_2 - ii_3 + ii_1

            score = sum_A - sum_B

        if self.feat_type == (1, 2): # two vertical

            # ii[up and down, left to right]
            ii_1 = ii[pos[0], pos[1]] # top left point
            ii_2 = ii[pos[0], pos[1] + size[1] / 2] # top horizontal mid point
            ii_3 = ii[pos[0] + size[0], pos[1]] # bottom left
            ii_4 = ii[pos[0] + size[0], pos[1] + size[1] / 2] # bottom horizontal mid point

            sum_A = ii_4 - ii_2 - ii_3 + ii_1

            ii_1 = ii[pos[0], pos[1] + size[1] / 2] # top horizontal mid point
            ii_2 = ii[pos[0], pos[1] + size[1]] # top right
            ii_3 = ii[pos[0] + size[0], pos[1] + size[1] / 2] # bottom horizontal mid point
            ii_4 = ii[pos[0] + size[0], pos[1] + size[1]] # bottom right

            sum_B = ii_4 - ii_2 - ii_3 + ii_1
            
            score = sum_A - sum_B

        if self.feat_type == (3, 1): # three horizontal

            # ii[up and down, left to right]
            ii_1 = ii[pos[0], pos[1]] # top left point
            ii_2 = ii[pos[0], pos[1] + size[1]] # top right
            ii_3 = ii[pos[0] + size[0] / 3, pos[1]] # left top third vertical
            ii_4 = ii[pos[0] + size[0] / 3, pos[1] + size[1]] # right top third vertical

            sum_A = ii_4 - ii_2 - ii_3 + ii_1

            ii_1 = ii[pos[0] + size[0] / 3, pos[1]] # left top third vertical
            ii_2 = ii[pos[0] + size[0] / 3, pos[1] + size[1]] # right top third vertical
            ii_3 = ii[pos[0] + 2*size[0] / 3, pos[1]] # left bottom third vertical
            ii_4 = ii[pos[0] + 2*size[0] / 3, pos[1] + size[1]] # right bottom third vertical

            sum_B = ii_4 - ii_2 - ii_3 + ii_1

            ii_1 = ii[pos[0] + 2*size[0] / 3, pos[1]] # left bottom third vertical
            ii_2 = ii[pos[0] + 2*size[0] / 3, pos[1] + size[1]] # right bottom third vertical
            ii_3 = ii[pos[0] + size[0], pos[1]] # bottom left
            ii_4 = ii[pos[0] + size[0], pos[1] + size[1]] # bottom right

            sum_C = ii_4 - ii_2 - ii_3 + ii_1
            
            score = sum_A - sum_B + sum_C

        if self.feat_type == (1, 3): # three vertical

            # ii[up and down, left to right]
            ii_1 = ii[pos[0], pos[1]] # top left point
            ii_2 = ii[pos[0], pos[1] + size[1] / 3] # top horizontal left third
            ii_3 = ii[pos[0] + size[0], pos[1]] # bottom left
            ii_4 = ii[pos[0] + size[0], pos[1] + size[1] / 3] # bottom horizontal left third

            sum_A = ii_4 - ii_2 - ii_3 + ii_1

            ii_1 = ii[pos[0], pos[1] + size[1] / 3] # top horizontal left third
            ii_2 = ii[pos[0], pos[1] + 2*size[1] / 3] # top horizontal two thirds
            ii_3 = ii[pos[0] + size[0], pos[1] + size[1] / 3] # bottom horizontal left third
            ii_4 = ii[pos[0] + size[0], pos[1] + 2*size[1] / 3] # bottom horizontal two thirds

            sum_B = ii_4 - ii_2 - ii_3 + ii_1

            ii_1 = ii[pos[0], pos[1] + 2*size[1] / 3] # top horizontal two thirds
            ii_2 = ii[pos[0], pos[1] + size[1]] # top right
            ii_3 = ii[pos[0] + size[0], pos[1] + 2*size[1] / 3] # bottom horizontal two thirds
            ii_4 = ii[pos[0] + size[0], pos[1] + size[1]] # bottom right

            sum_C = ii_4 - ii_2 - ii_3 + ii_1
            
            score = sum_A - sum_B + sum_C

        if self.feat_type == (2, 2):

            #| A | B |
            #| C | D |
            # 
            # A is gray, B is white
            # C is white, D is gray
     
            # print("in self.feat_type = (2,2)")

            # correct values for first local unit test case:
            A = 28266
            B = 28404
            C = 29102
            D = 26598

            # size = (size[0] - 1, size[1] - 1)
            # ii[up and down, left to right]
            ii_1 = ii[pos[0], pos[1]] # top left point
            ii_2 = ii[pos[0], pos[1] + size[1] / 2] # top horiz. mid point
            ii_3 = ii[pos[0] + size[0] / 2, pos[1]] # left vertical mid point
            ii_4 = ii[pos[0] + size[0] / 2, pos[1] + size[1] / 2] # center of square

            sum_A = ii_4 - ii_2 - ii_3 + ii_1

            # if sum_A != A:
                # print("sum_A {0} != {1}".format(sum_A, A))

            ii_1 = ii[pos[0], pos[1] + size[1] / 2] # top horiz. mid point
            ii_2 = ii[pos[0], pos[1] + size[1]] # top right corner
            ii_3 = ii[pos[0] + size[0] / 2, pos[1] + size[1] / 2] # center of square
            ii_4 = ii[pos[0] + size[0] / 2, pos[1] + size[1]] # right vertical midpoint

            sum_B = ii_4 - ii_2 - ii_3 + ii_1
            # if sum_B != B:
                # print("sum_B {0} != {1}".format(sum_B, B))
            ii_1 = ii[pos[0] + size[0] / 2, pos[1]] # left vertical mid point
            ii_2 = ii[pos[0] + size[0] / 2, pos[1] + size[1] / 2] # center of square
            ii_3 = ii[pos[0] + size[0], pos[1]] # bottom left corner
            ii_4 = ii[pos[0] + size[0], pos[1] + size[1] / 2] # bottom horizontal midpoint
            sum_C = ii_4 - ii_2 - ii_3 + ii_1
            # if sum_C != C:
                # print("sum_C {0} != {1}".format(sum_C, C))
            ii_1 = ii[pos[0] + size[0] / 2, pos[1] + size[1] / 2] # middle of square
            ii_2 = ii[pos[0] + size[0] / 2, pos[1] + size[1]] # right vertical midpoint
            ii_3 = ii[pos[0] + size[0], pos[1] + size[1] / 2] # bottom horizontal midpoint
            ii_4 = ii[pos[0] + size[0], pos[1] + size[1]] # bottom right corner
            sum_D = ii_4 - ii_2 - ii_3 + ii_1
            # if sum_D != D:
                # print("sum_D {0} != {1}".format(sum_D, D))
 

            # A is gray, B is white
            # C is white, D is gray
            score = -sum_A + sum_B + sum_C - sum_D
            


        return score

    def evaluate_slow(self, ii):

        pos = self.position
        pos = (pos[0] - 1, pos[1] - 1)

        size = self.size
        ii = ii.copy()
        ii = ii.astype(int)

        if self.feat_type == (2, 1):
            A = np.sum(ii[pos[0]:pos[0] + size[0] / 2,
                                  pos[1]:pos[1] + size[1]])
            B = np.sum(ii[pos[0] + size[0] / 2:pos[0] + size[0],
                                  pos[1]:pos[1] + size[1]])
            score = A - B

        if self.feat_type == (1, 2):
            A = np.sum(ii[pos[0]:pos[0] + size[0],
                                  pos[1]:pos[1] + size[1] / 2])
            B = np.sum(ii[pos[0]:pos[0] + size[0],
                                  pos[1] + size[1] / 2:pos[1] + size[1]])
            score = A - B

        if self.feat_type == (3, 1):
            A = np.sum(ii[pos[0]:pos[0] + size[0] / 3,
                                  pos[1]:pos[1] + size[1]])
            B = np.sum(ii[pos[0] + size[0] / 3:pos[0] + 2 * size[0] / 3,
                                  pos[1]:pos[1] + size[1]])
            C = np.sum(ii[pos[0] + 2 * size[0] / 3:pos[0] + size[0],
                                  pos[1]:pos[1] + size[1]])
            score = A - B + C

        if self.feat_type == (1, 3):
            A = np.sum(ii[pos[0]:pos[0] + size[0],
                                  pos[1]:pos[1] + size[1] / 3])
            B = np.sum(ii[pos[0]:pos[0] + size[0],
                                  pos[1] + size[1] / 3:pos[1] + 2 * size[1] / 3])
            C = np.sum(ii[pos[0]:pos[0] + size[0],
                                  pos[1] + 2 * size[1] / 3:pos[1] + size[1]])
            score = A - B + C

        if self.feat_type == (2, 2):
            A = np.sum(ii[pos[0]:pos[0] + size[0] / 2,
                                  pos[1]:pos[1] + size[1] / 2])
            B = np.sum(ii[pos[0]:pos[0] + size[0] / 2,
                                  pos[1] + size[1] / 2:pos[1] + size[1]])
            C = np.sum(ii[pos[0] + size[0] / 2:pos[0] + size[0],
                                  pos[1]:pos[1] + size[1] / 2])
            D = np.sum(ii[pos[0] + size[0] / 2:pos[0] + size[0],
                                  pos[1] + size[1] / 2:pos[1] + size[1]])
            score = -A + B + C - D

        return score



def convert_images_to_integral_images(images):
    """Convert a list of grayscale images to integral images.

    Args:
        images (list): List of grayscale images (uint8 or float).

    Returns:
        (list): List of integral images.
    """

    # the white area (255) represents an addition
    # the gray area (126) represents a subtraction

    # the integral image at location x,y contains
    # the sum of pixels above and to the left of x,y
    result = []
    for image in images:
        temp = image.copy()
        for i in range(image.ndim):
            temp = temp.cumsum(axis=i)
        result.append(temp) 

    return result


class ViolaJones:
    """Viola Jones face detection method

    Args:
        pos (list): List of positive images.
        neg (list): List of negative images.
        integral_images (list): List of integral images.

    Attributes:
        haarFeatures (list): List of haarFeature objects.
        integralImages (list): List of integral images.
        classifiers (list): List of weak classifiers (VJ_Classifier).
        alphas (list): Alpha values, one for each weak classifier.
        posImages (list): List of positive images.
        negImages (list): List of negative images.
        labels (numpy.array): Positive and negative labels.
    """
    def __init__(self, pos, neg, integral_images):
        self.haarFeatures = []
        self.integralImages = integral_images
        self.classifiers = []
        self.alphas = []
        self.posImages = pos
        self.negImages = neg
        self.labels = np.hstack((np.ones(len(pos)), -1*np.ones(len(neg))))

    def createHaarFeatures(self):
        # Let's take detector resolution of 24x24 like in the paper
        FeatureTypes = {"two_horizontal": (2, 1),
                        "two_vertical": (1, 2),
                        "three_horizontal": (3, 1),
                        "three_vertical": (1, 3),
                        "four_square": (2, 2)}

        haarFeatures = []
        for _, feat_type in FeatureTypes.iteritems():
            for sizei in range(feat_type[0], 24 + 1, feat_type[0]):
                for sizej in range(feat_type[1], 24 + 1, feat_type[1]):
                    for posi in range(0, 24 - sizei + 1, 8):
                        for posj in range(0, 24 - sizej + 1, 8):
                            haarFeatures.append(
                                HaarFeature(feat_type, [posi, posj],
                                            [sizei, sizej]))
        self.haarFeatures = haarFeatures

    def train(self, num_classifiers):

        # Use this scores array to train a weak classifier using VJ_Classifier
        # in the for loop below.
        scores = np.zeros((len(self.integralImages), len(self.haarFeatures)))
        print " -- compute all scores --"
        for i, im in enumerate(self.integralImages):
            scores[i, :] = [hf.evaluate(im) for hf in self.haarFeatures]

        weights_pos = np.ones(len(self.posImages), dtype='float') * 1.0 / (
                           2*len(self.posImages))
        weights_neg = np.ones(len(self.negImages), dtype='float') * 1.0 / (
                           2*len(self.negImages))
        weights = np.hstack((weights_pos, weights_neg))

        print " -- select classifiers --"
        for i in range(num_classifiers):

            weights /= weights.sum(axis=None)
            # instantiate h_j
            # print(scores.shape)
            # print(self.labels.shape)
            # print(weights.shape)
            # for each in self.haarFeatures:
            h = VJ_Classifier(scores,
                self.labels,
                weights)

            # train this classifier using the VJ_Classifier class
            h.train()

            # append h_j to the self.classfiers attribute
            self.classifiers.append(h)

            # After calling the weak classifier's train function 
            # you can obtain its lowest error by using the "error" attribute.
            epsilon = h.error

            # calculate beta
            beta = float(epsilon) / (1 - epsilon)

            for i in range(0, len(weights)):
                # update the weights
                y_pred = h.predict(scores[i])
                # raw_input(y_pred)
                # e_i = 0 # set e_i to 0 if predictions are correct
                # weights[np.argwhere(y_pred == self.labels)] = weights*np.power(beta, 1 - e_i)
                # e_i = 1 # set e_i to 1 if prediction are not correct
                # weights[np.argwhere(y_pred != self.labels)] = weights*np.power(beta, 1 - e_i)
                if y_pred == self.labels[i]:
                    e_i = -1 # set e_i to 0 if predictions are correct
                    weights[i] = weights[i]*np.power(beta, 1 - e_i)
                else:
                    e_i = 1
                    weights[i] = weights[i]*np.power(beta, 1 - e_i)


            # calculate alpha and append it to self.alphas
            alpha = np.log(1 / beta)
            self.alphas.append(alpha)

    def predict(self, images):
        """Return predictions for a given list of images.

        Args:
            images (list of element of type numpy.array): list of images (observations).

        Returns:
            list: Predictions, one for each element in images.
        """

        ii = convert_images_to_integral_images(images)

        scores = np.zeros((len(ii), len(self.haarFeatures)))

        # Populate the score location for each classifier 'clf' in
        # self.classifiers.
        for idx, h in enumerate(self.classifiers):

            # Obtain the Haar feature id from clf.feature
            # feature_id is index of the feature that leads to the minimum classification error
            feature_id = h.feature

            # Use this id to select the respective feature object from
            # self.haarFeatures
            feature_obj = self.haarFeatures[feature_id]
           
            # Add the score value to score[x, feature id] calling the feature's
            # evaluate function. 'x' is each image in 'ii'

            for j, img in enumerate(ii):
                scores[j, feature_id] = feature_obj.evaluate(img)


        result = []

        # Append the results for each row in 'scores'. This value is obtained
        # using the equation for the strong classifier H(x).

        for x in scores:
            # Implements the string classifier H(x) definition
            alpha_sum = 0
            classifier_sum = 0
            for idx, classifier in enumerate(self.classifiers):
                alpha_sum += self.alphas[idx]
                classifier_sum += classifier.predict(x)

            if alpha_sum*classifier_sum >= 0.5*alpha_sum:
                result.append(1)
            else:
                result.append(-1)
            
        return result

    def faceDetection(self, image, filename):
        """Scans for faces in a given image.

        Complete this function following the instructions in the problem set
        document.

        Use this function to also save the output image.

        Args:
            image (numpy.array): Input image.
            filename (str): Output image file name.

        Returns:
            None.
        """
        image = image.copy() # (60, 120, 3)

        image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        # generate a bunch of 24 x 24 image cutouts from image
        images = []
        positions = []

        M = image_gray.shape[0]
        N = image_gray.shape[1]

        i_step = 18
        j_step = 24
        for i in range(0, M, i_step):
            if i + i_step >= M:
                break
            for j in range(0, N, j_step):
                if j + j_step >= N:
                    break
                cutout = image_gray[i:i+24, j:j+24]
                # show_img("cutout", cutout)
                images.append(cutout)
                positions.append([i, j])


        # send them to self.predict. The prediction will be 1
        # at any location the VJ algorithm thinks there is a match
        # and -1 otherwise.
        results = self.predict(images)

        square_coords = []
        for idx, result in enumerate(results):
            if result == 1: # predicted match for the face
                y, x = positions[idx]
                thickness = 1
                cv2.line(image,
                         (x, y), # first point
                         (x+24, y), # secondpoint
                         (0,255,0), # color
                         thickness) # line thickness
                cv2.line(image,
                         (x, y), # first point
                         (x, y+24), # secondpoint
                         (0,255,0), # color
                         thickness) # line thickness
                cv2.line(image,
                         (x+24, y+24), # secondpoint
                         (x, y+24), # first point
                         (0,255,0), # color
                         thickness) # line thickness
                cv2.line(image,
                         (x+24, y+24), # first point
                         (x+24, y), # secondpoint
                         (0,255,0), # color
                         thickness) # line thickness
                square_coords.append([[x,y],
                                      [x+24, y],
                                      [x, y+24],
                                      [x+24,y+24]])
                # square_coords.append([positions[idx])

        # result = draw_box(image, square_coords)
        cv2.imwrite("output/{}.png".format(filename), image)


def show_img(img_name, img):
    img = img.copy()
    if True:
        cv2.namedWindow(img_name, cv2.WINDOW_NORMAL)
        cv2.imshow(img_name, img)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
