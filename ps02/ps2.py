"""
CS6476 Problem Set 2 imports. Only Numpy and cv2 are allowed.
"""
import cv2

import numpy as np
# useful sources
# https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_houghlines/py_houghlines.html
# https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_colorspaces/py_colorspaces.html
# https://docs.opencv.org/2.4/modules/imgproc/doc/feature_detection.html
# https://www.mathopenref.com/coordcentroid.html
# https://stackoverflow.com/questions/30499857/how-to-loop-through-2d-numpy-array-using-x-and-y-coordinates-without-getting-out
# https://en.wikipedia.org/wiki/Invertible_matrix
# Fast NL Means Denoise: https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_photo/py_non_local_means/py_non_local_means.html
# https://docs.opencv.org/3.0-beta/modules/photo/doc/denoising.html
# Image Filtering and De-noising: https://docs.opencv.org/3.1.0/d4/d13/tutorial_py_filtering.html
# Bilateral Filter: https://docs.opencv.org/2.4/modules/imgproc/doc/filtering.html
# https://docs.opencv.org/2.4/doc/tutorials/imgproc/gausian_median_blur_bilateral_filter/gausian_median_blur_bilateral_filter.html
# https://docs.opencv.org/2.4/modules/imgproc/doc/filtering.html?highlight=gaussianblur#gaussianblur

def rgb_to_hsv(red, green, blue):
    color = np.uint8([[[blue,green,red]]])
    hsv_color = cv2.cvtColor(color, cv2.COLOR_BGR2HSV)
    # print(hsv_color)
    hue = hsv_color[0][0][0]
    saturation = hsv_color[0][0][1]
    value = hsv_color[0][0][2]
    print("RGB: {0},{1},{2}, HSV: {3},{4},{5}".format(red,
                                                      green,
                                                      blue,
                                                      hue,
                                                      saturation,
                                                      value))
    return (hue, saturation, value)

def save_hsv(filename, img_hsv):
    bgr_img = cv2.cvtColor(img_hsv, cv2.COLOR_HSV2BGR)
    cv2.imwrite(filename, bgr_img)

def show_img(img_name, img):
    if True:
        cv2.imshow(img_name, img)
        cv2.waitKey(0)
        cv2.destroyAllWindows()


def traffic_light_detection(img_in, radii_range):
    """Finds the coordinates of a traffic light image given a radii
    range.

    Use the radii range to find the circles in the traffic light and
    identify which of them represents the yellow light.

    Analyze the states of all three lights and determine whether the
    traffic light is red, yellow, or green. This will be referred to
    as the 'state'.

    It is recommended you use Hough tools to find these circles in
    the image.

    The input image may be just the traffic light with a white
    background or a larger image of a scene containing a traffic
    light.

    Args:
        img_in (numpy.array): image containing a traffic light.
        radii_range (list): range of radii values to search for.

    Returns:
        tuple: 2-element tuple containing:
        coordinates (tuple): traffic light center using the (x, y)
                             convention.
        state (str): traffic light state. A value in {'red', 'yellow',
                     'green'}
    """

    img_temp = img_in.copy()
    # show_img("Image", img_temp)
    # filename = raw_input("Please provide a png filename to save to ('Enter' to skip): ")
    # if filename.lower() != "":
        # cv2.imwrite(filename, img_temp)
    # convert the image to grayscale
    img_gray = cv2.cvtColor(img_temp, cv2.COLOR_BGR2GRAY)

    minRadius = min(radii_range)
    maxRadius = max(radii_range)
    # raw_input("radii_range = {0}".format(radii_range))
    # raw_input("minRadius= {0}".format(minRadius))
    # raw_input("maxRadius = {0}".format(maxRadius))


    # img_gray = img_temp[:,:,1] # green channel (b, g, r)
    img_gray = getCannyEdges(img_temp)
    print(minRadius)
    print(maxRadius)
    # playWithParamsHoughCircles(img_gray, img_temp, minRadius, maxRadius)
    # minDist, param1, param2, minRadius, maxRadius, accumulator_res_inverse
    # 36, 30, 15, 10, 29, 1
    # result = (x, y, radius)

    # prev_result = None
    # while minRadius < maxRadius:
    result = cv2.HoughCircles(img_gray, # 8 bit single channel grayscale input image
                              method=cv2.cv.CV_HOUGH_GRADIENT, # detection method to use
                              dp=1, # inverse ratio of accumulator resolution. 1 means same resolution, 2 means half
                              minDist=minRadius*2, # min distance between centers of detected circles
                              # param1: (CV_HOUGH_GRADIENT first parameter) higher threshold of 
                              # what is passed to canny edge detector
                              param1=30, 
                              # param2: (CV_HOUGH_GRADIENT second parameter) accumulator threshold 
                              # for the circle centers at the detection stage
                              # The smaller param2 is, the more false circles may be detected.
                              param2=23,
                              minRadius=9, # minimum circle radius
                              maxRadius=31) # maximum circle radius

    if result is not None:
        # drawCircles(img_temp, result)
        if len(result[0]) < 3:
            return None
        light_coords = getLightCoords(img_temp, result)
        if light_coords is None:
            return None
        red, yellow, green = light_coords[0], light_coords[1], light_coords[2]

        redOn = isLightOn(img_temp, red, "red")
        yellowOn = isLightOn(img_temp, yellow, "yellow")
        greenOn = isLightOn(img_temp, green, "green")
        # raw_input(("redOn: {0}\n"
                  # "yellowOn: {1}\n"
                  # "greenOn: {2}\n").format(redOn, yellowOn, greenOn))
        ans = "green"
        if greenOn:
            ans = "green"
        elif redOn:
            ans = "red"
        elif yellowOn:
            ans = "yellow"
        # state = getLightState(img_temp)

        drawCrossHairsAtPoint(img_temp, yellow[0], yellow[1], 5)
        drawCrossHairsAtPoint(img_temp, red[0], red[1], 5)
        drawCrossHairsAtPoint(img_temp, green[0], green[1], 5)
        # show_img("img_temp", img_temp)
        x = int(yellow[0])
        y = int(yellow[1])

        return (x, y), ans

def getLightCoords(img_temp, result):
    """getYellowCenter

    :param img_temp:
    :param result: has circle coordinates in format (x, y, radius)
    """
    # sort the coordinates so x positions go from small to large
    # TODO: need to sort by a specific column, otherwise, this will mix and match coordinates
    # raw_input(result)
    coords_temp = result[0]
    coords = coords_temp[np.argsort(coords_temp[:, 1])]
    # raw_input(coords)
    # coords = np.sort(result[0], axis=0)
    medians = np.median(coords, axis=0)
    res = np.empty((0,3))

    # show_img("img_temp", img_temp)
    # raw_input(coords)
    # remove outliers
    for coord in coords:
        # check if y value is way different from the other traffic lights
        # they should all be lined up together
        if abs(coord[0] - medians[0]) > 10:
            continue
        # check if the circle radius is too big or too small when compared
        # to most of the circles found
        elif abs(coord[2] - medians[2]) > 7:
            continue
        temp = np.array([coord])
        res = np.vstack((res, temp))

    # center of yellow light is assumed to be the center of
    # the entire traffic light 
    if len(res) == 3:
        print("Found three valid points. ")
    else:
        print("Did not find three valid points.")
        # show_img("img_temp", img_temp)
        # raw_input("coords = {0}".format(coords))
        # raw_input("medians = {0}".format(medians))
        # raw_input("res = {0}".format(res))

    # raw_input(res)
    # res = np.sort(res, axis=0)
    # yellow light coordinates will be 
    if len(res) < 3:
        return None
    red = res[0]
    yellow = res[1]
    green = res[2]
    return red, yellow, green

    # for coord in result[0]:
        # raw_input(coord)

def filterDoNotEnterLines(img_temp, hough_segments):
    # raw_input(hough_segments)
    if hough_segments[0].shape[0] == 2:
        return hough_segments

    raw_input(hough_segments)



def isLightOn(img_temp, coord, color):
    # RED_ON = (254, 15, 0)
    # RED_OFF = (127, 7, 0)

    # YELLOW_ON = (255, 255, 0)
    # YELLOW_OFF = (126, 126, 0)

    # GREEN_ON = (17, 255, 0)
    # GREEN_OFF = (9, 127, 0)

    val = img_temp[int(coord[1]), int(coord[0])]

    # val is in BGR
    r,g,b = val[2], val[1], val[0]
    # raw_input("{0} = {1}".format(color, val))
    if color == "red":
        if r >= 200 and r <= 255:
            return True
        else:
            return False
    elif color == "yellow":
        if (r >= 200 and g >= 200
            and r <= 255 and g <= 255):
            return True
        else:
            return False
    elif color == "green":
        if g >= 200 and g <= 255:
            return True
        else:
            return False
    else:
        print("Shouldn't get here.")
        return False



def getLightState(img_temp):


    # RGB: Red, Green, Blue
    rlow = rgb_to_hsv(127,0,0)
    # HSV: hue, saturation, value
    lower_range_red = np.array([rlow[0], rlow[1], rlow[2]]) # RGB: (127, 7, 0)

    rhigh = rgb_to_hsv(255,0,0)
    upper_range_red = np.array([rhigh[0], rhigh[1], rhigh[2]]) # RGB: (254, 15, 0)

    glow = rgb_to_hsv(0, 127, 0)
    lower_range_green = np.array([glow[0], glow[1], glow[2]]) # RGB: (9, 127, 0)

    ghigh = rgb_to_hsv(0, 255, 0)
    upper_range_green = np.array([ghigh[0], ghigh[1], ghigh[2]]) # RGB: (17, 255, 0)

    ylow = rgb_to_hsv(126, 126, 0)
    lower_range_yellow = np.array([ylow[0], ylow[1], ylow[2]]) # RGB: (126, 126, 0)

    yhigh = rgb_to_hsv(255, 255, 0)
    upper_range_yellow = np.array([yhigh[0], yhigh[1], yhigh[2]]) # RGB: (255, 255, 0)

    img_hsv = cv2.cvtColor(img_temp, cv2.COLOR_BGR2HSV)
    # save_hsv("img_final.png", img_hsv)
    red_mask = cv2.inRange(img_hsv,
                           lower_range_red,
                           upper_range_red)
    green_mask = cv2.inRange(img_hsv,
                           lower_range_green,
                           upper_range_green)
    yellow_mask = cv2.inRange(img_hsv,
                           lower_range_yellow,
                           upper_range_yellow)

    red_img = cv2.bitwise_and(img_hsv,
                              img_hsv,
                              mask=red_mask)
    green_img = cv2.bitwise_and(img_hsv,
                              img_hsv,
                              mask=green_mask)
    yellow_img = cv2.bitwise_and(img_hsv,
                              img_hsv,
                              mask=yellow_mask)
    show_img("red_img.png", red_img)
    show_img("yellow_img.png", yellow_img)
    show_img("green_img.png", green_img)


def drawCircles(img_temp, result):
    img_temp = img_temp.copy()
    result = result.copy()
    for circle in result[0,:]:
        # cv2.circle(img, center (tuple), radius (float), color (tuple) (B,G,R), thickness)
        cv2.circle(img_temp,(circle[0], circle[1]), circle[2], (0,255,0), 2)
        # raw_input(circle[0])
        # raw_input(circle[1])
        drawCrossHairsAtPoint(img_temp, circle[0], circle[1], 5)
    show_img("circles", img_temp)

def drawCrossHairsAtPoint(img_base, x, y, s):
    """drawCrossHairsAtPoint
    Warning: modifies img_base

    :param img_base:
    :param x:
    :param y:
    :param s:
    """
    cv2.line(img_base,
             (int(x - s), int(y)), # first point
             (int(x + s), int(y)), # secondpoint
             (0,255,0), # color
             2) # line thickness
    cv2.line(img_base,
             (int(x), int(y - s)), # first point
             (int(x), int(y + s)), # secondpoint
             (0,255,0), # color
             2) # line thickness
    # show_img("cross hairs", img)


def makeWhiteImg(img_base):
    outline = np.zeros(img_base.shape)
    outline += 255 # make it all white
    return outline

def getCannyEdges(img_base, channel=2):

    temp = img_base.copy()
    temp = temp[:,:,channel]
    # use only green channel
    # image: single 8-bit channel image
    # threshold1 - first threshold for the hysteresis procedure.
    # threshold2 - second threshold for the hysteresis procedure.
    # apertureSize - aperture size for the Sobel() operator.
    res = cv2.Canny(temp,
                     50,
                     150,
                     apertureSize = 3)
    # show_img("canny image", res)
    return res

def getCannyEdgesGreen(img_base):

    temp = img_base.copy()
    temp = temp[:,:,1]
    # use only green channel
    # image: single 8-bit channel image
    # threshold1 - first threshold for the hysteresis procedure.
    # threshold2 - second threshold for the hysteresis procedure.
    # apertureSize - aperture size for the Sobel() operator.
    res = cv2.Canny(temp,
                     50,
                     150,
                     apertureSize = 3)
    # show_img("canny image", res)
    return res

def playWithCanny(img_temp):
    pass

def getHoughLines(img_base, p_theta=np.pi, rho_val=1, threshold=50):
    img = img_base.copy()
    # img = img[:,:,2] # green channel only
    outline = makeWhiteImg(img)
    # rho: distance resolution of the accumulator in pixels
    # theta: angle resolution of the accumulator in pixels (e.g. np.pi, or np.pi/2)
    # threshold: Accumulator threshold - only those lines are returned that get enough votes 
    houghlines = cv2.HoughLines(img,  
                                rho_val, # rho
                                p_theta, # theta 
                                threshold) # threshold

    # raw_input(houghlines.shape)
    for rho, theta in houghlines[0]:
        factor = 1000
        cos_theta = np.cos(theta)
        sin_theta = np.sin(theta)
        x0 = rho*cos_theta
        y0 = rho*sin_theta
        x = int(x0 + factor*(-sin_theta))
        y = int(y0 + factor*(cos_theta))
        x1 = int(x0 - factor*(-sin_theta))
        y1 = int(y0 - factor*(cos_theta))

        cv2.line(outline,
                 (x, y), # first point
                 (x1,y1), # secondpoint
                 (0,0,0), # black color
                 2) # line thickness

    show_img("GetHoughLines: outline ({0} lines)".format(houghlines.shape[1]), outline)

    return outline


def getHoughLinesP(img_base,
                   p_theta,
                   threshold=50,
                   minLineLength=1000,
                   maxLineGap=10):
    """getHoughLinesP
    Get line segments

    :param img_base: base image
    :param p_theta: angle resolution of the accumulato
    :param threshold: threshold: minimum votes required (only those lines are returned that get enough votes)
    :param minLineLength: minimum line length, reject lines shorter than this value
    :param maxLineGap: max allowed gap between points on the same line to link them
    """

    img = img_base.copy()
    # img = img_base[:,:,2] # green channel only
    outline = makeWhiteImg(img)

    # get probabilistic hough transform algorithm for line detection
    # single channel 8-bit img
    # rho: distance resolution of the accumulator in pixels
    # theta: angle resolution of the accumulator in pixels
    houghlines = cv2.HoughLinesP(img,
                                  1,
                                  p_theta,
                                  threshold,
                                  minLineLength,
                                  maxLineGap)
    # raw_input(houghlines)
    if houghlines is not None:
        for x0, y0, x1, y1 in houghlines[0]:
            # cv2.line(out_numpy_arr, first point, second point, color, thickness)
            cv2.line(outline, (x0,y0), (x1,y1), (0,0,0),2)
    # show_img("GetHoughLinesP: outline ({0} lines)".format(houghlines.shape[1]), outline)
    return houghlines

def yield_sign_detection(img_in,
                         rgb_low=(255,0,0),
                         rgb_high=(255,0,0),
                         p_rho=1,
                         p_theta=np.pi/42,
                         p_threshold=50,
                         p_minLineLength=85,
                         p_maxLineGap=10):
    """Finds the centroid coordinates of a yield sign in the provided
    image.

    Args:
        img_in (numpy.array): image containing a traffic light.

    Returns:
        (x,y) tuple of coordinates of the center of the yield sign.
    """
    img_temp = img_in.copy()
    # show_img("yield sign detection", img_temp)
    # (R, G, B)
    YIELD_BORDER_RED = (255, 8, 0)
    YIELD_INNER_WHITE = (255, 255, 255)

    # playWithMask(img_temp)
    # filter out only red and white colors in the image
    rlow = rgb_to_hsv(rgb_low[0],rgb_low[1],rgb_low[2]) # R,G,B
    # HSV: hue, saturation, value
    lower_range_red = np.array([rlow[0], rlow[1], rlow[2]])

    rhigh = rgb_to_hsv(rgb_high[0],rgb_high[1],rgb_high[2])
    upper_range_red = np.array([rhigh[0], rhigh[1], rhigh[2]])
    img_hsv = cv2.cvtColor(img_temp, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(img_hsv,
                       lower_range_red,
                       upper_range_red)
    red_white_img = cv2.bitwise_and(img_hsv,
                                    img_hsv,
                                    mask=mask)

    # apply a canny edge detector

    res = getCannyEdges(red_white_img)
    # show_img("canny edges", res)

    # kernel = np.ones((50,50))
    # dilated_img = cv2.dilate(res,kernel,iterations=1)
    # show_img("dilated", dilated_img)
    # res = dilated_img.copy()
    # get the average x and average y of all pixels after filtering
    # raw_input(res.shape)
    # raise NotImplementedError

    # use hough lines, find the three vertices of the
    # triangle. then calculate the centroid

    # tune and then get parameterized hough lines
    # playWithParams(res, angleMax=2000,thresholdMax=500, minLineLengthMax=2000, img=img_temp)
    # angle, threshold, minLineLength, rho
    # 1642, 83, 40, 1
    # 42, 50, 81, 1
    # 42, 50, 85, 1

    houghsegments = cv2.HoughLinesP(image=res,
                                    rho=p_rho,
                                    theta=p_theta,  # angle, radians
                                    threshold=p_threshold,
                                    minLineLength=p_minLineLength,
                                    maxLineGap=p_maxLineGap)
    if houghsegments is not None:
        for x0, y0, x1, y1 in houghsegments[0]:
            # cv2.line(out_numpy_arr, first point, second point, color, thickness)
            cv2.line(img_temp, (x0, y0), (x1, y1), (0, 0, 0), 2)
        result = np.mean(houghsegments[0], axis=0)
        # raw_input(result)
        mid_x = int((result[0] + result[2]) / 2)
        # raw_input(mid_x)
        mid_y = int((result[1] + result[3]) / 2)
        # raw_input(mid_y)
        # drawCrossHairsAtPoint(img_temp, mid_x, mid_y, 5)

        # show_img("img_temp", img_temp)

        return mid_x, mid_y


def getAllIntersectionPoints(houghlines):
    """getAllIntersectionPoints
    Gets all intersection points for the lines
    Iterates over all lines and calls getIntersection for each.

    :param houghlines:
    """
    intersections = np.empty((0,2))
    # raw_input(houghlines.shape)
    # raw_input("houghlines = {0}".format(houghlines))
    for rho_1, theta_1 in houghlines[0]:
        for rho_2, theta_2 in houghlines[0]:
            if rho_1 == rho_2 and theta_1 == theta_2:
                continue
            # raw_input(("finding any intersection between "
                      # "rho={0} theta={1} and rho={2} theta={3}").format(rho_1,
                                                                        # theta_1,
                                                                        # rho_2,
                                                                        # theta_2))
            X = getIntersection(rho_1, theta_1, rho_2, theta_2)
            if X is not None:
                # raw_input(X)
                # raw_input(X.shape)
                temp = np.array([X[0][0], X[1][0]])
                # raw_input(temp.shape)
                # temp = np.array([res[0], res[1]])
                # raw_input(intersections)
                # raw_input(intersections.shape)
                intersections = np.vstack((intersections,temp))

    return intersections

def getIntersection(rho_1, theta_1, rho_2, theta_2):
    """getIntersection
    Returns point of intersectino of the two lines or None if they don't intersect

    :param rho_1:
    :param theta_1:
    :param rho_2:
    :param theta_2:
    """

    A = np.array([[np.cos(theta_1), np.sin(theta_1)],
                  [np.cos(theta_2), np.sin(theta_2)]])
    b = np.array([[rho_1],[rho_2]])
    X = None
    try: 
        AInv = np.linalg.inv(A)
        X = np.dot(AInv, b)
        # check match with np.linalg.solve(A,b)
        # raw_input(X)
        # X_Alt = np.linalg.solve(A,b)
        # raw_input(X_Alt)
        # raw_input(np.allclose(X, X_Alt))
    except np.linalg.LinAlgError as e:
        print("Error: {0}".format(e))

    return X


def stop_sign_detection(img_in,
                        rgb_low=(200,0,0),
                        rgb_high=(205,0,0),
                        p_rho=1,
                        p_theta=np.pi/10,
                        p_threshold=38,
                        p_lines=1,
                        p_minLineLength=20,
                        p_maxLineGap=10):
    """Finds the centroid coordinates of a stop sign in the provided
    image.

    Args:
        img_in (numpy.array): image containing a traffic light.

    Returns:
        (x,y) tuple of the coordinates of the center of the stop sign.
    """
    img_temp = img_in.copy()
    # option #1
    # draw parallel lines or line segments along all the images, then 
    # draw a line betwee the center of each of those segments to the line 
    # on the opposite side with a matching slope
    # then, find the point where all of those lines meet in the center.
    # also, make sure that the sign is actually red...
    # green_channel = img_temp[:,:,1] # bgr, red channel
    # show_img("stop sign", img_temp)
    # getCannyEdges(img_temp)
    # getHoughLinesP(green_channel, np.pi/10)


    # option #2 (used):
    # filter the image so that we only get a dark red color
    # get line segments for top, bottom, left and right edges of the stop sign
    # get the center point for all of the line segments
    # take the average x value for all line segments, that is the center x coordinate
    # get the average y value for all line segments, that is the center y coordinate.

    # red_channel = img_temp[:,:,2]
    # RGB: Red, Green, Blue
    # the red color in the stop sign is RGB(204, 0, 0)
    print("Stop sign rgb low: {0}".format(rgb_low))
    print("Stop sign rgb high: {0}".format(rgb_high))
    # playWithMask(img_temp)
    rlow = rgb_to_hsv(rgb_low[0],rgb_low[1],rgb_low[2])
    # HSV: hue, saturation, value
    lower_range_red = np.array([rlow[0], rlow[1], rlow[2]]) # RGB: (127, 7, 0)

    rhigh = rgb_to_hsv(rgb_high[0], rgb_high[1], rgb_high[2])
    upper_range_red = np.array([rhigh[0], rhigh[1], rhigh[2]])  # RGB: (254, 15, 0)
    img_hsv = cv2.cvtColor(img_temp, cv2.COLOR_BGR2HSV)
    red_mask = cv2.inRange(img_hsv,
                           lower_range_red,
                           upper_range_red)
    red_img = cv2.bitwise_and(img_hsv,
                              img_hsv,
                              mask=red_mask)
    # show_img("stop sign", red_img)

    # get canny edges from the red image only
    res = getCannyEdges(red_img)
    # show_img("canny edges", res)

    # playWithParams(res, angleMax = 1000, img=img_temp)
    # angle, threshold, minLineLength, rho, lines, max line gap
    # 6, 38, 33, 1, 17 (doesn't seem to matter), 10
    # 164, 39, 20, 3, 8, 9
    # 164, 39, 20, 1, 1, 10
    # 164, 38, 20, 1, 1, 10
    # 160, 38, 20, 1, 1, 10

    # 176, 38, 20, 1, 1, 10
    # 172, 38, 20, 1, 1, 10
    # 10, 38, 20, 1, 1, 10

    houghlines = cv2.HoughLinesP(image=res,
                                 rho=p_rho,
                                 theta=p_theta,
                                 threshold=p_threshold,
                                 lines=p_lines,
                                 minLineLength=p_minLineLength,
                                 maxLineGap=p_maxLineGap)
    if houghlines is not None:

        # sort by
        # houghlinesTemp = houghlines[0]
        # raw_input(houghlines)
        # houghlinesTemp = houghlinesTemp[np.argsort(houghlinesTemp[:, 3])]
        # houghlines[0] = houghlinesTemp
        # raw_input(houghlines)

        # then only use the first 8 detected lines after they are sorted
        # if len(houghlines[0]) < 8:
            # raw_input("houglines isless than 8")
            # for x0, y0, x1, y1 in houghlines[0]:
                # cv2.line(img_temp, (x0,y0), (x1,y1), (0,0,0),2)
            # show_img("less than 8", img_temp)
            # return None
        for x0, y0, x1, y1 in houghlines[0]:
            # cv2.line(out_numpy_arr, first point, second point, color, thickness)
            cv2.line(img_temp, (x0,y0), (x1,y1), (0,0,0),2)
    else:
        # raw_input("houglines is none")
        return None
    # show_img("HoughLines", img_temp)

    # show_img("stop sign", img_temp)
    # Take average x and y values for the stop sign
    houghlines = houghlines[0]
    x0_mean = houghlines[:, 0].mean()
    y0_mean = houghlines[:, 1].mean()
    x1_mean = houghlines[:, 2].mean()
    y1_mean = houghlines[:, 3].mean()
    # raw_input("x0_mean = {0}".format(x0_mean))
    # raw_input("x1_mean = {0}".format(x1_mean))
    # raw_input("y0_mean = {0}".format(y0_mean))
    # raw_input("y1_mean = {0}".format(y1_mean))
    x_center = int(abs(x1_mean + x0_mean) / 2)
    y_center = int(abs(y1_mean + y0_mean) / 2)
    # raw_input("x_center = {0}".format(x_center))
    # raw_input("y_center = {0}".format(y_center))
    drawCrossHairsAtPoint(img_temp, x_center, y_center, 5)
    # show_img("stop sign", img_temp)

    return x_center, y_center


def playWithParamsHoughLines(img_base, img_temp):

    rho_title = "Rho"
    threshold_title = "Threshold"
    theta_title = "Theta"
    window = "HoughLines"
    cv2.namedWindow("HoughLines")
    cv2.createTrackbar(rho_title, window, 1, 50, doNothing)
    cv2.createTrackbar(threshold_title, window, 50, 1000, doNothing)
    cv2.createTrackbar(theta_title, window, 1, 1000, doNothing)

    while(1):
        temp_img = img_temp.copy()
        img = img_base.copy()
        # img = img[:,:,2] # green channel only

        k = cv2.waitKey(1) & 0xFF
        if k == 10:
            break
        rho_val = cv2.getTrackbarPos(rho_title, window)
        threshold_val = cv2.getTrackbarPos(threshold_title, window)
        theta_val = cv2.getTrackbarPos(theta_title, window)

        if rho_val == 0:
            rho_val = 1
            print("rho lower limit is 1")
        if threshold_val == 0:
            threshold_val = 1
            print("threshold lower limit is 1")
        if theta_val == 0:
            theta_val = 1
            print("theta lower limit is 1")
            

        outline = makeWhiteImg(img)
        # raw_input("rho = {0}".format(rho_val))
        # raw_input("threshold = {0}".format(threshold_val))
        # raw_input("theta = {0}".format(theta_val))

        # rho: distance resolution of the accumulator in pixels
        # theta: angle resolution of the accumulator in pixels (e.g. np.pi, or np.pi/2)
        # threshold: Accumulator threshold - only those lines are returned that get enough votes 
        houghlines = cv2.HoughLines(img,  
                                    rho_val, # rho
                                    np.pi/theta_val, # theta 
                                    threshold_val) # threshold

        if houghlines is not None:
            for rho, theta in houghlines[0]:
                factor = 1000
                cos_theta = np.cos(theta)
                sin_theta = np.sin(theta)
                x0 = rho*cos_theta
                y0 = rho*sin_theta
                x = int(x0 + factor*(-sin_theta))
                y = int(y0 + factor*(cos_theta))
                x1 = int(x0 - factor*(-sin_theta))
                y1 = int(y0 - factor*(cos_theta))

                # raw_input(x)
                # raw_input(y)
                # raw_input(x1)
                # raw_input(y1)

                cv2.line(temp_img,
                         (x, y), # first point
                         (x1,y1), # secondpoint
                         (0,0,0), # black color
                         2) # line thickness
        cv2.imshow(window, temp_img)

    cv2.destroyAllWindows()


def playWithParamsHoughCircles(img, img_color, minRadius, maxRadius):
    minDist_title = "minDist"
    param1_title = "param1"
    param2_title = "param2"
    minRadius_title = "minRadius"
    accumulator_res_title ="accumulator resolution inverse"
    maxRadius_title = "maxRadius"
    window = "HoughCircles"
    cv2.namedWindow(window)
    cv2.createTrackbar(minDist_title, window, 1, 150, doNothing)
    cv2.createTrackbar(param1_title, window, 30, 100, doNothing)
    cv2.createTrackbar(param2_title, window, 15, 50 , doNothing)
    cv2.createTrackbar(minRadius_title, window, minRadius, minRadius*4, doNothing)
    cv2.createTrackbar(maxRadius_title, window, maxRadius, maxRadius*4, doNothing)
    cv2.createTrackbar(accumulator_res_title, window, 1, 20, doNothing)
    while(1):
        img_temp = img.copy()
        img_temp_color = img_color.copy()
        k = cv2.waitKey(1) & 0xFF
        if k == 10:
            break
        minDist_val = cv2.getTrackbarPos(minDist_title, window)
        accum_val = cv2.getTrackbarPos(accumulator_res_title, window)
        param1_val = cv2.getTrackbarPos(param1_title, window)
        param2_val = cv2.getTrackbarPos(param2_title, window)
        minRadius_val = cv2.getTrackbarPos(minRadius_title, window)
        maxRadius_val = cv2.getTrackbarPos(maxRadius_title, window)
        # raw_input("press enter...")
        if minDist_val == 0:
            minDist_val = 1
            print("minDist_val = 1 instead of 0")
        if maxRadius_val < minRadius_val:
            print("Max radius val must be >= min radius val.")
            maxRadius_val = minRadius_val
        if minRadius_val < 0 or maxRadius_val < 0:
            print("minRadius_val = {0}".format(minRadius_val))
            print("maxRadius_val = {0}".format(maxRadius_val))
            print("min or max radius is less than 0")
            break
        # if param1_val == 0:
            # param1_val = 1
            # print("param1_val = 1 instead of 0")
        # if param2_val == 0:
            # param2_val = 1
            # print("param2_val = 1 instead of 0")
        # outline = makeWhiteImg(img_temp)
        # raw_input(minDist_val)
        # raw_input(param1_val)
        # raw_input(param2_val)
        # raw_input(minRadius_val)
        # raw_input(maxRadius_val)
        result = cv2.HoughCircles(img_temp, # 8 bit single channel grayscale input image
                                  method=cv2.cv.CV_HOUGH_GRADIENT, # detection method to use
                                  dp=accum_val, # inverse ratio of accumulator resolution. 1 means same resolution, 2 means half
                                  minDist=minDist_val, # min distance between centers of detected circles
                                  # param1: (CV_HOUGH_GRADIENT first parameter) higher threshold of 
                                  # what is passed to canny edge detector
                                  param1=param1_val, 
                                  # param2: (CV_HOUGH_GRADIENT second parameter) accumulator threshold 
                                  # for the circle centers at the detection stage
                                  # The smaller param2 is, the more false circles may be detected.
                                  param2=param2_val,
                                  minRadius=minRadius_val, # minimum circle radius
                                  maxRadius=maxRadius_val) # maximum circle radius
        if result is not None:
            for circle in result[0,:]:
                # cv2.circle(img, center (tuple), radius (float), color (tuple) (B,G,R), thickness)
                cv2.circle(img_temp_color,(circle[0], circle[1]), circle[2], (0,255,128), 2)
                # raw_input(circle[0])
                # raw_input(circle[1])
                drawCrossHairsAtPoint(img_temp_color, circle[0], circle[1], 5)
            # show_img("HoughLines", temp)
        cv2.imshow(window, img_temp_color)

    cv2.destroyAllWindows()


def playWithParams(res,
                   angleMax = 8,
                   thresholdMax = 100,
                   minLineLengthMax = 1000,
                   rhoMax=100,
                   img=None):
    """playWithParams

    :param res: output image from getCannyEdges
    """
    angle = "Angle"
    threshold = "Threshold"
    minLineLength = "MinimumLineLength"
    window = "HoughLines"
    maxLineGap = "Max Line Gap"
    rho = "Rho"
    lines = "Lines"
    cv2.namedWindow("HoughLines", cv2.WINDOW_NORMAL)
    cv2.createTrackbar(angle, window, 172, angleMax, doNothing)
    cv2.createTrackbar(threshold, window, 38, thresholdMax , doNothing)
    cv2.createTrackbar(minLineLength, window, 20, minLineLengthMax, doNothing)
    cv2.createTrackbar(rho, window, 1, rhoMax, doNothing)
    cv2.createTrackbar(lines, window, 1, 500, doNothing)
    cv2.createTrackbar(maxLineGap, window, 10, 100, doNothing)
    while(1):
        temp = res.copy()
        if img is None:
            img_temp = makeWhiteImg(temp)
        else:
            img_temp = img.copy()
        k = cv2.waitKey(1) & 0xFF
        # print(k)
        if k == 10:
            break
        angle_val = cv2.getTrackbarPos(angle, window)
        threshold_val = cv2.getTrackbarPos(threshold, window)
        minLineLength_val = cv2.getTrackbarPos(minLineLength, window)
        rho_val = cv2.getTrackbarPos(rho, window)
        lines_val = cv2.getTrackbarPos(lines, window)
        maxLineGap_val = cv2.getTrackbarPos(maxLineGap, window)
        # raw_input("press enter...")
        if angle_val == 0:
            angle_val = 1
            print("val = 1 instead of 0")
        if rho_val < 1:
            rho_val = 1
            print("rho min is 1")
        # houghlines = getHoughLinesP(temp,
                       # np.pi/angle_val,
                       # threshold=threshold_val,
                       # minLineLength=minLineLength_val,
                       # maxLineGap=10,
                       # rho=rho_val)
        houghlines = cv2.HoughLinesP(image=temp,
                                      rho=1,
                                      theta=np.pi/angle_val,
                                      threshold=threshold_val,
                                      lines=lines_val,
                                      minLineLength=minLineLength_val,
                                      maxLineGap=maxLineGap_val)
        if houghlines is not None:
            for x0, y0, x1, y1 in houghlines[0]:
                # cv2.line(out_numpy_arr, first point, second point, color, thickness)
                cv2.line(img_temp, (x0,y0), (x1,y1), (0,0,0),2)
            # show_img("HoughLines", temp)
        cv2.imshow(window, img_temp)

    cv2.destroyAllWindows()


def doNothing(x):
    pass

def maskImageHSVNoBitwise(img_in, hsv_low, hsv_high):

    img_temp = img_in.copy()
    img_hsv = cv2.cvtColor(img_temp, cv2.COLOR_BGR2HSV)

    # HSV: hue, saturation, value
    lower_range = np.array([hsv_low[0], hsv_low[1], hsv_low[2]])
    upper_range = np.array([hsv_high[0], hsv_high[1], hsv_high[2]])

    mask = cv2.inRange(img_hsv,
                       lower_range,
                       upper_range)

    return mask

def maskImageHSV(img_in, hsv_low, hsv_high):

    img_temp = img_in.copy()
    img_hsv = cv2.cvtColor(img_temp, cv2.COLOR_BGR2HSV)

    # HSV: hue, saturation, value
    lower_range = np.array([hsv_low[0], hsv_low[1], hsv_low[2]])
    upper_range = np.array([hsv_high[0], hsv_high[1], hsv_high[2]])

    mask = cv2.inRange(img_hsv,
                       lower_range,
                       upper_range)

    img = cv2.bitwise_and(img_hsv,
                          img_hsv,
                          mask=mask)
    return img

def maskImage(img, rgb_low, rgb_high):
    img_temp = img.copy()
    hsv_low = rgb_to_hsv(rgb_low[0],rgb_low[1],rgb_low[2])
    # HSV: hue, saturation, value
    lower_range = np.array([hsv_low[0], hsv_low[1], hsv_low[2]]) # RGB: (127, 7, 0)

    hsv_high = rgb_to_hsv(rgb_high[0],rgb_high[1],rgb_high[2])
    upper_range = np.array([hsv_high[0], hsv_high[1], hsv_high[2]]) # RGB: (254, 15, 0)
    img_hsv = cv2.cvtColor(img_temp, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(img_hsv,
                           lower_range,
                           upper_range)
    img = cv2.bitwise_and(img_hsv,
                              img_hsv,
                              mask=mask)
    return img

def getCenterFromHoughLines(houghlines=None, img=None):
    img_temp = img.copy()
    intersections = None
    if houghlines is not None:
        for rho, theta in houghlines[0]:
            factor = 1000
            cos_theta = np.cos(theta)
            sin_theta = np.sin(theta)
            x0 = rho*cos_theta
            y0 = rho*sin_theta
            x = int(x0 + factor*(-sin_theta))
            y = int(y0 + factor*(cos_theta))
            x1 = int(x0 - factor*(-sin_theta))
            y1 = int(y0 - factor*(cos_theta))

            cv2.line(img_temp,
                     (x, y), # first point
                     (x1,y1), # secondpoint
                     (0,0,0), # black color
                     2) # line thickness
        intersections = getAllIntersectionPoints(houghlines)
        count = 0
        for x in range(0, intersections.shape[0]):
            temp_x = int(intersections[x,0])
            temp_y = int(intersections[x,1])
            if False: # this causes problems with autograder TODO: find out why
                drawCrossHairsAtPoint(img_temp,
                                      temp_x,
                                      temp_y,
                                      5)
            count += 1

    if intersections is not None:
        # show_img("{0} points".format(count), img_temp)
        result = np.mean(intersections, axis=0)
        if False: # this causes problems with autograder, TODO: find out why
            drawCrossHairsAtPoint(img_temp,
                                  int(result[0]),
                                  int(result[1]),
                                  5)
        show_img("result", img_temp)

        return int(result[0]), int(result[1])

def playWithMaskHSV(img_temp):

    window = "Play with Mask HSV"
    h_low = "h low"
    s_low = "s low"
    v_low = "v low "

    h_high = "h high"
    s_high = "s high"
    v_high = "v high"

    cv2.namedWindow(window)
    cv2.createTrackbar(h_low, window, 0, 255, doNothing)
    cv2.createTrackbar(s_low, window, 0, 255, doNothing)
    cv2.createTrackbar(v_low, window, 0, 255, doNothing)
    cv2.createTrackbar(h_high, window, 0, 255, doNothing)
    cv2.createTrackbar(s_high, window, 0, 255, doNothing)
    cv2.createTrackbar(v_high, window, 0, 255, doNothing)

    while(1):
        temp_img = img_temp.copy()
        k = cv2.waitKey(1) & 0xFF
        if k == 10:
            break

        hlow_val = cv2.getTrackbarPos(h_low, window)
        slow_val = cv2.getTrackbarPos(s_low, window)
        vlow_val = cv2.getTrackbarPos(v_low, window)
        hhigh_val = cv2.getTrackbarPos(h_high, window)
        shigh_val = cv2.getTrackbarPos(s_high, window)
        vhigh_val = cv2.getTrackbarPos(v_high, window)

        masked_img = maskImageHSV(temp_img,
                               (hlow_val,slow_val,vlow_val),
                               (hhigh_val,shigh_val,vhigh_val))
        res = getCannyEdges(masked_img)

        cv2.imshow(window, res)

    cv2.destroyAllWindows()

def playWithMask(img_temp):

    window = "Play with Mask"
    ylow_b_txt = "ylow_b"
    ylow_g_txt = "ylow_g"
    ylow_r_txt = "ylow_r"

    yhigh_b_txt = "yhigh_b"
    yhigh_g_txt = "yhigh_g"
    yhigh_r_txt = "yhigh_r"

    cv2.namedWindow(window)
    cv2.createTrackbar(ylow_r_txt, window, 0, 255, doNothing)
    cv2.createTrackbar(ylow_g_txt, window, 0, 255, doNothing)
    cv2.createTrackbar(ylow_b_txt, window, 0, 255, doNothing)
    cv2.createTrackbar(yhigh_r_txt, window, 0, 255, doNothing)
    cv2.createTrackbar(yhigh_g_txt, window, 0, 255, doNothing)
    cv2.createTrackbar(yhigh_b_txt, window, 0, 255, doNothing)

    while(1):
        temp_img = img_temp.copy()
        k = cv2.waitKey(1) & 0xFF
        if k == 10:
            break

        ylow_b = cv2.getTrackbarPos(ylow_b_txt, window)
        ylow_g = cv2.getTrackbarPos(ylow_g_txt, window)
        ylow_r = cv2.getTrackbarPos(ylow_r_txt, window)
        yhigh_b = cv2.getTrackbarPos(yhigh_b_txt, window)
        yhigh_g = cv2.getTrackbarPos(yhigh_g_txt, window)
        yhigh_r = cv2.getTrackbarPos(yhigh_r_txt, window)

        masked_img = maskImage(temp_img,
                               (ylow_r,ylow_g,ylow_b),
                               (yhigh_r,yhigh_g,yhigh_b))
        res = getCannyEdges(masked_img)

        cv2.imshow(window, res)
        # cv2.imshow(window, masked_img)

    cv2.destroyAllWindows()


def playWithDilation(img_in):
    window = "Play with Dilation"

    kernel_title = "kernel"
    iterations = "iterations"

    cv2.namedWindow(window)
    cv2.createTrackbar(kernel_title, window, 5, 255, doNothing)
    cv2.createTrackbar(iterations, window, 1, 255, doNothing)

    while(1):
        temp_img = img_in.copy()
        k = cv2.waitKey(1) & 0xFF
        if k == 10:
            break

        kernel_val = cv2.getTrackbarPos(kernel_title, window)
        iterations_val = cv2.getTrackbarPos(iterations, window)
        if kernel_val == 0:
            kernel_val = 1

        kernel = np.ones((kernel_val,kernel_val))
        res = cv2.dilate(temp_img, kernel, iterations=iterations_val)

        cv2.imshow(window, res)

    cv2.destroyAllWindows()


# noisy:
# angle = np.pi/28
# threshold = 13
# minlinlenght = 44
# rho = 1
# lines = 1
# max line gap = 3
def warning_sign_detection(img_in,
                           ylow=(250,250,0),
                           yhigh=(255,255,0),
                           dilate=False,
                           useGreenCanny=False,
                           p_theta=np.pi/16,
                           p_rho=1,
                           p_threshold=7,
                           p_minLineLength=47,
                           p_maxLineGap=10,
                           useHSVMask=False,
                           hsvMaskLow=None,
                           hsvMaskHigh=None,
                           needCannyEdges=True,
                           subtractSecondMask=False,
                           ylow2=(0,0,0),
                           yhigh2=(114, 152, 42)):
    """Finds the centroid coordinates of a warning sign in the
    provided image.

    Args:
        img_in (numpy.array): image containing a traffic light.

    Returns:
        (x,y) tuple of the coordinates of the center of the sign.
    """

    img_temp = img_in.copy()
    # ylow = (250,250,0)
    # yhigh = (255,255,0)
    # show_img("img passed to warning sign detection", img_temp)
    print("ylow:")
    print(ylow)
    print("yhigh:")
    print(yhigh)

    # playWithMask(img_temp)
    # playWithMaskHSV(img_temp)
    # show_img("warning - red img", red_img)
    if useHSVMask:
        red_img = maskImageHSVNoBitwise(img_temp, hsvMaskLow, hsvMaskHigh)
    else:
        red_img = maskImage(img_temp, ylow, yhigh)

    # show_img("red_img", red_img)
    if needCannyEdges:
        res = getCannyEdges(red_img)
        # show_img("res canny edges", res)
    else:
        res = red_img[:, :, 1]
        # show_img("res green channel", res)

    # show_img("warning_sign canny before subtraction", res)
    if subtractSecondMask:
        red_img_2 = maskImage(img_temp, ylow2, yhigh2)
        res2 = getCannyEdges(red_img_2)
        res = res - res2
        res = cv2.subtract(res2, res)
    # show_img("warning_sign canny", res)
    # HoughLinesProbabilistic
    # playWithParams(res, angleMax=100, img=img_temp)
    # angle, threshold, minLineLength, 1
    # 4, 48, 33
    # 4, 42, 139
    # 16, 44, 147
    # 16, 7, 47, 1
    houghsegments = cv2.HoughLinesP(image=res,
                                  rho=p_rho,
                                  theta=p_theta, # angle, radians
                                  threshold=p_threshold,
                                  minLineLength=p_minLineLength,
                                  maxLineGap=p_maxLineGap)
    if houghsegments is not None:
        for x0, y0, x1, y1 in houghsegments[0]:
            # cv2.line(out_numpy_arr, first point, second point, color, thickness)
            cv2.line(img_temp, (x0,y0), (x1,y1), (0,0,0),2)
        result = np.mean(houghsegments[0], axis=0)
        # raw_input(result)
        mid_x = int((result[0] + result[2]) / 2)
        # raw_input(mid_x)
        mid_y = int((result[1] + result[3]) / 2)
        # raw_input(mid_y)
        drawCrossHairsAtPoint(img_temp, mid_x, mid_y, 5)

        # show_img("img_temp", img_temp)

        return mid_x, mid_y



    # HoughLines
    # playWithParamsHoughLines(res, img_temp)
    # Rho, Threshold, Theta
    # 7, 49, 4
    # 7, 57, 4 # best for both!!

    # houghlines = cv2.HoughLines(image=res,  
                                # rho=7, # rho
                                # theta=np.pi/4, # theta 
                                # threshold=57) # threshold

    # return getCenterFromHoughLines(houghlines=houghlines,
                                     # img=img_temp)




def construction_sign_detection(img_in):
    """Finds the centroid coordinates of a construction sign in the
    provided image.

    Args:
        img_in (numpy.array): image containing a traffic light.

    Returns:
        (x,y) tuple of the coordinates of the center of the sign.
    """
    img_temp = img_in.copy()
    ylow = (250,100,0)
    yhigh = (255,128,0)
    red_img = maskImage(img_temp, ylow, yhigh)
    res = getCannyEdges(red_img)
    # HoughLinesProbabilistic
    # playWithParams(res, angleMax=100, img=img_temp)
    # angle, threshold, minLineLength, 1
    # 4, 48, 33
    # 4, 42, 139
    # 16, 44, 147
    # 16, 7, 47, 1
    houghsegments = cv2.HoughLinesP(image=res,
                                  rho=1,
                                  theta=np.pi/16, # angle, radians
                                  threshold=7,
                                  minLineLength=47,
                                  maxLineGap=10)
    if houghsegments is not None:
        for x0, y0, x1, y1 in houghsegments[0]:
            # cv2.line(out_numpy_arr, first point, second point, color, thickness)
            cv2.line(img_temp, (x0,y0), (x1,y1), (0,0,0),2)
        result = np.mean(houghsegments[0], axis=0)
        # raw_input(result)
        mid_x = int((result[0] + result[2]) / 2)
        # raw_input(mid_x)
        mid_y = int((result[1] + result[3]) / 2)
        # raw_input(mid_y)
        # drawCrossHairsAtPoint(img_temp, mid_x, mid_y, 5)

        # show_img("img_temp", img_temp)

        return mid_x, mid_y

# noisy
# p_minDist = 36
# p_param1 = 21
# p_param2 = 20
# p_minRadius = 6
# p_maxRadius = 38
# p_acc_res = 1

def do_not_enter_sign_detection(img_in,
                                rlow=(255, 0, 0),
                                rhigh=(255, 0, 0),
                                p_minDist=15,
                                p_param1=30,
                                p_param2=15,
                                p_minRadius=10,
                                p_maxRadius=38):
    """Find the centroid coordinates of a do not enter sign in the
    provided image.

    Args:
        img_in (numpy.array): image containing a traffic light.

    Returns:
        (x,y) typle of the coordinates of the center of the sign.
    """
    img_temp = img_in.copy()
    # show_img("do not enter", img_temp)
    # the red color in the do not enter sign is RGB(255, 0, 0)
    # rlow = (255,255,255)
    # rhigh = (255,255,255)
    # rlow = (255, 0, 0)
    # rhigh = (255, 0, 0)
    # playWithMask(img_temp)
    red_img = maskImage(img_temp, rlow, rhigh)
    res = getCannyEdges(red_img)
    # show_img("canny", res)

    # THERE ARE TWO POSSIBLE SOLUTIONS FOR DETECTING THE DO NOT ENTER 
    # SIGN WHEN IT'S NEAR OTHER SIGNS
    # 1. FILTER OUT SEGMENTS THAT ARE NOT INSIDE CIRCLES (HARDER)
    # 2. FILTER OUT SEGMENTS THAT DONT HAVE THE MOST COMMON LENGTH FOUND IN THE NUMPY ARRAY
    # 3. MASK WITH RED INSTEAD AND THEN FIND CIRCLES instead of segments

    # option: get hough circles and then find out which hough circles have 
    # additional segments inside of them!
    # minRadius = 10
    # maxRadius = 50
    # playWithParamsHoughCircles(res, img_temp, minRadius, maxRadius)
    # minDist, param1, param2, minRadius, maxRadius, accumulator resolution inverse
    # 15, 30 15, 10, 38, 1

    hough_circles = cv2.HoughCircles(res, # 8 bit single channel grayscale input image
                              method=cv2.cv.CV_HOUGH_GRADIENT, # detection method to use
                              dp=1, # inverse ratio of accumulator resolution. 1 means same resolution, 2 means half
                              minDist=p_minDist, # min distance between centers of detected circles
                              # param1: (CV_HOUGH_GRADIENT first parameter) higher threshold of 
                              # what is passed to canny edge detector
                              param1=p_param1, 
                              # param2: (CV_HOUGH_GRADIENT second parameter) accumulator threshold 
                              # for the circle centers at the detection stage
                              # The smaller param2 is, the more false circles may be detected.
                              param2=p_param2,
                              minRadius=p_minRadius, # minimum circle radius
                              maxRadius=p_maxRadius) # maximum circle radius
    if hough_circles is not None:
        # drawCircles(img_temp, hough_circles)
        if hough_circles[0].shape[0] == 1:
            # raw_input(hough_circles[0])
            # raw_input(type(hough_circles[0][0]))
            x = int(hough_circles[0][0][0])
            y = int(hough_circles[0][0][1])
            # rho = hough_circles[0][0][2]
            return x, y



def traffic_sign_detection(img_in):
    """Finds all traffic signs in a synthetic image.

    The image may contain at least one of the following:
    - traffic_light
    - no_entry
    - stop
    - warning
    - yield
    - construction

    Use these names for your output.

    See the instructions document for a visual definition of each
    sign.

    Args:
        img_in (numpy.array): input image containing at least one
                              traffic sign.

    Returns:
        dict: dictionary containing only the signs present in the
              image along with their respective centroid coordinates
              as tuples.

              For example: {'stop': (1, 3), 'yield': (4, 11)}
              These are just example values and may not represent a
              valid scene.
    """
    img_temp = img_in.copy()

    # show_img("image scene", img_temp)
    result = {}

    # - traffic_light
    traffic_light_location = traffic_light_detection(img_temp, [10,30])
    # raw_input(traffic_light_location)
    if traffic_light_location is not None:
        result["traffic_light"] = traffic_light_location

    # - yield
    yield_sign_location = yield_sign_detection(img_temp)
    # raw_input(yield_sign_location)
    if yield_sign_location is not None:
        result["yield"] = yield_sign_location


    # - stop
    stop_sign_location = stop_sign_detection(img_temp)
    # raw_input(stop_sign_location)
    if stop_sign_location is not None:
        result["stop"] = stop_sign_location

    # - no_entry
    do_not_enter_location = do_not_enter_sign_detection(img_temp)
    # raw_input(do_not_enter_location)
    if do_not_enter_location is not None:
        result["no_entry"] = do_not_enter_location
    
    # - warning
    warning_location = warning_sign_detection(img_temp)
    # raw_input(warning_location)
    if warning_location is not None:
        result["warning"] = warning_location

    # - construction
    construction_location = construction_sign_detection(img_temp)
    # raw_input(construction_location)
    if construction_location is not None:
        result["construction"] = construction_location

    img_temp = drawResult(result, img_temp)
    # show_img("image scene", img_temp)
    return result

def playWithDenoise(img_temp):

    h = "h"
    hForColor = "hForColorComponents"
    templateWinSize = "Template Window Size (best if odd)"
    searchWinSize = "Search Window Size (best if odd)"
    window = "NL Means Denoise"
    cv2.namedWindow(window)
    cv2.createTrackbar(h, window, 10, 100, doNothing)
    cv2.createTrackbar(hForColor, window, 10, 100, doNothing)
    cv2.createTrackbar(templateWinSize, window, 7, 100, doNothing)
    cv2.createTrackbar(searchWinSize, window, 21, 100, doNothing)

    while(1):
        temp_img = img_temp.copy()
        temp_img_2 = img_temp.copy()
        k = cv2.waitKey(1) & 0xFF
        if k == 10:
            break
        h_val = cv2.getTrackbarPos(h, window)
        hForColorComponents_val = cv2.getTrackbarPos(hForColor, window)
        templateWindowSize_val = cv2.getTrackbarPos(templateWinSize, window)
        searchWindowSize_val = cv2.getTrackbarPos(searchWinSize, window)

        # documentation is found here: 
        # https://docs.opencv.org/3.0-beta/modules/photo/doc/denoising.html
        cv2.fastNlMeansDenoisingColored(src=temp_img,
                                         dst=temp_img_2,
                                         h=h_val, # 
                                         hColor=hForColorComponents_val,
                                         templateWindowSize=templateWindowSize_val, #
                                         searchWindowSize=searchWindowSize_val)
        # raw_input("Press enter to regenerate image.")


        cv2.imshow(window, temp_img_2)

    cv2.destroyAllWindows()

def playWithBilateralFilter(img_temp):

    d = "Diameter of pixel neighborhood"
    sigmaColor= "Filter sigma in the color space"
    sigmaSpace = "Filter sigma in the coordinate space"
    window = "Bilateral Filter"
    cv2.namedWindow(window)
    cv2.createTrackbar(d, window, 9, 100, doNothing)
    cv2.createTrackbar(sigmaColor, window, 75, 100, doNothing)
    cv2.createTrackbar(sigmaSpace, window, 75, 100, doNothing)

    while(1):
        temp_img = img_temp.copy()
        temp_img_2 = img_temp.copy()
        k = cv2.waitKey(1) & 0xFF
        if k == 10:
            break
        d_val = cv2.getTrackbarPos(d, window)
        sigmaColor_val = cv2.getTrackbarPos(sigmaColor, window)
        sigmaSpace_val = cv2.getTrackbarPos(sigmaSpace, window)

        # documentation is found here: 
        # https://docs.opencv.org/3.0-beta/modules/photo/doc/denoising.html
        cv2.bilateralFilter(src=temp_img,
                            dst=temp_img_2,
                            d=d_val,
                            sigmaColor=sigmaColor_val,
                            sigmaSpace=sigmaSpace_val)
        # raw_input("Press enter to regenerate image.")


        cv2.imshow(window, temp_img_2)

    cv2.destroyAllWindows()

def playWithGaussianBlur(img_temp):

    kSize = "kSize"
    # kSize_y = "kSize_y"
    sigmaX = "sigmaX"
    sigmaY = "sigmaY"
    window = "Gaussian Blur"
    cv2.namedWindow(window)
    cv2.createTrackbar(kSize, window, 5, 50, doNothing)
    # cv2.createTrackbar(kSize_y, window, 5, 50, doNothing)
    cv2.createTrackbar(sigmaX, window, 0, 20, doNothing)
    cv2.createTrackbar(sigmaY, window, 0, 20, doNothing)

    while(1):
        temp_img = img_temp.copy()
        out_img = img_temp.copy()
        k = cv2.waitKey(1) & 0xFF
        if k == 10:
            break
        kSize_val = cv2.getTrackbarPos(kSize, window)
        # kSize_y_val = cv2.getTrackbarPos(kSize_y, window)
        sigmaX_val = cv2.getTrackbarPos(sigmaX, window)
        sigmaY_val = cv2.getTrackbarPos(sigmaY, window)

        if kSize_val % 2 == 0:
            kSize_val += 1
            print(kSize_val)
        # documentation is found here: 
        # https://docs.opencv.org/2.4/modules/imgproc/doc/filtering.html?highlight=gaussianblur#gaussianblur
        # https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_core/py_basic_ops/py_basic_ops.html
        # out_img = cv2.GaussianBlur(src=temp_img,
                                   # ksize=(kSize_val,kSize_val),
                                   # sigmaX=sigmaX_val)
        out_img = cv2.GaussianBlur(src=temp_img,
                                   ksize=(kSize_val,kSize_val), 
                                   sigmaX=sigmaX_val,
                                   sigmaY=sigmaY_val,
                                   borderType=cv2.BORDER_DEFAULT)
        # raw_input("Press enter to regenerate image.")


        cv2.imshow(window, out_img)

    cv2.destroyAllWindows()

def playWithMedianBlur(img_temp):

    kSize = "kSize"
    window = "Median Blur"
    cv2.namedWindow(window)
    cv2.createTrackbar(kSize, window, 3, 50, doNothing)

    while(1):
        temp_img = img_temp.copy()
        out_img = img_temp.copy()
        k = cv2.waitKey(1) & 0xFF
        if k == 10:
            break
        kSize_val = cv2.getTrackbarPos(kSize, window)
        if (kSize_val % 2 == 0):
            kSize_val += 1
        if (kSize_val == 0):
            kSize_val = 1

        print("kSize = {0}".format(kSize_val))
        # documentation is found here: 
        cv2.medianBlur(src=temp_img,
                       dst=out_img,
                       ksize=kSize_val) # ksize must be odd: 3,5,7, ...

        cv2.imshow(window, out_img)

    cv2.destroyAllWindows()

def traffic_sign_detection_noisy(img_in):
    """Finds all traffic signs in a synthetic noisy image.

    The image may contain at least one of the following:
    - traffic_light
    - no_entry
    - stop
    - warning
    - yield
    - construction

    Use these names for your output.

    See the instructions document for a visual definition of each
    sign.

    Args:
        img_in (numpy.array): input image containing at least one
                              traffic sign.

    Returns:
        dict: dictionary containing only the signs present in the
              image along with their respective centroid coordinates
              as tuples.

              For example: {'stop': (1, 3), 'yield': (4, 11)}
              These are just example values and may not represent a
              valid scene.
    """
    #TODO: use guassianblur or medianblur to filter out the noise.
    # STRATEGIES
    # 1. Pass more parameters to each function to control individual sign detection behavior
    # without actually modifying the original run.sh output and behavior
    # 2. try denoising differently for each individaul sign
    # 3. lookup best method to denoise gaussian noise.
    # 4. Investigate the characteristics of the noise provided and find better ways of denoising it.
    # 5. Read up: https://www.bogotobogo.com/python/OpenCV_Python/python_opencv3_Image_Non-local_Means_Denoising_Algorithm_Noise_Reduction.php

    img_noisy = img_in.copy()
    img_fastNL = img_in.copy()
    img_fastNL_traffic = img_in.copy()
    img_gaussBlur = img_in.copy()
    img_bFilter = img_in.copy()
    img_bFilter_traffic = img_in.copy()
    img_medianBlur = img_in.copy()
    img_temp = img_in.copy()
    # show_img("Noisy Image", img_temp)

    cv2.fastNlMeansDenoisingColored(src=img_noisy,
                                    dst=img_fastNL_traffic,
                                    h=17,
                                    hColor=17,
                                    templateWindowSize=7,
                                    searchWindowSize=21)
    cv2.bilateralFilter(src=img_fastNL_traffic,
                        dst=img_bFilter_traffic,
                        d=50,
                        sigmaColor=75,
                        sigmaSpace=75)

    cv2.fastNlMeansDenoisingColored(src=img_noisy,
                                    dst=img_fastNL,
                                    h=10,
                                    hColor=10,
                                    templateWindowSize=7,
                                    searchWindowSize=21)
    # Good for noise removal while keeping edges sharp
    cv2.bilateralFilter(src=img_fastNL,
                        dst=img_bFilter,
                        d=50,
                        sigmaColor=75,
                        sigmaSpace=75)

    result = {}

    #################
    # - traffic_light (GOOD)
    #################
    # playWithDenoise(img_temp)

    # show_img("Image - Fast NL Means Denoising", img_fastNL)
    traffic_light_location = traffic_light_detection(img_bFilter_traffic, [10,30])
    # raw_input(traffic_light_location)
    if traffic_light_location is not None:
        result["traffic_light"] = traffic_light_location
    else:
        print("Did not detect a traffic light.")

    #################
    # - yield
    #################
    yield_sign_location = yield_sign_detection(img_bFilter,
                                               rgb_low=(194,44,44),
                                               rgb_high=(255,70,0),
                                               p_rho=1,
                                               p_theta=np.pi/272,
                                               p_threshold=45,
                                               p_minLineLength=90,
                                               p_maxLineGap=10)
    if yield_sign_location is not None:
        result["yield"] = yield_sign_location
    else:
        print("Did not detect yield sign.")


    #################
    # - stop (GOOD)
    #################
    stop_sign_location = stop_sign_detection(img_bFilter,
                                             rgb_low=(0,0,0),
                                             rgb_high=(225,6,0),
                                             p_rho=1,
                                             p_theta=np.pi/1,
                                             # p_threshold=30,
                                             p_threshold=36,
                                             p_lines=1,
                                             p_minLineLength=41,
                                             p_maxLineGap=7)
    if stop_sign_location is not None:
        result["stop"] = stop_sign_location
    else:
        print("Did not detect stop sign.")

    #################
    # - no_entry (GOOD)
    #################
    # use the same img_bFilter that the stop sign uses
    do_not_enter_location = do_not_enter_sign_detection(img_bFilter,
                                                        rlow=(183,110,109),
                                                        rhigh=(255,0,0),
                                                        p_minDist=24,
                                                        p_param1=25,
                                                        p_param2=31,
                                                        p_minRadius=6,
                                                        p_maxRadius=43)
    # raw_input(do_not_enter_location)
    if do_not_enter_location is not None:
        result["no_entry"] = do_not_enter_location
    else:
        print("Did not detect do not enter sign.")

    #################
    # warning sign (GOOD)
    #################
    warning_location = warning_sign_detection(img_bFilter,
                                              ylow=(118,117,113), # (0,0,0)
                                              yhigh=(232,255,0), # (255,255,0)
                                              useGreenCanny=False,
                                              dilate=False,
                                              p_theta=np.pi/28,
                                              p_rho=1,
                                              p_threshold=13,
                                              p_minLineLength=44,
                                              p_maxLineGap=3)

    if warning_location is not None:
        result["warning"] = warning_location
    else:
        print("Did not detect warning sign.")

    #################
    # - construction (GOOD)
    #################
    construction_location = construction_sign_detection(img_noisy)
    if construction_location is not None:
        result["construction"] = construction_location
    else:
        print("Did not detect construction sign.")

    img_temp = drawResult(result, img_temp)

    # show_img("noisy image scene", img_temp)
    return result


def traffic_sign_detection_challenge(img_in):
    """Finds traffic signs in an real image

    See point 5 in the instructions for details.

    Args:
        img_in (numpy.array): input image containing at least one
                              traffic sign.

    Returns:
        dict: dictionary containing only the signs present in the
              image along with their respective centroid coordinates
              as tuples.

              For example: {'stop': (1, 3), 'yield': (4, 11)}
              These are just example values and may not represent a
              valid scene.
    """
    img_temp = img_in.copy()
    # show_img("real world (part 5)", img_temp)
    result = {}

    # stop_sign_location = stop_sign_detection(img_temp,
                                             # rgb_low=(90,90,90),
                                             # rgb_high=(255,159,182),
                                             # p_rho=1,
                                             # p_theta=np.pi/466,
                                             # p_threshold=38,
                                             # p_lines=1,
                                             # p_minLineLength=66,
                                             # p_maxLineGap=1)
    stop_sign_location = stop_sign_detection(img_temp,
                                             rgb_low=(201,201,201),
                                             rgb_high=(255,166,169),
                                             p_rho=1,
                                             p_theta=np.pi/80,
                                             p_threshold=3,
                                             p_lines=1,
                                             p_minLineLength=58,
                                             p_maxLineGap=4)
                                             # p_maxLineGap=10)
    if stop_sign_location is not None:
        result["stop"] = stop_sign_location
    else:
        print("Did not detect stop sign.")

    warning_location = warning_sign_detection(img_temp,
                                              ylow=(228,165,138), # (0,0,0)
                                              yhigh=(0,255,255), # (255,255,0)
                                              useGreenCanny=False,
                                              dilate=False,
                                              p_theta=np.pi/28,
                                              p_rho=1,
                                              p_threshold=13,
                                              p_minLineLength=44,
                                              p_maxLineGap=3)

    if warning_location is not None:
        result["warning"] = warning_location
    else:
        print("Did not detect warning sign.")

    do_not_enter_location = do_not_enter_sign_detection(img_temp,
                                                        rlow=(75,43,43),
                                                        rhigh=(255,83,65),
                                                        p_minDist=66,
                                                        p_param1=12,
                                                        p_param2=22,
                                                        p_minRadius=21,
                                                        p_maxRadius=33)
    # raw_input(do_not_enter_location)
    if do_not_enter_location is not None:
        result["no_entry"] = do_not_enter_location
    else:
        print("Did not detect do not enter sign.")

    img_temp = drawResult(result, img_temp)
    # show_img("real world result", img_temp)
    return result

def drawResult(result, img_temp):
    for key, value in result.iteritems():
        if key == "traffic_light":
            x = value[0][0]
            y = value[0][1]
            text1 = "{0}".format(key)
            text2 = "({0},{1})".format(x, y)
            text3 = "{0}".format(value[1])
            x_temp = x - 35
            if x_temp < 0:
                x_temp = 0 
            addText(img_temp, text1, (x_temp, y+50), color=(0, 0, 0))
            addText(img_temp, text2, (x_temp, y+75), color=(0, 0, 0))
            addText(img_temp, text3, (x_temp, y+100), color=(0, 0, 0))
        else:
            x = value[0]
            y = value[1]
            x_temp = x - 35
            if x_temp < 0:
                x_temp = 0 
            text1 = "{0}".format(key)
            text2= "({0},{1})".format(x, y)
            addText(img_temp, text1, (x_temp, y+50), color=(0, 0, 0))
            addText(img_temp, text2, (x_temp, y+75), color=(0, 0, 0))

        drawCrossHairsAtPoint(img_temp, x, y, 5)

    return img_temp

def addText(img, text, location, color=(0, 0, 0)):
    print(color)
    print(location)
    # fontFace = cv2.FONT_HERSHEY_DUPLEX
    fontFace = cv2.FONT_HERSHEY_SIMPLEX
    location = location
    fontScale = 0.6
    lineType = 2
    thickness = 2
    cv2.putText(img=img,
                text=text,
                org=location,
                fontFace=fontFace,
                fontScale=fontScale,
                color=color,
                lineType=lineType,
                thickness=thickness)
