import math
import numpy as np
import cv2
import sys

# # Implement the functions below.


def extract_red(image):
    """ Returns the red channel of the input image. It is highly recommended to make a copy of the
    input image in order to avoid modifying the original array. You can do this by calling:
    temp_image = np.copy(image)

    Args:
        image (numpy.array): Input RGB (BGR in OpenCV) image.

    Returns:
        numpy.array: Output 2D array containing the red channel.
    """
    # BGR = channels 0,1,2
    result = np.copy(image)
    return result[:,:,2]


def extract_green(image):
    """ Returns the green channel of the input image. It is highly recommended to make a copy of the
    input image in order to avoid modifying the original array. You can do this by calling:
    temp_image = np.copy(image)

    Args:
        image (numpy.array): Input RGB (BGR in OpenCV) image.

    Returns:
        numpy.array: Output 2D array containing the green channel.
    """
    # BGR = channels 0,1,2
    result = np.copy(image)
    return result[:,:,1]


def extract_blue(image):
    """ Returns the blue channel of the input image. It is highly recommended to make a copy of the
    input image in order to avoid modifying the original array. You can do this by calling:
    temp_image = np.copy(image)

    Args:
        image (numpy.array): Input RGB (BGR in OpenCV) image.

    Returns:
        numpy.array: Output 2D array containing the blue channel.
    """
    # BGR = channels 0,1,2
    result = np.copy(image)
    return result[:,:,0]


def swap_green_blue(image):
    """ Returns an image with the green and blue channels of the input image swapped. It is highly
    recommended to make a copy of the input image in order to avoid modifying the original array.
    You can do this by calling:
    temp_image = np.copy(image)

    Args:
        image (numpy.array): Input RGB (BGR in OpenCV) image.

    Returns:
        numpy.array: Output 3D array with the green and blue channels swapped.
    """

    result = np.copy(image)
    # opencv: BGR: blue, green, red = channels 0,1,2
    # blue
    # raw_input(image[:,:,0])
    # green
    # raw_input(image[:,:,1])
    # red
    # raw_input(image[:,:,2])
    result[:,:,0] = image[:,:,1]
    result[:,:,1] = image[:,:,0]
    return result


def copy_paste_middle(src, dst, shape):
    """ Copies the middle region of size shape from src to the middle of dst. It is
    highly recommended to make a copy of the input image in order to avoid modifying the
    original array. You can do this by calling:
    temp_image = np.copy(image)

        Note: Assumes that src and dst are monochrome images, i.e. 2d arrays.

        Note: Where 'middle' is ambiguous because of any difference in the oddness
        or evenness of the size of the copied region and the image size, the function
        rounds downwards.  E.g. in copying a shape = (1,1) from a src image of size (2,2)
        into an dst image of size (3,3), the function copies the range [0:1,0:1] of
        the src into the range [1:2,1:2] of the dst.

    Args:
        src (numpy.array): 2D array where the rectangular shape will be copied from.
        dst (numpy.array): 2D array where the rectangular shape will be copied to.
        shape (tuple): Tuple containing the height (int) and width (int) of the section to be
                       copied.

    Returns:
        numpy.array: Output monochrome image (2D array)
    """
    src_temp = np.copy(src)
    dst_temp = np.copy(dst)
    # in order to round downward, use integer division.
    # in python 2 it will return the math.floor
    src_mid_x = src_temp.shape[0] / 2 - shape[0] / 2 
    src_mid_y = src_temp.shape[1] / 2 - shape[1] / 2
    dst_mid_x = dst_temp.shape[0] / 2 - shape[0] / 2 
    dst_mid_y = dst_temp.shape[1] / 2 - shape[1] / 2
    dst_temp[dst_mid_x:dst_mid_x+shape[0],
             dst_mid_y:dst_mid_y+shape[1]] = src_temp[src_mid_x:src_mid_x+shape[0],
                                                      src_mid_y:src_mid_y+shape[1]]
    return dst_temp


def image_stats(image):
    """ Returns the tuple (min,max,mean,stddev) of statistics for the input monochrome image.
    In order to become more familiar with Numpy, you should look for pre-defined functions
    that do these operations i.e. numpy.min.

    It is highly recommended to make a copy of the input image in order to avoid modifying
    the original array. You can do this by calling:
    temp_image = np.copy(image)

    Args:
        image (numpy.array): Input 2D image.

    Returns:
        tuple: Four-element tuple containing:
               min (float): Input array minimum value.
               max (float): Input array maximum value.
               mean (float): Input array mean / average value.
               stddev (float): Input array standard deviation.
    """

    temp = np.copy(image)

    # ensure output values are all float64
    temp = temp.astype("float64")
    # min(), max(): without specifying along with axis to find the min and max values for, the min and max values
    # for the entire ndarray will be returned.

    minimum = temp.min()
    maximum = temp.max()
    mean_val = temp.mean()
    stddev = temp.std()
    return minimum, maximum, mean_val, stddev

def center_and_normalize(image, scale):
    """ Returns an image with the same mean as the original but with values scaled about the
    mean so as to have a standard deviation of "scale".

    Note: This function makes no defense against the creation
    of out-of-range pixel values.  Consider converting the input image to
    a float64 type before passing in an image.

    It is highly recommended to make a copy of the input image in order to avoid modifying
    the original array. You can do this by calling:
    temp_image = np.copy(image)

    Args:
        image (numpy.array): Input 2D image.
        scale (int or float): scale factor.

    Returns:
        numpy.array: Output 2D image.
    """


    """
    Subtract the mean from all pixels in the monochrome image, 
    then divide by standard deviation, 
    then multiply by the scaling factor 10 if your image is 0 to 255 or 0.05
    if your image ranges from 0.0 to 1.0. Now, add the mean back into the product. 
    """

    temp = np.copy(image)

    # ensure dtype is float64 and not uint8, for example
    # raw_input("temp: {0}, temp.dtype: {1}".format(temp, temp.dtype))
    temp = temp.astype("float64")
    # raw_input("temp (as float64): {0}".format(temp))

    # get image stats
    minimum, maximum, mean_val, stddev = image_stats(temp)

    # subtract mean from all pixels
    temp -= mean_val
    # ensure 0 is our floor
    # temp[temp < 0] = 0
    # raw_input("temp - mean-val: {0}".format(temp))

    # divide by stddev
    temp = temp / stddev

    # multiply by the scaling factor
    if maximum <= 1:
        temp *= scale
    elif maximum <= 255:
        temp *= scale

    # add mean back into product
    temp += mean_val

    return temp

def shift_image_left(image, shift):
    """ Outputs the input monochrome image shifted shift pixels to the left.

    The returned image has the same shape as the original with
    the BORDER_REPLICATE rule to fill-in missing values.  See

    http://docs.opencv.org/2.4/doc/tutorials/imgproc/imgtrans/copyMakeBorder/copyMakeBorder.html?highlight=copy

    for further explanation.

    It is highly recommended to make a copy of the input image in order to avoid modifying
    the original array. You can do this by calling:
    temp_image = np.copy(image)

    Args:
        image (numpy.array): Input 2D image.
        shift (int): Displacement value representing the number of pixels to shift the input image.
            This parameter may be 0 representing zero displacement.

    Returns:
        numpy.array: Output shifted 2D image.
    """

    """
    Q : In shift_image_left(), what does shifting to the left by 2 pixels mean?
    A: You can remove the first 2 columns, and add 2 blank columns to the right and replicate the last 2 columns on the right.
    """
    temp = np.copy(image)
    if shift == 0:
        return temp
    shifted = np.roll(temp, -shift)

    # the vacant columns will all be equal to the right most original column, hence "replicated"
    shifted[:, -shift:] = temp[:, -1:]
    return shifted


def difference_image(img1, img2):
    """ Returns the difference between the two input images (img1 - img2). The resulting array must be normalized
    and scaled to fit [0, 255].

    It is highly recommended to make a copy of the input image in order to avoid modifying
    the original array. You can do this by calling:
    temp_image = np.copy(image)

    Args:
        img1 (numpy.array): Input 2D image.
        img2 (numpy.array): Input 2D image.

    Returns:
        numpy.array: Output 2D image containing the result of subtracting img2 from img1.
    """

    """
    Subtract the shifted version of img1_green from the original img1_green, 
    and save the difference image. 
    """

    # ensure to conver to float. see 2A-L1, Lecture #29
    img1_temp = np.copy(img1)
    img1_temp = img1_temp.astype("float64")
    img2_temp = np.copy(img2)
    img2_temp = img2_temp.astype("float64")

    # See 2A-L1, Lecture #29: Difference = (a - b) + (b - a)
    # this calculates the absolute difference between the two images, since
    # when values are negative we truncate to 0, and so the opposite operation gives a positive result
    # and is added to 0.
    # e.g. 
    # let a = 7 and b = 9, with [0, 255] truncation:
    # (7 - 9) + (9 - 7) = 0 + 2 = 2
    # result = (img1_temp - img2_temp) + (img2_temp - img1_temp)
    result = img1_temp - img2_temp

    result = cv2.normalize(result, None, 0, 255, cv2.NORM_MINMAX)

    return result


def add_noise(image, channel, sigma):
    """ Returns a copy of the input color image with Gaussian noise added to
    channel (0-2). The Gaussian noise mean must be zero. The parameter sigma
    controls the standard deviation of the noise.

    The returned array values must not be clipped or normalized and scaled. This means that
    there could be values that are not in [0, 255].

    Note: This function makes no defense against the creation
    of out-of-range pixel values.  Consider converting the input image to
    a float64 type before passing in an image.

    It is highly recommended to make a copy of the input image in order to avoid modifying
    the original array. You can do this by calling:
    temp_image = np.copy(image)

    Args:
        image (numpy.array): input RGB (BGR in OpenCV) image.
        channel (int): Channel index value.
        sigma (float): Gaussian noise standard deviation.

    Returns:
        numpy.array: Output 3D array containing the result of adding Gaussian noise to the
            specified channel.
    """
    temp = np.copy(image)
    # ensure dtype is float64 and not uint8, for example
    temp = temp.astype("float64")
    temp_channel = np.copy(temp[:,:,channel])
    # raw_input("temp_channel.shape = {0}".format(temp_channel.shape))
    rows_of_noise = temp_channel.shape[0]
    cols_of_noise = temp_channel.shape[1]

    # generate the gaussian noise, multiply by sigma
    noise = np.random.randn(rows_of_noise, cols_of_noise)*sigma
    temp_channel += noise

    # no need to clip off out of range pixels (see above)
    # temp_channel[temp_channel > 255] = 255
    # temp_channel[temp_channel < 0] = 0


    temp[:,:,channel] = temp_channel
    return temp

