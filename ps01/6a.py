import numpy as np
import cv2

from ps1 import *

if __name__ == '__main__':
    filename = "southafricaflagface.png"
    image = cv2.imread(filename)
    red_channel = extract_red(image)
    green_channel = extract_green(image)
    blue_channel = extract_blue(image)


    cv2.imwrite("{0}_red.png".format(filename.split(".")[0]), red_channel)
    cv2.imwrite("{0}_green.png".format(filename.split(".")[0]), green_channel)
    cv2.imwrite("{0}_blue.png".format(filename.split(".")[0]), blue_channel)

