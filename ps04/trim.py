#!/usr/bin/python3

from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip

t1 = 19 
t2 = 29

ffmpeg_extract_subclip("my-ad.mp4", t1, t2, targetname="my-ad-shortened.mp4")
