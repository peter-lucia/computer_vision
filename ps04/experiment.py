"""Problem Set 4: Motion Detection"""

import cv2
import os
import numpy as np

import ps4

# I/O directories
input_dir = "input_images"
output_dir = "output"


# Utility code
def quiver(u, v, scale, stride, color=(0, 255, 0), img_out=None, img_out_gray=True):

    if img_out is None:
        img_out = np.zeros((v.shape[0], u.shape[1], 3), dtype=np.uint8)
    else:
        img_out = img_out.copy()
        if img_out_gray:
            img_out = cv2.cvtColor(img_out, cv2.COLOR_GRAY2RGB)

    for y in xrange(0, v.shape[0], stride):

        for x in xrange(0, u.shape[1], stride):

            cv2.line(img_out, (x, y), (x + int(u[y, x] * scale),
                                       y + int(v[y, x] * scale)), color, 1)
            cv2.circle(img_out, (x + int(u[y, x] * scale),
                                 y + int(v[y, x] * scale)), 1, color, 1)
    return img_out


# Functions you need to complete:

def scale_u_and_v(u, v, level, pyr):
    """Scales up U and V arrays to match the image dimensions assigned 
    to the first pyramid level: pyr[0].

    You will use this method in part 3. In this section you are asked 
    to select a level in the gaussian pyramid which contains images 
    that are smaller than the one located in pyr[0]. This function 
    should take the U and V arrays computed from this lower level and 
    expand them to match a the size of pyr[0].

    This function consists of a sequence of ps4.expand_image operations 
    based on the pyramid level used to obtain both U and V. Multiply 
    the result of expand_image by 2 to scale the vector values. After 
    each expand_image operation you should adjust the resulting arrays 
    to match the current level shape 
    i.e. U.shape == pyr[current_level].shape and 
    V.shape == pyr[current_level].shape. In case they don't, adjust
    the U and V arrays by removing the extra rows and columns.

    Hint: create a for loop from level-1 to 0 inclusive.

    Both resulting arrays' shapes should match pyr[0].shape.

    Args:
        u: U array obtained from ps4.optic_flow_lk
        v: V array obtained from ps4.optic_flow_lk
        level: level value used in the gaussian pyramid to obtain U 
               and V (see part_3)
        pyr: gaussian pyramid used to verify the shapes of U and V at 
             each iteration until the level 0 has been met.

    Returns:
        tuple: two-element tuple containing:
            u (numpy.array): scaled U array of shape equal to 
                             pyr[0].shape
            v (numpy.array): scaled V array of shape equal to 
                             pyr[0].shape
    """

    # TODO: Your code here
    # Hint: create a for loop from level-1 to 0 inclusive.
    u = u.copy()
    v = v.copy()

    # This function consists of a sequence of ps4.expand_image operations 
    # based on the pyramid level used to obtain both U and V. Multiply 
    # the result of expand_image by 2 to scale the vector values. After 
    # each expand_image operation you should adjust the resulting arrays 
    # to match the current level shape 
    for i in range(level-1, -1, -1):
        u = ps4.expand_image(u)
        v = ps4.expand_image(v)
        img = ps4.expand_image(pyr[i])
        
        h_diff = u.shape[0] - img.shape[0]
        w_diff = u.shape[1] - img.shape[1]
        if h_diff != 0 and w_diff != 0:
            if h_diff == 1 and w_diff == 1:
                u = u[:-1,:-1]
                v = v[:-1,:-1]
        # elif h_diff != 0:
            # if h_diff == 1:
                # u = u[:-1, :]
                # v = v[:-1, :]
        # elif w_diff != 0:
            # if w_diff == 1:
                # u = u[:, :-1]
                # v = v[:, :-1]

        u *= 2
        v *= 2

    return u, v


def part_1a():

    shift_0 = cv2.imread(os.path.join(input_dir, 'TestSeq',
                                      'Shift0.png'), 0) / 255.
    shift_r2 = cv2.imread(os.path.join(input_dir, 'TestSeq', 
                                       'ShiftR2.png'), 0) / 255.
    shift_r5_u5 = cv2.imread(os.path.join(input_dir, 'TestSeq', 
                                          'ShiftR5U5.png'), 0) / 255.

    # Optional: smooth the images if LK doesn't work well on raw images
    k_size = 15  # TODO: Select a kernel size

    # Gaussian kernels help when there is non-uniform motion.
    # Gaussian kernel size must be larger than uniform kernel sizes

    k_type = "uniform"  # TODO: Select a kernel type
    sigma = 1  # TODO: Select a sigma value if you are using a gaussian kernel
    u, v = ps4.optic_flow_lk(shift_0, shift_r2, k_size, k_type, sigma)

    # Flow image
    u_v = quiver(u, v, scale=3, stride=10)
    cv2.imwrite(os.path.join(output_dir, "ps4-1-a-1.png"), u_v)

    # Now let's try with ShiftR5U5. You may want to try smoothing the
    # input images first.

    k_size = 50  # TODO: Select a kernel size
    k_type = "uniform"  # TODO: Select a kernel type
    sigma = 30  # TODO: Select a sigma value if you are using a gaussian kernel
    u, v = ps4.optic_flow_lk(shift_0, shift_r5_u5, k_size, k_type, sigma)

    # Flow image
    u_v = quiver(u, v, scale=3, stride=10)
    cv2.imwrite(os.path.join(output_dir, "ps4-1-a-2.png"), u_v)


def part_1b():
    """Performs the same operations applied in part_1a using the images
    ShiftR10, ShiftR20 and ShiftR40.

    You will compare the base image Shift0.png with the remaining
    images located in the directory TestSeq:
    - ShiftR10.png
    - ShiftR20.png
    - ShiftR40.png

    Make sure you explore different parameters and/or pre-process the
    input images to improve your results.

    In this part you should save the following images:
    - ps4-1-b-1.png
    - ps4-1-b-2.png
    - ps4-1-b-3.png

    Returns:
        None
    """
    shift_0 = cv2.imread(os.path.join(input_dir, 'TestSeq',
                                      'Shift0.png'), 0) / 255.
    shift_r10 = cv2.imread(os.path.join(input_dir, 'TestSeq',
                                        'ShiftR10.png'), 0) / 255.
    shift_r20 = cv2.imread(os.path.join(input_dir, 'TestSeq',
                                        'ShiftR20.png'), 0) / 255.
    shift_r40 = cv2.imread(os.path.join(input_dir, 'TestSeq',
                                        'ShiftR40.png'), 0) / 255.

    # PL: may need to adjust my gaussian kernel in optic_flow_lk 
    # or maybe use filter2D instead, as others only used uniform
    # in 1a, and got better results with gaussian everywhere else.
    # PL: LK should not work well for at least some of these comparisons.
    """
    ps4-1-b-1.png 
    """
    # playWithQuiver(shift_0, shift_r10, title="Shift0.png to ShiftR10.png")
    k_size = 40  # TODO: Select a kernel size
    k_type = "uniform"  # TODO: Select a kernel type
    sigma = 0  # TODO: Select a sigma value if you are using a gaussian kernel
    u, v = ps4.optic_flow_lk(shift_0, shift_r10, k_size, k_type, sigma)
    u_v = quiver(u, v, scale=3, stride=10)
    cv2.imwrite(os.path.join(output_dir, "ps4-1-b-1.png"), u_v)

    """
    - ps4-1-b-2.png
    """
    # playWithQuiver(shift_0, shift_r20, title="Shift0.png to ShiftR10.png")
    k_size = 24  # TODO: Select a kernel size
    k_type = "uniform"  # TODO: Select a kernel type
    sigma = 0  # TODO: Select a sigma value if you are using a gaussian kernel
    u, v = ps4.optic_flow_lk(shift_0, shift_r20, k_size, k_type, sigma)
    u_v = quiver(u, v, scale=3, stride=10)
    cv2.imwrite(os.path.join(output_dir, "ps4-1-b-2.png"), u_v)

    """
    - ps4-1-b-3.png
    """
    # playWithQuiver(shift_0, shift_r40, title="Shift0.png to ShiftR10.png")
    k_size = 31  # TODO: Select a kernel size
    k_type = "uniform"  # TODO: Select a kernel type
    sigma = 0  # TODO: Select a sigma value if you are using a gaussian kernel
    u, v = ps4.optic_flow_lk(shift_0, shift_r40, k_size, k_type, sigma)
    u_v = quiver(u, v, scale=3, stride=10)
    cv2.imwrite(os.path.join(output_dir, "ps4-1-b-3.png"), u_v)



def part_2():

    yos_img_01 = cv2.imread(os.path.join(input_dir, 'DataSeq1',
                                         'yos_img_01.jpg'), 0) / 255.

    # 2a
    levels = 4
    yos_img_01_g_pyr = ps4.gaussian_pyramid(yos_img_01, levels)
    yos_img_01_g_pyr_img = ps4.create_combined_img(yos_img_01_g_pyr)
    yos_img_01_g_pyr_img = ps4.normalize_and_scale(yos_img_01_g_pyr_img,
                                                   scale_range=(0,255))
    cv2.imwrite(os.path.join(output_dir, "ps4-2-a-1.png"),
                yos_img_01_g_pyr_img)

    # 2b
    yos_img_01_l_pyr = ps4.laplacian_pyramid(yos_img_01_g_pyr)

    yos_img_01_l_pyr_img = ps4.create_combined_img(yos_img_01_l_pyr)
    # ps4.show_img("result", yos_img_01_l_pyr_img)
    # yos_img_01_l_pyr_img[yos_img_01_l_pyr_img < 0] = 0
    yos_img_01_l_pyr_img = ps4.normalize_and_scale(yos_img_01_l_pyr_img,
                                                   scale_range=(0,255))
    cv2.imwrite(os.path.join(output_dir, "ps4-2-b-1.png"),
                yos_img_01_l_pyr_img)


def part_3a_1():
    yos_img_01 = cv2.imread(
        os.path.join(input_dir, 'DataSeq1', 'yos_img_01.jpg'), 0) / 255.
    yos_img_02 = cv2.imread(
        os.path.join(input_dir, 'DataSeq1', 'yos_img_02.jpg'), 0) / 255.

    levels = 7  # Define the number of pyramid levels
    yos_img_01_g_pyr = ps4.gaussian_pyramid(yos_img_01, levels)
    yos_img_02_g_pyr = ps4.gaussian_pyramid(yos_img_02, levels)

    level_id = 0  # TODO: Select the level number (or id) you wish to use
    # playWithQuiver(yos_img_01_g_pyr[level_id],
                   # yos_img_02_g_pyr[level_id],
                   # "Part 3a-1")
    k_size = 143  # TODO: Select a kernel size
    k_type = "gaussian"  # TODO: Select a kernel type
    sigma = 252  # TODO: Select a sigma value if you are using a gaussian kernel
    u, v = ps4.optic_flow_lk(yos_img_01_g_pyr[level_id],
                             yos_img_02_g_pyr[level_id],
                             k_size, k_type, sigma)

    u, v = scale_u_and_v(u, v, level_id, yos_img_02_g_pyr)

    interpolation = cv2.INTER_CUBIC  # You may try different values
    border_mode = cv2.BORDER_REFLECT101  # You may try different values
    yos_img_02_warped = ps4.warp(yos_img_02, u, v, interpolation, border_mode)

    diff_yos_img_01_02 = yos_img_01 - yos_img_02_warped
    # cv2.imwrite(os.path.join(output_dir, "ps4-3-a-1.png"),
                # ps4.normalize_and_scale(diff_yos_img))
    cv2.imwrite(os.path.join(output_dir, "ps4-3-a-1.png"),
                ps4.normalize_and_scale(diff_yos_img_01_02))


def part_3a_2():
    yos_img_02 = cv2.imread(
        os.path.join(input_dir, 'DataSeq1', 'yos_img_02.jpg'), 0) / 255.
    yos_img_03 = cv2.imread(
        os.path.join(input_dir, 'DataSeq1', 'yos_img_03.jpg'), 0) / 255.

    levels = 7  # Define the number of pyramid levels
    yos_img_02_g_pyr = ps4.gaussian_pyramid(yos_img_02, levels)
    yos_img_03_g_pyr = ps4.gaussian_pyramid(yos_img_03, levels)

    level_id = 0  # TODO: Select the level number (or id) you wish to use
    k_size = 148  # TODO: Select a kernel size
    k_type = "gaussian"  # TODO: Select a kernel type
    sigma = 252  # TODO: Select a sigma value if you are using a gaussian kernel
    # playWithQuiver(yos_img_02_g_pyr[level_id],
                   # yos_img_03_g_pyr[level_id],
                   # "part_3a_2")
    u, v = ps4.optic_flow_lk(yos_img_02_g_pyr[level_id],
                             yos_img_03_g_pyr[level_id],
                             k_size, k_type, sigma)

    u, v = scale_u_and_v(u, v, level_id, yos_img_03_g_pyr)

    interpolation = cv2.INTER_CUBIC  # You may try different values
    border_mode = cv2.BORDER_REFLECT101  # You may try different values
    yos_img_03_warped = ps4.warp(yos_img_03, u, v, interpolation, border_mode)

    diff_yos_img = yos_img_02 - yos_img_03_warped
    cv2.imwrite(os.path.join(output_dir, "ps4-3-a-2.png"),
                ps4.normalize_and_scale(diff_yos_img))


def part_4a():
    shift_0 = cv2.imread(os.path.join(input_dir, 'TestSeq',
                                      'Shift0.png'), 0) / 255.
    shift_r10 = cv2.imread(os.path.join(input_dir, 'TestSeq',
                                        'ShiftR10.png'), 0) / 255.
    shift_r20 = cv2.imread(os.path.join(input_dir, 'TestSeq',
                                        'ShiftR20.png'), 0) / 255.
    shift_r40 = cv2.imread(os.path.join(input_dir, 'TestSeq',
                                        'ShiftR40.png'), 0) / 255.

    levels = 4  # TODO: Define the number of levels
    k_size = 20  # TODO: Select a kernel size
    k_type = "uniform"  # TODO: Select a kernel type
    sigma = 0  # TODO: Select a sigma value if you are using a gaussian kernel
    interpolation = cv2.INTER_CUBIC  # You may try different values
    border_mode = cv2.BORDER_REFLECT101  # You may try different values

    # playWithQuiverHLK(shift_0, shift_r10, "ps4-4-a-1.png")
    u10, v10 = ps4.hierarchical_lk(shift_0, shift_r10, levels, k_size, k_type,
                                   sigma, interpolation, border_mode)

    u_v = quiver(u10, v10, scale=3, stride=10)
    cv2.imwrite(os.path.join(output_dir, "ps4-4-a-1.png"), u_v)

    # You may want to try different parameters for the remaining function
    # calls.
    # playWithQuiverHLK(shift_0, shift_r20, "ps4-4-a-2.png")

    levels = 4  # TODO: Define the number of levels
    k_size = 25  # TODO: Select a kernel size
    k_type = "uniform"  # TODO: Select a kernel type
    sigma = 0  # TODO: Select a sigma value if you are using a gaussian kernel
    interpolation = cv2.INTER_CUBIC  # You may try different values
    border_mode = cv2.BORDER_REFLECT101  # You may try different values

    u20, v20 = ps4.hierarchical_lk(shift_0, shift_r20, levels, k_size, k_type,
                                   sigma, interpolation, border_mode)

    u_v = quiver(u20, v20, scale=3, stride=10)
    cv2.imwrite(os.path.join(output_dir, "ps4-4-a-2.png"), u_v)

    # playWithQuiverHLK(shift_0, shift_r40, "ps4-4-a-3.png")

    levels = 5  # TODO: Define the number of levels
    k_size = 26  # TODO: Select a kernel size
    k_type = "uniform"  # TODO: Select a kernel type
    sigma = 0  # TODO: Select a sigma value if you are using a gaussian kernel
    interpolation = cv2.INTER_CUBIC  # You may try different values
    border_mode = cv2.BORDER_REFLECT101  # You may try different values

    u40, v40 = ps4.hierarchical_lk(shift_0, shift_r40, levels, k_size, k_type,
                                   sigma, interpolation, border_mode)
    u_v = quiver(u40, v40, scale=3, stride=10)
    cv2.imwrite(os.path.join(output_dir, "ps4-4-a-3.png"), u_v)


def part_4b():
    urban_img_01 = cv2.imread(
        os.path.join(input_dir, 'Urban2', 'urban01.png'), 0) / 255.
    urban_img_02 = cv2.imread(
        os.path.join(input_dir, 'Urban2', 'urban02.png'), 0) / 255.

    levels = 3  # TODO: Define the number of levels
    k_size = 163  # TODO: Select a kernel size
    # k_size = 106  # TODO: Select a kernel size
    k_type = "uniform"  # TODO: Select a kernel type
    sigma = 0  # TODO: Select a sigma value if you are using a gaussian kernel
    interpolation = cv2.INTER_CUBIC  # You may try different values
    border_mode = cv2.BORDER_REFLECT101  # You may try different values

    # playWithQuiverHLK(urban_img_01, urban_img_02, "part 4b - 1")
    u, v = ps4.hierarchical_lk(urban_img_01, urban_img_02, levels, k_size,
                               k_type, sigma, interpolation, border_mode)

    u_v = quiver(u, v, scale=3, stride=10)
    cv2.imwrite(os.path.join(output_dir, "ps4-4-b-1.png"), u_v)

    interpolation = cv2.INTER_CUBIC  # You may try different values
    border_mode = cv2.BORDER_REFLECT101  # You may try different values
    urban_img_02_warped = ps4.warp(urban_img_02, u, v, interpolation,
                                   border_mode)

    diff_img = urban_img_01 - urban_img_02_warped
    cv2.imwrite(os.path.join(output_dir, "ps4-4-b-2.png"),
                ps4.normalize_and_scale(diff_img))


def part_5a():
    """Frame interpolation

    Follow the instructions in the problem set instructions.

    Place all your work in this file and this section.
    """
    shift_0 = cv2.imread(os.path.join(input_dir, 'TestSeq',
                                      'Shift0.png'), 0) / 255.
    shift_r10 = cv2.imread(os.path.join(input_dir, 'TestSeq',
                                        'ShiftR10.png'), 0) / 255.

    levels = 3  # TODO: Define the number of levels
    k_size = 28  # TODO: Select a kernel size
    k_type = "uniform"  # TODO: Select a kernel type
    sigma = 0  # TODO: Select a sigma value if you are using a gaussian kernel
    interpolation = cv2.INTER_CUBIC  # You may try different values
    border_mode = cv2.BORDER_REFLECT101  # You may try different values

    # playWithQuiverHLK(shift_0, shift_r10, "ps4-5-a-1.png")
    u, v = ps4.hierarchical_lk(shift_0, shift_r10, levels, k_size, k_type,
                                   sigma, interpolation, border_mode)

    # from shift_r10 to i08, we must go 2/10 of the distance of the 
    # full u, v transformation from shift_0 to shift_r10
    # u, v maps from shift_r10 back to shift_0
    i08 = ps4.warp(shift_r10, u*.2, v*.2, interpolation, border_mode) 
    # from shift_r10 to i06, we must go 4/10 of the distance of the 
    # full u, v transformation from shift_0 to shift_r10
    i06 = ps4.warp(shift_r10, u*.4, v*.4, interpolation, border_mode) 
    # etc...
    # from shift_0 to shift_r10, we must go the last .2 and .4 
    i04 = ps4.warp(shift_r10, u*.6, v*.6, interpolation, border_mode)
    i02 = ps4.warp(shift_r10, u*.8, v*.8, interpolation, border_mode)

    show_normalize_save(shift_0, i02, i04, i06, i08, shift_r10, output_dir, "ps4-5-a-1.png")

def show_normalize_save(shift_0, i02, i04, i06, i08, shift_r10, output_dir, output_file):

    # ps4.show_img("i0", shift_0)
    # ps4.show_img("i02", i02)
    # ps4.show_img("i04", i04)
    # ps4.show_img("i06", i06)
    # ps4.show_img("i08", i08)
    # ps4.show_img("i1", shift_r10)

    i08 = ps4.normalize_and_scale(i08)
    i06 = ps4.normalize_and_scale(i06)
    i04 = ps4.normalize_and_scale(i04)
    i02 = ps4.normalize_and_scale(i02)
    shift_0 = ps4.normalize_and_scale(shift_0)
    shift_r10 = ps4.normalize_and_scale(shift_r10)

    # cv2.imwrite(os.path.join(output_dir, "start.png"), shift_0)
    # cv2.imwrite(os.path.join(output_dir, "i02.png"), i02)
    # cv2.imwrite(os.path.join(output_dir, "i04.png"), i04)
    # cv2.imwrite(os.path.join(output_dir, "i06.png"), i06)
    # cv2.imwrite(os.path.join(output_dir, "i08.png"), i08)
    # cv2.imwrite(os.path.join(output_dir, "end.png"), shift_r10)

    H, W = shift_0.shape[0], shift_0.shape[1]
    result = np.zeros((H*2, W*3))
    # result[:,:] = -1 # not needed, since we're normalizing prior
    result[0:H, 0:W] = shift_0
    result[0:H, W:2*W] = i02
    result[0:H, 2*W:3*W] = i04
    result[H:2*H, 0:W] = i06
    result[H:2*H, W:2*W] = i08
    result[H:2*H, 2*W:3*W] = shift_r10

    # ps4.show_img("result", result)
    cv2.imwrite(os.path.join(output_dir, output_file),
                result)

def part_5b():
    """Frame interpolation

    Follow the instructions in the problem set instructions.

    Place all your work in this file and this section.
    """
    #######################################3
    # I0 - I1
    #######################################3


    img_start = "mc01.png"
    img_end = "mc02.png"
    img_dir = "MiniCooper"
    levels = 5  # 3, 4, 6
    k_size = 32  # 44, 59, 40
    k_type = "uniform"
    sigma = 0
    interpolation = cv2.INTER_CUBIC
    border_mode = cv2.BORDER_REFLECT101
    output_file = "ps4-5-b-1.png"

    shift_0 = cv2.imread(os.path.join(input_dir, img_dir,
                                      img_start), 0) / 255.
    shift_r10 = cv2.imread(os.path.join(input_dir, img_dir,
                                        img_end), 0) / 255.

    # playWithQuiverHLK(shift_0, shift_r10, output_file)
    u, v = ps4.hierarchical_lk(shift_0, shift_r10, levels, k_size, k_type,
                               sigma, interpolation, border_mode)

    # from shift_r10 to i08, we must go 2/10 of the distance of the
    # full u, v transformation from shift_0 to shift_r10
    # u, v maps from shift_r10 back to shift_0
    i08 = ps4.warp(shift_r10, u*.5, v*.5, interpolation, border_mode)
    # from shift_r10 to i06, we must go 4/10 of the distance of the
    # full u, v transformation from shift_0 to shift_r10
    i06 = ps4.warp(shift_r10, u, v, interpolation, border_mode)
    # etc...

                                # shift in opposite direction
    u, v = ps4.hierarchical_lk(shift_r10, shift_0, levels, k_size, k_type,
                               sigma, interpolation, border_mode)

    # from shift_0 to shift_r10, we must go the last .2 and .4 
    i04 = ps4.warp(shift_0, u, v, interpolation, border_mode)
    i02 = ps4.warp(shift_0, u*.5, v*.5, interpolation, border_mode)

    # TODO: post process with gaussianBlur or smooothing of some kind

    show_normalize_save(shift_0, i02, i04, i06, i08, shift_r10, output_dir, output_file)

    #######################################3
    # I1 - I2
    #######################################3

    img_start = "mc02.png"
    img_end = "mc03.png"
    img_dir = "MiniCooper"
    levels = 5  # 3, 4
    k_size = 32  # 36, 59
    k_type = "uniform"
    sigma = 0
    interpolation = cv2.INTER_CUBIC
    border_mode = cv2.BORDER_REFLECT101
    output_file = "ps4-5-b-2.png"

    shift_0 = cv2.imread(os.path.join(input_dir, img_dir,
                                      img_start), 0) / 255.
    shift_r10 = cv2.imread(os.path.join(input_dir, img_dir,
                                        img_end), 0) / 255.
    # playWithQuiverHLK(shift_0, shift_r10, output_file)

    u, v = ps4.hierarchical_lk(shift_0, shift_r10, levels, k_size, k_type,
                               sigma, interpolation, border_mode)

    i08 = ps4.warp(shift_r10, u*.5, v*.5, interpolation, border_mode)
    i06 = ps4.warp(shift_r10, u, v, interpolation, border_mode)

    # switch directions for the last two 
    u, v = ps4.hierarchical_lk(shift_r10, shift_0, levels, k_size, k_type,
                               sigma, interpolation, border_mode)
    i04 = ps4.warp(shift_0, u, v, interpolation, border_mode)
    i02 = ps4.warp(shift_0, u*.5, v*.5, interpolation, border_mode)

    show_normalize_save(shift_0, i02, i04, i06, i08, shift_r10, output_dir, output_file)


def part_6():
    """Challenge Problem

    Follow the instructions in the problem set instructions.

    Place all your work in this file and this section.
    """
    video_name= "ps4-my-video.mp4"
    VID_DIR = "input_videos"

    video = os.path.join(VID_DIR, video_name)
    image_gen = video_frame_generator(video)

    image1 = image_gen.next()
    h, w, d = image1.shape

    image2 = image_gen.next()

    # fps = 30 # 15?
    fps = 15
    out_path = "{0}/ps4-my-video-optic-flow.mp4".format(output_dir)
    video_out = mp4_video_writer(out_path, (w, h), fps=fps)

    frame_num = 1

    levels = 4  # 5, 3, 4
    k_size = 50  # 32, 44, 59
    k_type = "uniform"
    sigma = 0
    interpolation = cv2.INTER_CUBIC
    border_mode = cv2.BORDER_REFLECT101
    # cv2.IMREAD_COLOR # default
    # cv2.IMREAD_GRAYSCALE 
    # cv2.IMREAD_UNCHANGED: includes alpha channel

    while image2 is not None:
        temp_image = image1.copy()
        image1 = cv2.cvtColor(image1, cv2.COLOR_BGR2GRAY)
        image2 = cv2.cvtColor(image2, cv2.COLOR_BGR2GRAY)
        image1 = image1.astype("float32")
        image2 = image2.astype("float32")

        image1 /= 255.0
        image2 /= 255.0
        # ps4.show_img("image1", image1)
        # ps4.show_img("image2", image2)
        # if frame_num == 36:
            # playWithQuiverHLK(image1, image2, "challenge")

        u, v = ps4.hierarchical_lk(image1, image2, levels, k_size, k_type,
                                   sigma, interpolation, border_mode)

        # image1 = ps4.normalize_and_scale(image1)
        # image1 = image1.astype("float32")
        temp_image = temp_image.astype("float32")
        image_flow = quiver(u, v, scale=3, stride=10, color=(0, 255, 0), img_out=temp_image, img_out_gray=False)
        # image_flow = ps4.normalize_and_scale(image_flow)
        image_flow = image_flow.astype("uint8")
        # ps4.show_img("image_flow", image_flow)
        # if frame_num > 24 and frame_num < 40:
            # ps4.show_img("frame", image_flow)
        if frame_num == 34:
            cv2.imwrite(os.path.join(output_dir, "ps4-6-a-1.png"), image_flow)
        elif frame_num == 112:
            cv2.imwrite(os.path.join(output_dir, "ps4-6-a-2.png"), image_flow)

        print("Writing frame #{0}/113".format(frame_num))
        video_out.write(image_flow)
        image1 = image_gen.next()
        if image1 is None:
            break
        image2 = image_gen.next()
        frame_num += 1

    video_out.release()

##################
# HELPER FUNCTIONS
##################

def doNothing(x):
    pass
def playWithQuiver(img1, img2, title="Quiver"):

    window = title
    k_type_txt = "k_type"
    sigma_txt = "sigma"
    k_size_txt = "k_size"
    cv2.namedWindow(window, cv2.WINDOW_NORMAL)

    cv2.createTrackbar(k_size_txt, window, 1, 500, doNothing)
    cv2.createTrackbar(k_type_txt, window, 1, 1, doNothing)
    cv2.createTrackbar(sigma_txt, window, 1, 500, doNothing)

    while(1):
        img1 = img1.copy()
        img2 = img2.copy()
        k = cv2.waitKey(1) & 0xFF
        if k == 10:
            break
        k_size = cv2.getTrackbarPos(k_size_txt, window)
        k_type = cv2.getTrackbarPos(k_type_txt, window)
        sigma = cv2.getTrackbarPos(sigma_txt, window)

        if k_type == 0:
            k_type = "uniform"
        else:
            k_type = "gaussian"
        if k_size % 2 == 0:
            k_size += 1

        print("k_size = {0}".format(k_size))
        print("k_type = {0}".format(k_type))
        print("sigma = {0}".format(sigma))

        u, v = ps4.optic_flow_lk(img1,
                                 img2,
                                 k_size,
                                 k_type,
                                 sigma)

        u_v = quiver(u, v, scale=3, stride=10)

        cv2.imshow(window, u_v)

    cv2.destroyAllWindows()

def playWithQuiverHLK(img1, img2, title="Quiver"):

    window = title
    k_type_txt = "k_type"
    sigma_txt = "sigma"
    k_size_txt = "k_size"
    levels_txt = "levels"
    interpolation_txt = "interpolation"
    border_mode_txt = "border_mode"
    cv2.namedWindow(window, cv2.WINDOW_NORMAL)

    cv2.createTrackbar(k_size_txt, window, 1, 500, doNothing)
    cv2.createTrackbar(k_type_txt, window, 1, 1, doNothing)
    cv2.createTrackbar(sigma_txt, window, 1, 500, doNothing)
    cv2.createTrackbar(levels_txt, window, 3, 50, doNothing)
    cv2.createTrackbar(interpolation_txt, window, 0, 4, doNothing)
    cv2.createTrackbar(border_mode_txt, window, 0, 5, doNothing)

    while(1):
        img1 = img1.copy()
        img2 = img2.copy()
        k = cv2.waitKey(1) & 0xFF
        if k == 10:
            break
        k_size = cv2.getTrackbarPos(k_size_txt, window)
        k_type = cv2.getTrackbarPos(k_type_txt, window)
        sigma = cv2.getTrackbarPos(sigma_txt, window)
        levels = cv2.getTrackbarPos(levels_txt, window)
        interpolation = cv2.getTrackbarPos(interpolation_txt, window)
        border_mode = cv2.getTrackbarPos(border_mode_txt, window)

        if k_type == 0:
            k_type = "uniform"
        else:
            k_type = "gaussian"
        if k_size % 2 == 0:
            k_size += 1
        if levels == 0:
            levels = 1

        possible_borders = [cv2.BORDER_REFLECT_101,
                            cv2.BORDER_DEFAULT,
                            cv2.BORDER_CONSTANT,
                            cv2.BORDER_REFLECT,
                            cv2.BORDER_REPLICATE,
                            cv2.BORDER_WRAP]

        possible_interpolations = [cv2.INTER_CUBIC,
                                   cv2.INTER_NEAREST,
                                   cv2.INTER_LINEAR,
                                   cv2.INTER_AREA,
                                   cv2.INTER_LANCZOS4]

        border_mode = possible_borders[border_mode]
        interpolation = possible_interpolations[interpolation]

        print("k_size = {0}".format(k_size))
        print("k_type = {0}".format(k_type))
        print("sigma = {0}".format(sigma))
        print("levels = {0}".format(levels))
        print("interpolation = {0}".format(interpolation))
        print("border_mode = {0}".format(border_mode))

        u, v = ps4.hierarchical_lk(img1,
                                 img2,
                                 levels,
                                 k_size,
                                 k_type,
                                 sigma,
                                 interpolation,
                                 border_mode)

        img2 = img2.astype("float32")
        u_v = quiver(u, v, scale=3, stride=10, color=(0,255,0), img_out=img2)


        cv2.imshow(window, u_v)

    cv2.destroyAllWindows()

def video_frame_generator(filename):
    """A generator function that returns a frame on each 'next()' call.

    Will return 'None' when there are no frames left.

    Args:
        filename (string): Filename.

    Returns:
        None.
    """
    # Todo: Open file with VideoCapture and set result to 'video'. Replace None
    # https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_gui/py_video_display/py_video_display.html
    video = cv2.VideoCapture(filename)

    # Do not edit this while loop
    while video.isOpened():
        ret, frame = video.read()

        if ret:
            yield frame
        else:
            break

    # Todo: Close video (release) and yield a 'None' value. (add 2 lines)
    video.release()
    yield None

def mp4_video_writer(filename, frame_size, fps=20):
    """Opens and returns a video for writing.

    Use the VideoWriter's `write` method to save images.
    Remember to 'release' when finished.

    Args:
        filename (string): Filename for saved video
        frame_size (tuple): Width, height tuple of output video
        fps (int): Frames per second
    Returns:
        VideoWriter: Instance of VideoWriter ready for writing
    """
    # fourcc = cv2.cv.CV_FOURCC(*'MP4V')
    filename = filename.replace(".mp4", ".avi")
    fourcc = cv2.cv.CV_FOURCC(*'XVID')
    return cv2.VideoWriter(filename, fourcc, fps, frame_size)

if __name__ == "__main__":
    part_1a()
    part_1b()
    part_2()
    part_3a_1()
    part_3a_2()
    part_4a()
    part_4b()
    part_5a()
    part_5b()
    part_6()
