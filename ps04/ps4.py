"""Problem Set 4: Motion Detection"""

import numpy as np
import cv2
import os
import sys

"""
Examples of allowed functions:
cv2.normalize,
cv2.Sobel,
cv2.filter2D,
cv2.sepFilter2D,
cv2.remap,
cv2.getGaussianKernel,
cv2.boxFilter,
cv2.GaussianBlur,
np.linalg,
np.meshgrid

"""


# Utility function
def normalize_and_scale(image_in, scale_range=(0, 255)):
    """Normalizes and scales an image to a given range [0, 255].

    Utility function. There is no need to modify it.

    Args:
        image_in (numpy.array): input image.
        scale_range (tuple): range values (min, max). Default set to
                             [0, 255].

    Returns:
        numpy.array: output image.
    """
    image_out = np.zeros(image_in.shape)
    cv2.normalize(image_in, image_out, alpha=scale_range[0],
                  beta=scale_range[1], norm_type=cv2.NORM_MINMAX)

    return image_out


# Assignment code
def gradient_x(image):
    """Computes image gradient in X direction.

    Use cv2.Sobel to help you with this function. Additionally you
    should set cv2.Sobel's 'scale' parameter to one eighth and ksize
    to 3.

    Args:
        image (numpy.array): grayscale floating-point image with
                             values in [0.0, 1.0].

    Returns:
        numpy.array: image gradient in the X direction. Output
                     from cv2.Sobel.
    """

    # Information on cv2.Sobel
    # https://docs.opencv.org/3.0-beta/modules/imgproc/doc/filtering.html
    image = image.copy()
    gradX = cv2.Sobel(image,
                   ddepth=-1, # dst will have same depth as src
                   dx=1, # order of the derivative x
                   dy=0, # order of the derivative y
                   scale=0.125,
                   ksize=3) # size of the extended Sobel kernel,
                            # it must be 1,3,5,or 7
    return gradX


def gradient_y(image):
    """Computes image gradient in Y direction.

    Use cv2.Sobel to help you with this function. Additionally you
    should set cv2.Sobel's 'scale' parameter to one eighth and ksize
    to 3.

    Args:
        image (numpy.array): grayscale floating-point image with
                             values in [0.0, 1.0].

    Returns:
        numpy.array: image gradient in the Y direction.
                     Output from cv2.Sobel.
    """
    # Information on cv2.Sobel
    # https://docs.opencv.org/3.0-beta/modules/imgproc/doc/filtering.html
    image = image.copy()
    gradY = cv2.Sobel(src=image,
                   ddepth=-1, # dst will have same depth as src
                   dx=0, # order of the derivative x
                   dy=1, # order of the derivative y
                   scale=0.125,
                   ksize=3) # size of the extended Sobel kernel,
                            # it must be 1,3,5,or 7

    return gradY

def optic_flow_lk(img_a, img_b, k_size, k_type, sigma=1):
    """Computes optic flow using the Lucas-Kanade method.

    For efficiency, you should apply a convolution-based method.

    Note: Implement this method using the instructions in the lectures
    and the documentation.

    You are not allowed to use any OpenCV functions that are related
    to Optic Flow.

    Args:
        img_a (numpy.array): grayscale floating-point image with
                             values in [0.0, 1.0].
        img_b (numpy.array): grayscale floating-point image with
                             values in [0.0, 1.0].
        k_size (int): size of averaging kernel to use for weighted
                      averages. Here we assume the kernel window is a
                      square so you will use the same value for both
                      width and height.
        k_type (str): type of kernel to use for weighted averaging,
                      'uniform' or 'gaussian'. By uniform we mean a
                      kernel with the only ones divided by k_size**2.
                      To implement a Gaussian kernel use
                      cv2.getGaussianKernel. The autograder will use
                      'uniform'.
        sigma (float): sigma value if gaussian is chosen. Default
                       value set to 1 because the autograder does not
                       use this parameter.

    Returns:
        tuple: 2-element tuple containing:
            U (numpy.array): raw displacement (in pixels) along
                             X-axis, same size as the input images,
                             floating-point type.
            V (numpy.array): raw displacement (in pixels) along
                             Y-axis, same size and type as U.
    """
    img_a = img_a.copy()
    img_b = img_b.copy()

    # This should be iterative lucas and kanade

    # A kernel is essentially a fixed size array of numerical coefficeints 
    # along with an anchor point in that array, which is tipically located 
    # at the center.
    kernel = None
    if k_type == "uniform":
        kernel = np.ones((k_size, k_size))
    elif k_type == "gaussian":
        kernel = cv2.getGaussianKernel(ksize=k_size,
                              sigma=sigma)

    # initialize u and v to zero, same size as the input image
    u = np.zeros(img_a.shape)
    v = np.zeros(img_a.shape)

    # find gradients
    Ix = gradient_x(img_a)
    Iy = gradient_y(img_a)
    It = img_b - img_a

    IxIx = Ix*Ix
    IxIy = Ix*Iy
    IyIy = Iy*Iy
    IxIt = Ix*It
    IyIt = Iy*It

    # convolve images with kernel for smoothing
    SIGMA_IxIx = convolve(IxIx, kernel)
    SIGMA_IxIy = convolve(IxIy, kernel)
    SIGMA_IyIy = convolve(IyIy, kernel)
    SIGMA_IxIt = convolve(IxIt, kernel)
    SIGMA_IyIt = convolve(IyIt, kernel)

    # A * d = B
    #          A                * d   =     B
    # [ SIGMA(Ix^2) SIGMA(IxIy)][ u ]     [ SIGMA(IxIt) ]
    # [ SIGMA(IxIy) SIGMA(Iy^2)][ v ] = - [ SIGMA(IyIt) ] 
    #                             d   =    (A)^-1 * B 

    # get a,b,c,d for A
    a = SIGMA_IxIx
    b = SIGMA_IxIy
    c = SIGMA_IxIy
    d = SIGMA_IyIy

    # get inverse
    A_inv = (1 / (a*d - b*c))*np.array([[d, -b],
                                        [-c, a]])


    B = np.array([[-1*SIGMA_IxIt],
                  [-1*SIGMA_IyIt]])

    # get a,b,c,d for A_inv
    a = A_inv[0,0,:,:]
    b = A_inv[0,1,:,:]
    c = A_inv[1,0,:,:]
    d = A_inv[1,1,:,:]

    # get e, f
    e = B[0,0,:,:]
    f = B[1,0,:,:]

    # [ u ]    [ a  b ] [ e ]
    # [ v ] =  [ c  d ] [ f ] 

    
    u = a*e + b*f
    v = c*e + d*f

    # ensure points with np.nan are all 0
    u[np.isnan(u)] = 0
    v[np.isnan(v)] = 0

    # prevent overflow errors in quiver plot
    # u and v should be within appropriate bounds
    u[u > 99999] = 0
    u[u < -99999] = 0
    v[v > 99999] = 0
    v[v < -99999] = 0

    return u,v

def reduce_image(image):
    """Reduces an image to half its shape.

    The autograder will pass images with even width and height. It is
    up to you to determine values with odd dimensions. For example the
    output image can be the result of rounding up the division by 2:
    (13, 19) -> (7, 10)

    For simplicity and efficiency, implement a convolution-based
    method using the 5-tap separable filter.

    Follow the process shown in the lecture 6B-L3. Also refer to:
    -  Burt, P. J., and Adelson, E. H. (1983). The Laplacian Pyramid
       as a Compact Image Code
    You can find the link in the problem set instructions.

    Args:
        image (numpy.array): grayscale floating-point image, values in
                             [0.0, 1.0].

    Returns:
        numpy.array: output image with half the shape, same type as the
                     input image.
    """
    image = image.copy()
    H, W = image.shape[0], image.shape[1]

    # construct the base kernel, cv2.sepFilter2D will handle the 
    # creation of the full 5x5 kernel
    base_kernel = np.array([1,4,6,4,1])*(1/16.0)


    # apply 5 tap separable filter first
    reduced = cv2.sepFilter2D(src=image,
                              ddepth=-1,
                              kernelX=base_kernel,
                              kernelY=base_kernel)

    # then downsample, taking only even rows and columns
    reduced = reduced[::2, ::2]
    # show_img("result", reduced)

    return reduced

def gaussian_pyramid(image, levels):
    """Creates a Gaussian pyramid of a given image.

    This method uses reduce_image() at each level. Each image is
    stored in a list of length equal the number of levels.

    The first element in the list ([0]) should contain the input
    image. All other levels contain a reduced version of the previous
    level.

    All images in the pyramid should floating-point with values in

    Args:
        image (numpy.array): grayscale floating-point image, values
                             in [0.0, 1.0].
        levels (int): number of levels in the resulting pyramid.

    Returns:
        list: Gaussian pyramid, list of numpy.arrays.
    """
    image = image.copy()

    pyramids = [image]

    for i in range(1, levels):
        image = reduce_image(image)
        pyramids.append(image)

    return pyramids


def create_combined_img(img_list):
    """Stacks images from the input pyramid list side-by-side.

    Ordering should be large to small from left to right.

    See the problem set instructions for a reference on how the output
    should look like.

    Make sure you call normalize_and_scale() for each image in the
    pyramid when populating img_out.

    Args:
        img_list (list): list with pyramid images.

    Returns:
        numpy.array: output image with the pyramid images stacked
                     from left to right.
    """
    tempList = []
    for img in img_list:
        H, W = img.shape[0], img.shape[1]
        tempList.append([img, H, W])

    # sort by height
    tempList = sorted(tempList, reverse=True, key=lambda theList:theList[1])
    # sort by width
    tempList = sorted(tempList, reverse=True, key=lambda theList:theList[2])
    total_H = tempList[0][1]
    total_W = 0
    for each in tempList:
        print("{0}x{1}".format(each[1], each[2]))
        total_W += each[2]

    # sample result: 
    # 252x316
    # 126x158
    # 63x79
    # 32x40

    result = np.zeros((total_H, total_W))
    result[:,:] = -1
    print("result.shape = {0}".format(result.shape))
    currX, currY = 0, 0
    for each in tempList:
        imgH = each[1]
        imgW = each[2]
        print("imgH: {0}".format(imgH))
        print("imgW: {0}".format(imgW))
        print("currX: {0}".format(currX))
        print("currY: {0}".format(currY))
        
        print("each[0].shape = {0}".format(each[0].shape))
        print("result[{0}:{1}, {2}:{3}]".format(currX,imgH,currY,imgW))
        result[currX:currX+imgH, currY:currY+imgW] = normalize_and_scale(each[0],
                                                                        scale_range=(0,255))


        # show_img("result", result)
        currY += imgW

    return result


def expand_image(image):
    """Expands an image doubling its width and height.

    For simplicity and efficiency, implement a convolution-based
    method using the 5-tap separable filter.

    Follow the process shown in the lecture 6B-L3. Also refer to:
    -  Burt, P. J., and Adelson, E. H. (1983). The Laplacian Pyramid
       as a Compact Image Code

    You can find the link in the problem set instructions.

    Args:
        image (numpy.array): grayscale floating-point image, values
                             in [0.0, 1.0].

    Returns:
        numpy.array: same type as 'image' with the doubled height and
                     width.
    """
    image = image.copy()
    # show_img("image", image)
    H, W = image.shape[0], image.shape[1]

    # upsample
    expanded = np.zeros((H*2, W*2))
    expanded[::2, ::2] = image
    # expanded[1::2, ::2] = image
    # expanded[::2, 1::2] = image
    # expanded[1::2, 1::2] = image

    # show_img("upsampled", expanded)
    base_kernel = np.array([1,4,6,4,1])*(1/8.0)
    # kernel = np.zeros((5,5))
    # kernel = np.zeros((3,3))
    # kernel[:, 1] = base_kernel.T
    # kernel[:, 2] = base_kernel.T
    # kernel[:, 3] = base_kernel.T
    # kernel[1, :] = base_kernel
    # kernel[2, :] = base_kernel
    # kernel[3, :] = base_kernel
    # raw_input(kernel)

    # result = convolve(expanded, kernel)
    # return result

    # base_kernel_x = np.array([1,6,1])*(1/8.0)
    # base_kernel_y = np.array([4,4])*(1/8.0)
    # apply 3 tap separable filter first


    result = cv2.sepFilter2D(src=expanded,
                              ddepth=-1,
                              kernelX=base_kernel,
                              kernelY=base_kernel)

    # show_img("final", result)
    return result

def laplacian_pyramid(g_pyr):
    """Creates a Laplacian pyramid from a given Gaussian pyramid.

    This method uses expand_image() at each level.

    Args:
        g_pyr (list): Gaussian pyramid, returned by gaussian_pyramid().

    Returns:
        list: Laplacian pyramid, with l_pyr[-1] = g_pyr[-1].
    """

    result = []
    for i in range(0, len(g_pyr)):
        if i == len(g_pyr) - 1:
            image = g_pyr[i]
        else:
            expanded = expand_image(g_pyr[i+1])
            reduced = g_pyr[i]
            h_diff = expanded.shape[0] - reduced.shape[0]
            w_diff = expanded.shape[0] - reduced.shape[0]
            if h_diff != 0 and w_diff != 0:
                if h_diff == 1 and w_diff == 1:
                    expanded = expanded[:-1,:-1]
                if h_diff == -1 and w_diff == -1:
                    reduced = reduced[:-1,:-1]
            image = reduced - expanded

        result.append(image)

    return result


def warp(image, U, V, interpolation, border_mode):
    """Warps image using X and Y displacements (U and V).

    This function uses cv2.remap. The autograder will use cubic
    interpolation and the BORDER_REFLECT101 border mode. You may
    change this to work with the problem set images.

    See the cv2.remap documentation to read more about border and
    interpolation methods.

    Args:
        image (numpy.array): grayscale floating-point image, values
                             in [0.0, 1.0].
        U (numpy.array): displacement (in pixels) along X-axis.
        V (numpy.array): displacement (in pixels) along Y-axis.
        interpolation (Inter): interpolation method used in cv2.remap.
        border_mode (BorderType): pixel extrapolation method used in
                                  cv2.remap.

    Returns:
        numpy.array: warped image, such that
                     warped[y, x] = image[y + V[y, x], x + U[y, x]]
    """

    image = image.copy()
    M, N = image.shape[0], image.shape[1]
    U = U.copy()
    V = V.copy()

    # get indices of M x N
    # Y ,X  = np.indices, not X, Y = np.indices...
    mapy, mapx = np.indices((M, N))

    mapx = mapx.astype("float32")
    mapy = mapy.astype("float32")

    # add the displacements along X axis
    mapx += U
    # add the displacements along Y axis
    mapy += V

    res = cv2.remap(src=image,
                    map1=mapx,
                    map2=mapy,
                    interpolation=interpolation,
                    borderMode=border_mode)

    return res


def hierarchical_lk(img_a, img_b, levels, k_size, k_type, sigma, interpolation,
                    border_mode):
    """Computes the optic flow using Hierarchical Lucas-Kanade.

    This method should use reduce_image(), expand_image(), warp(),
    and optic_flow_lk().

    Args:
        img_a (numpy.array): grayscale floating-point image, values in
                             [0.0, 1.0].
        img_b (numpy.array): grayscale floating-point image, values in
                             [0.0, 1.0].
        levels (int): Number of levels.
        k_size (int): parameter to be passed to optic_flow_lk.
        k_type (str): parameter to be passed to optic_flow_lk.
        sigma (float): parameter to be passed to optic_flow_lk.
        interpolation (Inter): parameter to be passed to warp.
        border_mode (BorderType): parameter to be passed to warp.

    Returns:
        tuple: 2-element tuple containing:
            U (numpy.array): raw displacement (in pixels) along X-axis,
                             same size as the input images,
                             floating-point type.
            V (numpy.array): raw displacement (in pixels) along Y-axis,
                             same size and type as U.
    """

    img_a = img_a.copy()
    img_b = img_b.copy()

    # get gaussian pyraminds for both images
    g_pyr_a = gaussian_pyramid(img_a, levels)
    g_pyr_b = gaussian_pyramid(img_b, levels)

    top = -1

    img_a_top = g_pyr_a[top]
    img_b_top = g_pyr_b[top]

    u, v = optic_flow_lk(img_a_top,
                         img_b_top,
                         k_size,
                         k_type,
                         sigma)

    for i in range(levels-2, -1, -1):  # e.g. if levels=5, then: 4,3,2,1,0
        # expand flow
        u = expand_image(u)
        v = expand_image(v)

        # multiply u_prime, v_prime by 2 to get predicted flow
        u *= 2
        v *= 2


        # warp level i gaussian version of img a according to the
        # predicted flow u_prime, v_prime to create img_a_prime
        g_pyr_b[i] = warp(g_pyr_b[i], u, v, interpolation, border_mode)

        # apply LK between img_a_prime and level i gaussian version of img_b
        # to get u_delta, v_delta (the correct in flow)
        u_delta, v_delta = optic_flow_lk(g_pyr_a[i],
                                         g_pyr_b[i],
                                         k_size,
                                         k_type,
                                         sigma)

        # flow = expanded & multiplied by 2 flow + flow from iterative lk
        u += u_delta
        v += v_delta

    return u, v


######################
# HELPER FUNCTIONS
######################

def convolve(img, kernel):
    img_tmp = img.copy()
    kernel_tmp = kernel.copy()

    img_convolved = cv2.filter2D(src=img_tmp,
                                 ddepth=-1, # output image depth. use -1 to use src.depth()
                                 kernel=kernel_tmp)
    return img_convolved

def show_img(img_name, img):
    if True:
        cv2.imshow(img_name, img)
        cv2.waitKey(0)
        cv2.destroyAllWindows()


def meanSquareError(img1, img2):

    img1 = img1.copy()
    img2 = img2.copy()
    H = float(img1.shape[0])
    W = float(img2.shape[1])

    error = np.sum((img1 - img2)**2)
    error = error / (H*W)

    return error
