Timer unit: 1e-06 s

Total time: 0 s
File: ps3.py
Function: find_markers at line 86

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
    86                                           @profile
    87                                           def find_markers(image, template=None):
    88                                               """Finds four corner markers.
    89                                           
    90                                               Use a combination of circle finding, corner detection and convolution to
    91                                               find the four markers in the image.
    92                                           
    93                                               Args:
    94                                                   image (numpy.array): image array of uint8 values.
    95                                                   template (numpy.array): template image of the markers.
    96                                           
    97                                               Returns:
    98                                                   list: List of four (x, y) tuples
    99                                                       in the order [top-left, bottom-left, top-right, bottom-right].
   100                                               """
   101                                               # TODO: determine if this is the slow method, or one of the slow methods
   102                                               # if it is, try using matchtemplate instead (see match template branch itself)
   103                                               # by rotating the template until the highest match is found
   104                                           
   105                                               img_tmp = image.copy()
   106                                               template_tmp = template.copy()
   107                                               template_tmp_90 = template.copy()
   108                                               template_tmp_90 = np.rot90(template_tmp_90)
   109                                           
   110                                               img_convolved = convolveImg(img_tmp, template_tmp)
   111                                               img_convolved_90 = convolveImg(img_tmp, template_tmp_90)
   112                                           
   113                                               coords, maxVal = getCoordsFromConvolution(img_convolved, template_tmp)
   114                                               coords_90, maxVal_90 = getCoordsFromConvolution(img_convolved_90, template_tmp)
   115                                           
   116                                               if maxVal_90 > maxVal:
   117                                                   coords = coords_90
   118                                           
   119                                               # raw_input("RESULT: {0}".format(coords))
   120                                               coords = np.array(coords)
   121                                               result = []
   122                                               for y, x in coords:
   123                                                   result.append([y, x])
   124                                                   drawCrossHairsAtPoint(img_tmp, x, y, 5)
   125                                           
   126                                               result = np.array(result)
   127                                           
   128                                               # swap x and y
   129                                               result[:,0], result[:,1] = result[:,1], result[:,0].copy()
   130                                               # truncate to int
   131                                               # result = result.astype(int)
   132                                           
   133                                               # sort by first column
   134                                               result = result[np.argsort(result[:,0])]
   135                                               # sort by second column
   136                                               # result = result[np.argsort(result[:,1])]
   137                                           
   138                                               top_left = tuple(result[0]) if result[0,1] < result[1,1] else result[1]
   139                                               bottom_left = tuple(result[1]) if result[0,1] < result[1,1] else result[0]
   140                                               top_right = tuple(result[2]) if result[2,1] < result[3,1] else result[3]
   141                                               bottom_right = tuple(result[3]) if result[2,1] < result[3,1] else result[2]
   142                                           
   143                                               temp = [top_left, bottom_left, top_right, bottom_right]
   144                                           
   145                                           
   146                                               # raw_input(result)
   147                                               # temp = result.tolist()
   148                                               result = []
   149                                               for each in temp:
   150                                                   result.append(tuple(each))
   151                                           
   152                                               result = tuple(result)
   153                                               print(result)
   154                                               # show_img("result", img_tmp)
   155                                               return result

Total time: 0 s
File: ps3.py
Function: project_imageA_onto_imageB at line 208

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
   208                                           @profile
   209                                           def project_imageA_onto_imageB(imageA, imageB, homography):
   210                                               """Projects image A into the marked area in imageB.
   211                                           
   212                                               Using the four markers in imageB, project imageA into the marked area.
   213                                           
   214                                               Use your find_markers method to find the corners.
   215                                           
   216                                               Args:
   217                                                   imageA (numpy.array): image array of uint8 values.
   218                                                   imageB (numpy.array: image array of uint8 values.
   219                                                   homography (numpy.array): Transformation matrix, 3 x 3.
   220                                           
   221                                               Returns:
   222                                                   numpy.array: combined image
   223                                               """
   224                                               # imageA_temp = imageA.copy()
   225                                               # imageB_temp = imageB.copy()
   226                                               # show_img("image A", imageA_temp)
   227                                               # show_img("image B", imageB_temp)
   228                                               # https://docs.opencv.org/2.4/modules/imgproc/doc/geometric_transformations.html?highlight=remap#remap
   229                                               imageToProject = imageA.copy()
   230                                               white_imageToProject = imageA.copy()
   231                                               white_imageToProject[:,:,:] = 255
   232                                               result = imageB.copy()
   233                                               
   234                                               dst_height = result.shape[0]
   235                                               dst_width = result.shape[1]
   236                                           
   237                                               # need to linearize indices of destination image
   238                                               in_x, in_y = np.indices((dst_height, dst_width))
   239                                               in_x = in_x.astype('float32')
   240                                               in_y = in_y.astype('float32')
   241                                               linear_ind = np.array([in_y.ravel(),
   242                                                                      in_x.ravel(),
   243                                                                      np.ones_like(in_x).ravel()])
   244                                           
   245                                               # apply the homography matrix to every coordinate
   246                                               # in src to those in dst to get a mapping of the coordinates
   247                                               # onto which we will need to project image A
   248                                               # [ u ]     [ x ]
   249                                               # | v | = H | y |
   250                                               # [ 1 ]     [ 1 ] 
   251                                           
   252                                               temp = np.linalg.inv(homography)
   253                                               uv_ind = temp.dot(linear_ind)
   254                                           
   255                                               # convert from image coordinates to homogeneous coordinates
   256                                               # raw_input(uv_ind.shape)
   257                                               # raw_input(uv_ind)
   258                                               map1, map2 = uv_ind[:-1] / uv_ind[-1]
   259                                               # raw_input(map1)
   260                                               # raw_input(map2)
   261                                           
   262                                               map1 = map1.reshape(dst_height, dst_width)
   263                                               map2 = map2.reshape(dst_height, dst_width)
   264                                               map1 = map1.astype("float32")
   265                                               map2 = map2.astype("float32")
   266                                           
   267                                               # interpolate pixel values from the source image to
   268                                               # those at all coordinates in the dst image space
   269                                               res_image = cv2.remap(src=imageToProject,
   270                                                                     map1=map1,
   271                                                                     map2=map2,
   272                                                                     interpolation=cv2.INTER_LINEAR)
   273                                           
   274                                               white_res_image = cv2.remap(src=white_imageToProject,
   275                                                                     map1=map1,
   276                                                                     map2=map2,
   277                                                                     interpolation=cv2.INTER_LINEAR)
   278                                           
   279                                               result[white_res_image == 255] = 0
   280                                               # show_img("result", result)
   281                                               result += res_image
   282                                           
   283                                               # show_img("final", result)
   284                                           
   285                                               return result

