"""
CS6476 Problem Set 3 imports. Only Numpy and cv2 are allowed.
"""
import cv2
import numpy as np
from scipy import ndimage, signal


"""
Allowed functions:

cv2.cornerHarris,
cv2.matchTemplate,
cv2.SIFT(),
numpy.rot90,
cv2.pyrDown(),
cv2.remap,
SimpleBlobDetector,
all linalg functions,
cv2.fillPoly,
cv2.cornerEigenValsAndVecs...

"""

"""
Banned Functions:

cv2.findHomography,
cv2.getPerspectiveTransform,
cv2.findFundamentalMat,
cv2.warpPerspective,
cv2.goodFeaturesToTrack,
cv2.warpAffine()

"""

def euclidean_distance(p0, p1):
    """Gets the distance between two (x,y) points

    Args:
        p0 (tuple): Point 1.
        p1 (tuple): Point 2.

    Return:
        float: The distance between points
    """

    raise NotImplementedError


def get_corners_list(image):
    """Returns a ist of image corner coordinates used in warping.

    These coordinates represent four corner points that will be projected to
    a target image.

    Args:
        image (numpy.array): image array of float64.

    Returns:
        list: List of four (x, y) tuples
            in the order [top-left, bottom-left, top-right, bottom-right].
    """
    img_temp = image.copy()
    height = img_temp.shape[0] -1
    width = img_temp.shape[1] - 1

    top_left = tuple([0,0])
    bottom_left = tuple([0, height])
    top_right = tuple([width, 0])
    bottom_right = tuple([width, height])

    temp = [top_left, bottom_left, top_right, bottom_right]
    result = []
    for each in temp:
        result.append(tuple(each))

    return result

def show_img(img_name, img):
    if True:
        cv2.imshow(img_name, img)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

def find_markers(image, template=None):
    """Finds four corner markers.

    Use a combination of circle finding, corner detection and convolution to
    find the four markers in the image.

    Args:
        image (numpy.array): image array of uint8 values.
        template (numpy.array): template image of the markers.

    Returns:
        list: List of four (x, y) tuples
            in the order [top-left, bottom-left, top-right, bottom-right].
    """
    # TODO: determine if this is the slow method, or one of the slow methods
    # if it is, try using matchtemplate instead (see match template branch itself)
    # by rotating the template until the highest match is found

    img_tmp = image.copy()
    template_tmp = template.copy()

    angles = [0, 90]
    output = {}
    for angle in angles:
        template_rotated = ndimage.rotate(template_tmp, angle)
        img_convolved = convolveImg(img_tmp, template_rotated)
        coords, maxVal = getCoordsFromConvolution(img_convolved, template_rotated)
        output["{0}".format(angle)] = {"coords": coords, "maxVal": maxVal, "angle":angle}
        # print("maxVal: {0}, angle: {1}".format(maxVal, angle))

    maxVal = {"max": 0, "coords":[], "angle":None}
    for key, value in output.iteritems():
        if value["maxVal"] > maxVal["max"]:
            maxVal["coords"] = value["coords"]
            maxVal["max"] = value["maxVal"]
            maxVal["angle"] = value["angle"]

    # print("picked maxVal: {0}, angle: {1}".format(maxVal["max"], maxVal["angle"]))

    coords = maxVal["coords"]

    # raw_input("RESULT: {0}".format(coords))
    coords = np.array(coords)
    result = []
    for y, x in coords:
        result.append([y, x])
        drawCrossHairsAtPoint(img_tmp, x, y, 5)

    result = np.array(result)

    # swap x and y
    result[:,0], result[:,1] = result[:,1], result[:,0].copy()
    # truncate to int
    # result = result.astype(int)

    # sort by first column
    result = result[np.argsort(result[:,0])]
    # sort by second column
    # result = result[np.argsort(result[:,1])]

    top_left = tuple(result[0]) if result[0,1] < result[1,1] else result[1]
    bottom_left = tuple(result[1]) if result[0,1] < result[1,1] else result[0]
    top_right = tuple(result[2]) if result[2,1] < result[3,1] else result[3]
    bottom_right = tuple(result[3]) if result[2,1] < result[3,1] else result[2]

    temp = [top_left, bottom_left, top_right, bottom_right]


    # raw_input(result)
    # temp = result.tolist()
    result = []
    for each in temp:
        result.append(tuple(each))

    result = tuple(result)
    # print(result)
    # show_img("result", img_tmp)
    return result


def draw_box(image, markers, thickness=1):
    """Draws lines connecting box markers.

    Use your find_markers method to find the corners.
    Use cv2.line, leave the default "lineType" and Pass the thickness
    parameter from this function.

    Args:
        image (numpy.array): image array of uint8 values.
        markers(list): the points where the markers were located.
        thickness(int): thickness of line used to draw the boxes edges.

    Returns:
        numpy.array: image with lines drawn.
    """
    img_temp = image.copy()
    markers_tmp = np.array(markers)

    # sort the markers by x value, then determine which corner is which
    markers_tmp = markers_tmp[np.argsort(markers_tmp[:,0])]

    top_left = tuple(markers_tmp[0]) if markers_tmp[0,1] < markers_tmp[1,1] else markers_tmp[1]
    bottom_left = tuple(markers_tmp[1]) if markers_tmp[0,1] < markers_tmp[1,1] else markers_tmp[0]
    top_right = tuple(markers_tmp[2]) if markers_tmp[2,1] < markers_tmp[3,1] else markers_tmp[3]
    bottom_right = tuple(markers_tmp[3]) if markers_tmp[2,1] < markers_tmp[3,1] else markers_tmp[2]

    cv2.line(img_temp,
             (top_left[0], top_left[1]), # first point
             (bottom_left[0], bottom_left[1]), # secondpoint
             (0,255,0), # color
             thickness) # line thickness
    cv2.line(img_temp,
             (top_left[0], top_left[1]), # first point
             (top_right[0], top_right[1]), # secondpoint
             (0,255,0), # color
             thickness) # line thickness
    cv2.line(img_temp,
             (bottom_right[0], bottom_right[1]), # secondpoint
             (top_right[0], top_right[1]), # first point
             (0,255,0), # color
             thickness) # line thickness
    cv2.line(img_temp,
             (bottom_right[0], bottom_right[1]), # first point
             (bottom_left[0], bottom_left[1]), # secondpoint
             (0,255,0), # color
             thickness) # line thickness
    # show_img("draw box output", img_temp)
    return img_temp


def project_imageA_onto_imageB(imageA, imageB, homography):
    """Projects image A into the marked area in imageB.

    Using the four markers in imageB, project imageA into the marked area.

    Use your find_markers method to find the corners.

    Args:
        imageA (numpy.array): image array of uint8 values.
        imageB (numpy.array: image array of uint8 values.
        homography (numpy.array): Transformation matrix, 3 x 3.

    Returns:
        numpy.array: combined image
    """
    # imageA_temp = imageA.copy()
    # imageB_temp = imageB.copy()
    # show_img("image A", imageA_temp)
    # show_img("image B", imageB_temp)
    # https://docs.opencv.org/2.4/modules/imgproc/doc/geometric_transformations.html?highlight=remap#remap
    imageToProject = imageA.copy()
    white_imageToProject = imageA.copy()
    white_imageToProject[:,:,:] = 255
    result = imageB.copy()
    
    dst_height = result.shape[0]
    dst_width = result.shape[1]

    # need to linearize indices of destination image
    in_x, in_y = np.indices((dst_height, dst_width))
    in_x = in_x.astype('float32')
    in_y = in_y.astype('float32')
    linear_ind = np.array([in_y.ravel(),
                           in_x.ravel(),
                           np.ones_like(in_x).ravel()])

    # apply the homography matrix to every coordinate
    # in src to those in dst to get a mapping of the coordinates
    # onto which we will need to project image A
    # [ u ]     [ x ]
    # | v | = H | y |
    # [ 1 ]     [ 1 ] 

    temp = np.linalg.inv(homography)
    uv_ind = temp.dot(linear_ind)

    # convert from image coordinates to homogeneous coordinates
    # raw_input(uv_ind.shape)
    # raw_input(uv_ind)
    map1, map2 = uv_ind[:-1] / uv_ind[-1]
    # raw_input(map1)
    # raw_input(map2)

    map1 = map1.reshape(dst_height, dst_width)
    map2 = map2.reshape(dst_height, dst_width)
    map1 = map1.astype("float32")
    map2 = map2.astype("float32")

    # interpolate pixel values from the source image to
    # those at all coordinates in the dst image space
    res_image = cv2.remap(src=imageToProject,
                          map1=map1,
                          map2=map2,
                          interpolation=cv2.INTER_LINEAR)

    white_res_image = cv2.remap(src=white_imageToProject,
                          map1=map1,
                          map2=map2,
                          interpolation=cv2.INTER_LINEAR)

    result[white_res_image == 255] = 0
    # show_img("result", result)
    result += res_image

    # show_img("final", result)

    return result


def find_four_point_transform(src_points, dst_points):
    """Solves for and returns a perspective transform.

    Each source and corresponding destination point must be at the
    same index in the lists.

    Do not use the following functions (you will implement this yourself):
        cv2.findHomography
        cv2.getPerspectiveTransform

    Hint: You will probably need to use least squares to solve this.

    Args:
        src_points (list): List of four (x,y) source points.
        dst_points (list): List of four (x,y) destination points.

    Returns:
        numpy.array: 3 by 3 homography matrix of floating point values.
    """

    # we are solving for the matrix, H
    # p' = Hp
    # [ wx']   [ a  b  c ][ x ]
    # | wy'| = | d  e  f || y |
    # [ w ]    [ g  h  i ][ 1 ] 

    # since 8 unknowns, can set scale factor w = i = 1

    # [ x']   [ a  b  c ][ x ]
    # | y'| = | d  e  f || y |
    # [ 1 ]   [ g  h  1 ][ 1 ] 

    # this yields 8 equations when we have two pairs of 
    # both (x',y') and (x, y)

    # but a more elegant way to solve is to use 
    # singular value decomposition (SVD)
    # https://math.stackexchange.com/questions/494238/how-to-compute-homography-matrix-h-from-corresponding-points-2d-2d-planar-homog

    P = []
    for i in range(0, len(src_points)):
        x = src_points[i][0]
        y = src_points[i][1]
        u = dst_points[i][0]
        v = dst_points[i][1]
        P.append([-x, -y, -1, 0, 0, 0, u*x, u*y, u])
        P.append([0, 0, 0, -x, -y, -1, v*x, v*y, v])
        
    # now we have PH = 0, and solve for H using SVD
    P = np.array(P)
    U, s, Vh = np.linalg.svd(P)
    # select last singular vector of Vh
    # divide it by the bottom right value
    temp = Vh[-1,:] / Vh[-1,-1]

    # temp will be an array of length 9, 
    # we need 3x3 homography matrix
    H = temp.reshape(3,3)
    return H


def video_frame_generator(filename):
    """A generator function that returns a frame on each 'next()' call.

    Will return 'None' when there are no frames left.

    Args:
        filename (string): Filename.

    Returns:
        None.
    """
    # Todo: Open file with VideoCapture and set result to 'video'. Replace None
    # https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_gui/py_video_display/py_video_display.html
    video = cv2.VideoCapture(filename)

    # Do not edit this while loop
    while video.isOpened():
        ret, frame = video.read()

        if ret:
            yield frame
        else:
            break

    # Todo: Close video (release) and yield a 'None' value. (add 2 lines)
    video.release()
    yield None

def drawCrossHairsAtPoint(img_base, x, y, s):
    """drawCrossHairsAtPoint
    Warning: modifies img_base
    :param img_base:
    :param x:
    :param y:
    :param s:
    """
    cv2.line(img_base,
             (int(x - s), int(y)), # first point
             (int(x + s), int(y)), # secondpoint
             (0,255,0), # color
             2) # line thickness
    cv2.line(img_base,
             (int(x), int(y - s)), # first point
             (int(x), int(y + s)), # secondpoint
             (0,255,0), # color
             2) # line thickness
    addText(img_base,
            "x ={0}, y={1}".format(x,y),
            (x,y))
    # show_img("cross hairs", img_base)


def addText(img, text, location, color=(0, 0, 0)):
    fontFace = cv2.FONT_HERSHEY_SIMPLEX
    location = location
    fontScale = 0.6
    lineType = 2
    thickness = 2
    cv2.putText(img=img,
                text=text,
                org=location,
                fontFace=fontFace,
                fontScale=fontScale,
                color=color,
                lineType=lineType,
                thickness=thickness)

def convolveImg(img, template):
    img_tmp = img.copy()
    template_tmp = template.copy()


    # STEPS: 
    # 1. convolve the template with the image, find the max
    # 2. cover that marker with a black box the size of the template image
    # 3. repeat three more times until the four markers are found
    # use green channels for gray

    img_gray = img_tmp[:,:,1]
    template_gray = template_tmp[:,:,1]
    template_gray = template_gray.astype('float32')
    img_gray = img_gray.astype('float32')
    template_gray = template_gray - 127
    img_gray = img_gray - 127



    if False:
        img_convolved = ndimage.filters.convolve(input=img_gray,
                                                 weights=template_gray)

    if True:
        # reduce the width of the template by a few pixels...
        # template_tmp = template[:-1,:-1]
        # img_gray[img_gray<0] = 0
        img_convolved = cv2.filter2D(src=img_gray,
                                     ddepth=-1,
                                     kernel=template_gray)
    if False:
        img_convolved = signal.fftconvolve(in1=img_gray,
                                           in2=template_gray,
                                           mode="same")
    return img_convolved

def getCoordsFromConvolution(img_c, template):
    img_convolved = img_c.copy()
    template_tmp = template.copy()
    img_conv_h = img_convolved.shape[0]
    img_conv_w = img_convolved.shape[1]
    

    coords = []
    maxVal = 0
    # iterate over the convolved image four times to find all the markers
    for i in range(0, 4):
        temp = np.argwhere(img_convolved==img_convolved.max())
        # raw_input(temp)
        if i == 0:
            maxVal = img_convolved.max()
        marker_x = temp[0][0]
        marker_y = temp[0][1]
        msize_x = int(template_tmp.shape[0]*2.0)
        msize_y = int(template_tmp.shape[1]*2.0)
        splotchx_begin = marker_x - msize_x
        splotchx_end = marker_x + msize_x
        splotchy_begin = marker_y - msize_y
        splotchy_end = marker_y + msize_y
        if splotchx_begin < 0:
            splotchx_begin = 0
        if splotchx_end > img_conv_h:
            splotchx_end = img_conv_h
        if splotchy_begin < 0:
            splotchy_begin = 0
        if splotchy_end > img_conv_w:
            splotchy_end = img_conv_w
        coords.append([marker_x, marker_y])

        # # put a black box over that area
        # img_tmp[marker_x-msize_x:marker_x+msize_x, marker_y-msize_y:marker_y+msize_y] = 0 # delete
        # show_img("img gray", img_convolved) # delete, just for show
        img_convolved[splotchx_begin:splotchx_end, splotchy_begin:splotchy_end] = 0

    return coords, maxVal
