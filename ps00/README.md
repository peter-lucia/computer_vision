# Problem Set 0 Environment verification

This Problem Set is to setup the working environment for all other problem sets.

1. Start by downloading and installing the Docker Community Edition in your 
machine using the following instructions [here](https://docs.docker.com/install/).

2. Next download the [docker image for the class](https://drive.google.com/open?id=1WbLOvbDVdnHO0sP6QTnv4FZWc3utZCZ4).

3. Now install the image locally by running the following command: 
   ```bash
   docker load -i cv_all_image.tar
   ```
4. Now download the docker-compose.yml file. There are slight differences in 
the variables that need to be set for each operating system. Make sure to 
checkout the correct version for your system.:
   1. Linux(Ubuntu):
   ```bash
   git clone --single-branch -b master-ubuntu https://github.gatech.edu/omscs6476/docker_utils.git 
   ```
   2. Windows
   ```bash
   git clone --single-branch -b master-Windows https://github.gatech.edu/omscs6476/docker_utils.git 
   ```
   3. MacOS:
   ```bash
   git clone --single-branch -b master-macOS https://github.gatech.edu/omscs6476/docker_utils.git 
   ```
5. Now edit the docker-compose so that it correctly mounts this repository into 
/home/pset and set the correct user number and group for your system. 
Also see the instructions in the readme for the 
[docker compose here](https://github.gatech.edu/omscs6476/docker_utils) to setup pycharm.
6. Now run the ps0.py file from inside a docker container using the following
 command if you are on linux:
   ```bash
     docker-compose -f /path/to/docker-compose.yml run default
   ```
   All unit tests should pass and a frame of the video should be displayed. 